package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class Operations(
        @SerializedName("reg_admin") var regAdmin: Int? = null, // 3
        @SerializedName("battery") var battery: Int? = null, // 53
        @SerializedName("info") val info: Info? = null,
        @SerializedName("usage") val usage: Usage? = null,
        @SerializedName("operate_date") var operateDate: ArrayList<String>? = null,
        @SerializedName("stop_tracking") var sosDate: ArrayList<String>? = null,
        @SerializedName("error") var error: String? = null
)