package co.openapp.app.data.model.request.editmastercode

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 31,July,2018
 */
data class EditMasterCodeReq(

        @SerializedName("master_key")
        val masterKey: String? = null,

        @SerializedName("action")
        val action: String? = null
)