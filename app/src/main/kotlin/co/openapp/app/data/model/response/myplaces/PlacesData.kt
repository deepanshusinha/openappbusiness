package co.openapp.app.data.model.response.myplaces

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27,July,2018
 */
open class PlacesData {

        @SerializedName("id")
        open var id: Long? = null

        @SerializedName("label")
        open var label: String? = null

        @SerializedName("name")
        open var name: String? = null

        @SerializedName("latitude")
        open var latitude: Double? = null

        @SerializedName("longitude")
        open var longitude: Double? = null

        open var isSelected: Boolean = false

}