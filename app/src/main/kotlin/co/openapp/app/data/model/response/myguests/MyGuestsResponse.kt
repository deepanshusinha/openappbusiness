package co.openapp.app.data.model.response.myguests

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27,July,2018
 */
data class MyGuestsResponse(

        @SerializedName("data")
        var data: ArrayList<GuestData>? = null,

        @SerializedName("code")
        var code: Int = 0,

        @SerializedName("message")
        var message: String? = null
)