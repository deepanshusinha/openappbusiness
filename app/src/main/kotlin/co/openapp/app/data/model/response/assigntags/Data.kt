package co.openapp.app.data.model.response.assigntags

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 05,October,2018
 */
data class Data(
        @SerializedName("id")
        var id: Long? = null,

        @SerializedName("status")
        var status: String? = null,

        @SerializedName("lock_id")
        var lockId: Long? = null,

        @SerializedName("tag_id")
        var tagId: Long? = null
)