package co.openapp.app.data.db

/**
 * Created by deepanshusinha on 23/02/18.
 */

data class BottomSheet (
        var title: Int,

        var message: Int,

        var button: Int
)