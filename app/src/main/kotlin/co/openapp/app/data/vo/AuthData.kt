package co.openapp.app.data.vo

import com.google.gson.annotations.SerializedName


data class AuthData(
        @field:SerializedName("user")
        val user: User,
        @field:SerializedName("accessToken")
        val accessToken: String,
        @field:SerializedName("refreshToken")
        val refreshToken: String,
        @field:SerializedName("token")
        val token: String?
)