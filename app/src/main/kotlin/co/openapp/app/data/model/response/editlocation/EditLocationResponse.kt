package co.openapp.app.data.model.response.editlocation

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 31,July,2018
 */
data class EditLocationResponse (

        @SerializedName("data")
        val data: LocationTag? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null

)