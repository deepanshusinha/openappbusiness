package co.openapp.app.data.model.request.invite

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by abhijeethallur on 10,July,2018
 */
data class InviteGuestRequest(

        @SerializedName("invitation_mode")
        var invitationMode: String? = null,

        @SerializedName("locks")
        var locks: ArrayList<Long>? = null,

        @SerializedName("phone_nos")
        var phoneNumbers: ArrayList<PhoneNumbers>? = null,

        @SerializedName("notifications")
        var notification: Boolean = false,

        @SerializedName("access_level")
        var accessLevel: String? = null,

        @SerializedName("schedule_type")
        var scheduleType: String? = null,

        @SerializedName("schedule")
        var schedule: Schedule? = null

) : Serializable