package co.openapp.app.data.model.request.addadmin

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class AddAdminData(

        @SerializedName("manufacturer")
        var manufacturer: String? = null,

        @SerializedName("lockVersionString")
        var lockVersionString: String? = null,

        @SerializedName("lockName")
        var lockName: String? = null,

        @SerializedName("battery")
        var battery: Int? = null,

        @SerializedName("adminPwd")
        var adminPwd: String? = null,

        @SerializedName("lockKey")
        var lockKey: String? = null,

        @SerializedName("noKeyPwd")
        var noKeyPwd: String? = null,

        @SerializedName("deletePwd")
        var deletePwd: String? = null,

        @SerializedName("pwdInfo")
        var pwdInfo: String? = null,

        @SerializedName("timestamp")
        var timestamp: Long? = null,

        @SerializedName("aesKeyStr")
        var aesKeyStr: String? = null,

        @SerializedName("specialValue")
        var specialValue: Int? = null,

        @SerializedName("timezoneRawOffset")
        var timezoneRawOffset: Int? = null,

        @SerializedName("modelNumber")
        var modelNumber: String? = null,

        @SerializedName("hardwareRevision")
        var hardwareRevision: String? = null,

        @SerializedName("firmwareRevision")
        var firmwareRevision: String? = null
)