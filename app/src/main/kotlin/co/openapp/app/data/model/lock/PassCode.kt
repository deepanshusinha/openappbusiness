package co.openapp.app.data.model.lock


import com.google.gson.annotations.SerializedName

data class PassCode(
        @SerializedName("email")
        val email: String,
        @SerializedName("passcodeType")
        val passCodeType: Int,
        @SerializedName("startDate")
        val startDate: Long = 0,
        @SerializedName("endDate")
        val endDate: Long = 0
)