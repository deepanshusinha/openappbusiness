package co.openapp.app.data.repository.local

import android.os.Build
import android.text.TextUtils
import co.openapp.app.data.db.Records
import co.openapp.app.data.db.User
import co.openapp.app.data.model.request.*
import co.openapp.app.data.model.response.ListDataItem
import co.openapp.app.data.prefs.OpenAppPreferences
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.constant.Constants
import com.google.gson.reflect.TypeToken
import com.jaredrummler.android.device.DeviceName
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by deepanshusinha on 14/02/18.
 */

@Singleton
class OpenAppLocalRepository
@Inject constructor(private val preferences: OpenAppPreferences) {

    // User Details

    fun saveUserDetails(it: User) {
        preferences.save(Constants.SharedKeys.USER_DETAILS, it)
    }

    fun getUserDetails(): User? {
        return preferences[Constants.SharedKeys.USER_DETAILS, User::class.java]
    }

    fun getUserId(): Int? {
        return getUserDetails()?.id
    }

    fun clearSingleData(data: String) {
        preferences.clearData(data)
    }

    fun clearAllData() {
        preferences.clearAllData()
    }

    // Common

    fun saveVersionCode(versionCode: Long) {
        preferences.save(Constants.SharedKeys.VERSION_CODE, versionCode)
    }

    fun getVersionCode(): Long {
        return preferences.getLong(Constants.SharedKeys.VERSION_CODE)
    }

    fun saveVersionName(versionName: String) {
        preferences.save(Constants.SharedKeys.VERSION_NAME, versionName)
    }

    fun getVersionName(): String {
        return "v" + preferences.getString(Constants.SharedKeys.VERSION_NAME)
    }

    private fun getLockHashMapList(): HashMap<String, ListDataItem> {
        val hashMapType = object : TypeToken<HashMap<String, ListDataItem>>() {}.type
        val hashMap = preferences.get<HashMap<String, ListDataItem>>(Constants.SharedKeys.USER_LOCKS, hashMapType)

        return if (hashMap != null && hashMap.isNotEmpty()) {
            hashMap
        } else {
            HashMap()
        }
    }

    fun addAndSaveLockInList(jsonLock: ListDataItem) {
        val lockHashMapList = getLockHashMapList()
        lockHashMapList[jsonLock.macAddress] = jsonLock
        preferences.save(Constants.SharedKeys.USER_LOCKS, lockHashMapList)
    }

    fun saveUserLockList(lockList: List<ListDataItem>) {
        val lockHashMapList = HashMap<String, ListDataItem>()

        lockList.forEach { lockHashMapList[it.macAddress] = it }

        preferences.save(Constants.SharedKeys.USER_LOCKS, lockHashMapList)
    }

    fun getUserLockList(): List<ListDataItem> {
        val arrayList = ArrayList<ListDataItem>()

        getLockHashMapList().forEach { (_, value) ->
            arrayList.add(value)
        }

        return arrayList
    }

    private fun getLockId(macAddress: String?): Int? {
        getUserLockList().forEach {
            if (it.macAddress == macAddress)
                return it.lockId
        }
        return null
    }

    private fun saveAccessDataList(list: ArrayList<SyncRequest>) {
        preferences.save(Constants.SharedKeys.PUSH_DATA, list)
    }

    fun getAccessDataList(): ArrayList<SyncRequest> {
        val listType = object : TypeToken<ArrayList<SyncRequest>>() {}.type
        val arrayList = preferences.get<ArrayList<SyncRequest>>(Constants.SharedKeys.PUSH_DATA, listType)

        return if (arrayList != null && arrayList.size > 0) {
            arrayList
        } else {
            ArrayList()
        }
    }

    private fun lastItem(): Int {
        return getAccessDataList().size - 1
    }

    fun clearLogData() {
        preferences.clearData(Constants.SharedKeys.PUSH_DATA)
    }

    /** Returns the consumer friendly device name  */
    fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        if (model.startsWith(manufacturer)) {
            return capitalize(model)
        }
        return capitalize(manufacturer) + " " + model
    }

    private fun capitalize(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true

        val phrase = StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }

        return phrase.toString()
    }

    fun saveLocation(location: String) {
        preferences.save(Constants.SharedKeys.USER_LOCATION, location)
    }

    fun getLocation(): String? {
        return preferences.getString(Constants.SharedKeys.USER_LOCATION)
    }

    fun addSyncDataAccess(macAddress: String?) {
        val syncDataList = getAccessDataList()

        val syncRequest = SyncRequest()
        syncRequest.userId = getUserId()
        syncRequest.lockId = getLockId(macAddress)
        syncRequest.attempted = AppSnippet.currDate
        syncRequest.deviceId = getDeviceName()
        syncRequest.deviceName = DeviceName.getDeviceName()
        syncRequest.location = preferences.getString(Constants.SharedKeys.USER_LOCATION)

        syncDataList.add(syncRequest)

        saveAccessDataList(syncDataList)
    }

    fun attemptCount(count: Int) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            accessDataList[lastItem()].attemptCount = count
        }

        saveAccessDataList(accessDataList)
    }
    // Cabinet LockData

    fun cabinetConnectTime(time: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val cabinetLockData = getCabinetLockData(accessDataList)
            val cabinetLock = CabinetLock()
            cabinetLock.connected = time

            cabinetLockData.add(cabinetLock)

            accessDataList[lastItem()].cabinetLock = cabinetLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun cabinetError(error: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val cabinetLockData = getCabinetLockData(accessDataList)
            val cabinetLock = CabinetLock()
            cabinetLock.error = error

            cabinetLockData.add(cabinetLock)

            accessDataList[lastItem()].cabinetLock = cabinetLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun cabinetDisconnectTime(time: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val cabinetLockData = getCabinetLockData(accessDataList)
            val cabinetLockLastItem = cabinetLockData.size - 1

            if (cabinetLockLastItem >= 0)
                cabinetLockData[cabinetLockLastItem].disconnected = time

            accessDataList[lastItem()].cabinetLock = cabinetLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setCabinetOperations(cabOperations: Operations) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val cabinetLockData = getCabinetLockData(accessDataList)
            val cabinetLockLastItem = cabinetLockData.size - 1

            var operations = cabinetLockData[cabinetLockLastItem].operations
            if (operations == null)
                operations = Operations()
            operations.battery = cabOperations.battery

            cabinetLockData[cabinetLockLastItem].operations = operations

            accessDataList[lastItem()].cabinetLock = cabinetLockData
        }

        saveAccessDataList(accessDataList)
    }

    /*fun getCabinetOperations(): Operations? {
        val accessDataList = getAccessDataList()
        if (lastItem() >= 0)
            return getAccessDataList[lastItem()].cabinetLock.operations
    }*/

    fun setCabinetUsage(usage: Usage) {
        val cabinetSyncDataList = getAccessDataList()
        /*if (lastItem() >= 0) {
            cabinetSyncDataList[lastItem()].usage = usage
        }*/

        saveAccessDataList(cabinetSyncDataList)
    }

    fun getCabinetUsage(): Usage? {
        return getAccessDataList()[lastItem()].cabinetLock?.get(0)?.operations?.usage
    }

    fun setCabinetInfo(info: Info) {
        val cabinetSyncDataList = getAccessDataList()
        /*if (lastItem() >= 0) {
            cabinetSyncDataList[lastItem()].info = info
        }*/

        saveAccessDataList(cabinetSyncDataList)
    }

    fun getCabinetInfo(): Info? {
        return getAccessDataList()[lastItem()].cabinetLock?.get(0)?.operations?.info
    }

    fun setRegisteredAdmin(userId: Int?) {
        val cabinetSyncDataList = getAccessDataList()
        /*if (lastItem() >= 0) {
            cabinetSyncDataList[lastItem()].regAdmin = userId
        }*/

        saveAccessDataList(cabinetSyncDataList)
    }

    fun setAndAddCabinetOperateTime(currDate: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val cabinetLockData = getCabinetLockData(accessDataList)
            val cabinetLockLastItem = cabinetLockData.size - 1

            var operations = cabinetLockData[cabinetLockLastItem].operations
            if (operations == null)
                operations = Operations()

            var operateDate = operations.operateDate
            if (operateDate == null)
                operateDate = ArrayList()
            operateDate.add(currDate)

            operations.operateDate = operateDate

            cabinetLockData[cabinetLockLastItem].operations = operations

            accessDataList[lastItem()].cabinetLock = cabinetLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setAndSosTime(currDate: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val cabinetLockData = getCabinetLockData(accessDataList)
            val cabinetLockLastItem = cabinetLockData.size - 1

            var operations = cabinetLockData[cabinetLockLastItem].operations
            if (operations == null)
                operations = Operations()

            var operateDate = operations.sosDate
            if (operateDate == null)
                operateDate = ArrayList()
            operateDate.add(currDate)

            operations.sosDate = operateDate

            cabinetLockData[cabinetLockLastItem].operations = operations

            accessDataList[lastItem()].cabinetLock = cabinetLockData
        }

        saveAccessDataList(accessDataList)
    }

    private fun getCabinetLockData(accessDataList: ArrayList<SyncRequest>): ArrayList<CabinetLock> {
        val cabinetLock = accessDataList[lastItem()].cabinetLock

        return if (cabinetLock != null && cabinetLock.size > 0)
            cabinetLock
        else ArrayList()
    }

    // PadLock LockData

    fun padLockConnectTime(time: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLock = PadLock()
            padLock.connected = time

            padLockData.add(padLock)

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun padLockError(error: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLock = PadLock()
            padLock.error = error

            padLockData.add(padLock)

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun padLockDisconnectTime(time: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            if (padLockLastItem >= 0)
                padLockData[padLockLastItem].disconnected = time

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setOperations(value: Int) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            var operations = padLockData[padLockLastItem].operations
            if (operations == null)
                operations = Operations()
            operations.battery = value

            padLockData[padLockLastItem].operations = operations

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setOperations(value: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            var operations = padLockData[padLockLastItem].operations
            if (operations == null)
                operations = Operations()
            operations.error = value

            padLockData[padLockLastItem].operations = operations

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setAndAddOperateTime(currDate: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            var operations = padLockData[padLockLastItem].operations
            if (operations == null)
                operations = Operations()

            var operateDate = operations.operateDate
            if (operateDate == null)
                operateDate = ArrayList()
            operateDate.add(currDate)

            operations.operateDate = operateDate

            padLockData[padLockLastItem].operations = operations

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    private fun getPadLockData(accessDataList: ArrayList<SyncRequest>): ArrayList<PadLock> {
        val padLock = accessDataList[lastItem()].padLock

        return if (padLock != null && padLock.size > 0)
            padLock
        else ArrayList()
    }

    // Door LockData

    fun addOrSaveDoorLock(doorLock: DoorLock) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val doorLockData = getDoorLockData()
            doorLockData.add(doorLock)

            accessDataList[lastItem()].doorLock = doorLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun addOrSaveDoorLockError(errorMsg: String?) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val doorLockData = getDoorLockData()

            val doorLock = DoorLock()
            doorLock.error = errorMsg

            doorLockData.add(doorLock)
            accessDataList[lastItem()].doorLock = doorLockData
        }

        saveAccessDataList(accessDataList)
    }

    private fun getDoorLockData(): ArrayList<DoorLock> {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val doorLock = accessDataList[lastItem()].doorLock

            if (doorLock != null && doorLock.size > 0)
                return doorLock

        }
        return ArrayList()
    }

    fun getRecords(records: String?): List<Records>? {
        val listType = object : TypeToken<List<Records>>() {}.type
        if (records != null) {
            return preferences.getObject<List<Records>>(records, listType)
        }
        return ArrayList()
    }

    fun getInitTTLockList(): ArrayList<KeyRequest> {
        val listType = object : TypeToken<ArrayList<KeyRequest>>() {}.type
        val arrayList = preferences.get<ArrayList<KeyRequest>>(Constants.SharedKeys.INIT_TT_LOCK, listType)

        return if (arrayList != null && arrayList.size > 0) {
            arrayList
        } else {
            ArrayList()
        }
    }

    fun saveInitTTLockList(keyRequest: KeyRequest) {
        val initTTLockList = getInitTTLockList()
        initTTLockList.add(keyRequest)

        preferences.save(Constants.SharedKeys.INIT_TT_LOCK, initTTLockList)
    }

    fun clearInitTTLockData() {
        preferences.clearData(Constants.SharedKeys.INIT_TT_LOCK)
    }

    fun saveNokePwdUpdate(pwdUpdate: PadlockPwdUpdate) {
        preferences.save(Constants.SharedKeys.NOKE_PWD_UPDATE, pwdUpdate)
        val userLockList = getUserLockList()
        userLockList.forEach {
            if (it.macAddress == pwdUpdate.macAddress)
                it.padLockKey!!.lockPwd = pwdUpdate.passcode
        }
        saveUserLockList(userLockList)
    }

    fun getNokePwdUpdate(): PadlockPwdUpdate? {
        return if (preferences.isKeyShared(Constants.SharedKeys.NOKE_PWD_UPDATE))
            preferences[Constants.SharedKeys.NOKE_PWD_UPDATE, PadlockPwdUpdate::class.java]
        else
            null
    }

    fun clearNokePwdUpdate() {
        preferences.clearData(Constants.SharedKeys.NOKE_PWD_UPDATE)
    }

    fun isFirstTime(): Boolean {
        return preferences.isKeyShared(Constants.SharedKeys.FIRST_TIME)
    }

    fun setFirstTime(value: Boolean) {
        preferences.save(Constants.SharedKeys.FIRST_TIME, value)
    }

    fun saveAccessToken(it: String) {
        preferences.save(Constants.SharedKeys.ACCESS_TOKEN, it)
    }

    fun getAccessToken(): String? {
        return preferences.getString(Constants.SharedKeys.ACCESS_TOKEN)
    }

    fun saveRefreshToken(it: String) {
        preferences.save(Constants.SharedKeys.REFRESH_TOKEN, it)
    }

    fun getRefreshToken(): String? {
        return preferences.getString(Constants.SharedKeys.REFRESH_TOKEN)
    }

    fun saveLastOnlineTime(timeLong: Long) {
        preferences.save(Constants.SharedKeys.SERVER_TIME_UTC, timeLong.toString())
    }

    fun getLastOnlineTime(): Long? {
        return preferences.getString(Constants.SharedKeys.SERVER_TIME_UTC)?.toLong()
    }

    fun setNewApi(value: Boolean) {
        preferences.save(Constants.SharedKeys.NEW_API, value)
    }

    fun isNewApi(): Boolean {
        return preferences.getBoolean(Constants.SharedKeys.NEW_API)
    }
}