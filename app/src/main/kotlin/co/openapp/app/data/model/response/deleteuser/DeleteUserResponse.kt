package co.openapp.app.data.model.response.deleteuser

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 02,August,2018
 */
class DeleteUserResponse (

        @SerializedName("data")
        val data: Boolean? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)