package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

/**
 * Created by deepanshusinha on 23/02/18.
 */

data class ChangePasswordRequest(
        @SerializedName("old_password") var oldPassword: String,
        @SerializedName("new_password") var newPassword: String
)