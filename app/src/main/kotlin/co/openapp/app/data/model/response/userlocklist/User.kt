package co.openapp.app.data.model.response.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 26June2018
 */
open class User {

        @SerializedName("id")
        open var id: Long? = 0

        @SerializedName("role")
        open var role: String? = null

        @SerializedName("email")
        open var email: String? = null

        @SerializedName("first_name")
        open var firstName: String? = null

        @SerializedName("last_name")
        open var lastName: String? = null

        @SerializedName("mobile")
        open var mobile: String? = null

        @SerializedName("status")
        open var status: String? = null

        @SerializedName("email_verified")
        open var emailVerified: Boolean? = null

        @SerializedName("mobile_verified")
        open var mobileVerified: Boolean? = null

        @SerializedName("jwt_version")
        open var jwtVersion: Int? = 0

        @SerializedName("dp")
        open var dp: String? = null

        @SerializedName("name")
        open var name: String? = null
}