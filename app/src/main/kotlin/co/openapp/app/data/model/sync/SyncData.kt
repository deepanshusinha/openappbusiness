package co.openapp.app.data.model.sync

import co.openapp.app.data.db.Records
import co.openapp.app.data.model.lock.LocationRules
import co.openapp.app.data.model.lock.TimeRules
import co.openapp.app.data.model.lock.TripSheetRules
import co.openapp.app.data.model.request.DoorLock
import com.google.gson.annotations.SerializedName

data class SyncData(
        @SerializedName("attempted")
        var attempted: String? = null, // 2020-12-31 23:59:59
        @SerializedName("error")
        var error: ArrayList<String>? = null, // app_unauthorized_time/app_unauthorized_geofence
        @SerializedName("latitude")
        var latitude: Double? = 0.0, // 12
        @SerializedName("locationRule")
        var locationRule: ArrayList<LocationRules>? = null,
        @SerializedName("lockId")
        var lockId: String? = null, // 4
        @SerializedName("longitude")
        var longitude: Double? = 0.0, // 78
        @SerializedName("padLock")
        var padLock: ArrayList<PadLock>? = null,
        @SerializedName("doorLock")
        var doorLock: ArrayList<DoorLock>? = null,
        @SerializedName("timeRule")
        var timeRule: ArrayList<TimeRules>? = null,
        var tripSheetRule: ArrayList<TripSheetRules>? = null
)