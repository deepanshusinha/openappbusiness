package co.openapp.app.data.model.auth

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import co.openapp.app.BR
import co.openapp.app.R
import java.util.regex.Matcher
import java.util.regex.Pattern


class AuthForm : BaseObservable() {
    var register: Boolean = false
        internal set
    var reset: Boolean = false
        internal set

    val fields = AuthFields()
    private val errors = AuthErrorFields()
    val authFields = MutableLiveData<AuthFields>()

    val isChangeValid: Boolean
        @Bindable
        get() {
            var valid = isOldPasswordValid()
            valid = valid && isPasswordValid()
            valid = valid && isRePasswordValid()
            valid = valid && isOldPasswordMatches()
            return valid
        }

    val isResetValid: Boolean
        @Bindable
        get() {
            return if (reset) {
                isEmailValid()
            } else {
                var valid = isPasswordMatches()
                valid = valid && isPasswordValid()
                valid = valid && isRePasswordValid()
                valid = valid && isOtpValid()
                return valid
            }
        }

    val isValid: Boolean
        @Bindable
        get() {
            var valid = isPasswordValid()
//            notifyPropertyChanged(BR.emailError)

            if (register) {
                valid = isEmailValid() && valid
                valid = isFirstNameValid() && valid
                valid = isPhoneNumberValid() && valid
                valid = isPasswordMatches() && valid
                valid = isRePasswordValid() && valid
//                valid = isRememberMe() && valid
//                notifyPropertyChanged(BR.firstNameError)
//                notifyPropertyChanged(BR.phoneNumberError)
            } else {
                valid = isUserNameValid() && valid
            }
//            notifyPropertyChanged(BR.passwordError)

            if (valid) {
                errors.error = null
            }
//            notifyPropertyChanged(BR.error)

            return valid
        }

    val emailError: Int?
        @Bindable
        get() = errors.email

    val passwordError: Int?
        @Bindable
        get() = errors.password

    val firstNameError: Int?
        @Bindable
        get() = errors.firstName

    val phoneNumberError: Int?
        @Bindable
        get() = errors.phoneNumber

    val error: Int?
        @Bindable
        get() = errors.error

    @get:Bindable
    var rememberMe: Boolean = fields.rememberMe
        set(value) {
            // Avoids infinite loops.
            if (fields.rememberMe != value) {
                fields.rememberMe = value

                // Clear error on checked box
                if (value) {
                    errors.error = null
                }
            }

            // Notify observers of a new value.
            notifyPropertyChanged(BR.error)
            field = value
        }

    private fun isEmailValid(): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(fields.email)
        val matches = matcher.matches()

        return if (matches) {
            errors.email = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.email = R.string.prompt_email_address_required
            errors.error = R.string.prompt_email_address_required
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isPasswordValid(): Boolean {
        val password = fields.password

        return if (password.length > 3) {
            errors.password = null
            notifyPropertyChanged(BR.valid)
            true
        } else if (password.isNotEmpty()) {
            errors.password = R.string.prompt_password_error
            errors.error = R.string.prompt_password_error
            notifyPropertyChanged(BR.valid)
            false
        } else {
            errors.password = R.string.prompt_password_required
            errors.error = R.string.prompt_password_required
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isRePasswordValid(): Boolean {
        val password = fields.rePassword

        return if (password.isNotEmpty()) {
            errors.rePassword = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.rePassword = R.string.prompt_re_password_required
            errors.error = R.string.prompt_re_password_required
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isOldPasswordValid(): Boolean {
        val password = fields.oldPassword
        return if (password.isNotEmpty()) {
            errors.password = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.password = R.string.old_password_field_required
            errors.error = R.string.old_password_field_required
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isPasswordMatches(): Boolean {
        return if (fields.password == fields.rePassword) {
            errors.rePassword = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.rePassword = R.string.prompt_re_password_error
            errors.error = R.string.prompt_re_password_error
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isOldPasswordMatches(): Boolean {
        fields.newPassword = fields.password
        return if (fields.newPassword == fields.rePassword) {
            errors.rePassword = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.rePassword = R.string.prompt_re_password_error
            errors.error = R.string.prompt_re_password_error
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isFirstNameValid(): Boolean {
        val firstName = fields.firstName

        return if (firstName.isNotEmpty() && firstName.length > 2) {
            errors.firstName = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.firstName = R.string.prompt_first_name_required
            errors.error = R.string.prompt_first_name_required
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isPhoneNumberValid(): Boolean {
        val phoneNumber = fields.phoneNumber

        return if (phoneNumber.length == 10) {
            errors.phoneNumber = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.phoneNumber = R.string.prompt_phone_number_required
            errors.error = R.string.prompt_phone_number_required
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isUserNameValid(): Boolean {
        val userName = fields.userName

        return if (userName.isNotEmpty() && userName.length > 2) {
            errors.userName = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.userName = R.string.user_name_field_required
            errors.error = R.string.user_name_field_required
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    private fun isOtpValid(): Boolean {
        val otp = fields.otp

        return if (otp.length == 6) {
            errors.otp = null
            notifyPropertyChanged(BR.valid)
            true
        } else {
            errors.otp = R.string.otp_valid
            errors.error = R.string.otp_valid
            notifyPropertyChanged(BR.valid)
            false
        }
    }

    fun onClick() {
        if (isValid) {
            authFields.value = fields
        }
    }
}