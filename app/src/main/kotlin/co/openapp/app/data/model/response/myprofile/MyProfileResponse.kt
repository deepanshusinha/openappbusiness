package co.openapp.app.data.model.response.myprofile

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 26,July,2018
 */
data class MyProfileResponse(

        @SerializedName("data")
        val profileData: ProfileData? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)