package co.openapp.app.data.model.response.editassignedtag

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 31,July,2018
 */
data class EditedTag (

        @SerializedName("label")
        val label: String? = null,

        @SerializedName("latitude")
        val latitude: Double? = null,

        @SerializedName("longitude")
        val longitude: Double? = null,

        @SerializedName("name")
        val name: String? = null

)