package co.openapp.app.data.model.response.registerlock

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class RegisterLockResponse (

        @SerializedName("data")
        val data: Data? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)