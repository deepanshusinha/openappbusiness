package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class LockInitResponse(
        @SerializedName("macAddress") val lockId: Int, // 456456
        @SerializedName("keyId") val keyId: Int // 456456
)