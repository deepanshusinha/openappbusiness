package co.openapp.app.data.model.lock

import com.google.gson.annotations.SerializedName

data class TimeRules(
        @SerializedName("accessId")
        val accessId: String, // 1e77w5d00000000
        @SerializedName("ruleId")
        val ruleId: String, // 1e77nfd00000000
        @SerializedName("schedule_type")
        val scheduleType: String, // timed
        @SerializedName("remainingAccess")
        val remainingAccess: Int, // -1
        @SerializedName("validDates")
        val validDates: List<ValidDates>
)