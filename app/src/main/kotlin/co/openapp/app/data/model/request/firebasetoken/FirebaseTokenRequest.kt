package co.openapp.app.data.model.request.firebasetoken

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 10,August,2018
 */
data class FirebaseTokenRequest(

        @SerializedName("device_token")
        var deviceToken: String? = null

)