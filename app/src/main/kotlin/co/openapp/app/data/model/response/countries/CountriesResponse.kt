package co.openapp.app.data.model.response.countries

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 06,June,2018
 */
data class CountriesResponse(

        @SerializedName("data")
        val countries: ArrayList<CountryData?>? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)