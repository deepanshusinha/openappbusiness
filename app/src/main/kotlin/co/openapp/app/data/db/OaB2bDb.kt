package co.openapp.app.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import co.openapp.app.data.model.lock.LockData
import co.openapp.app.data.model.user.UserData

/**
 * Main database description.
 */
@Database(
        entities = [LockData::class, UserData::class],
        version = 20,
        exportSchema = false
)
@TypeConverters(OpenAppTypeConverters::class)
abstract class OaB2bDb : RoomDatabase() {

    abstract fun lockDao(): LockDao

    abstract fun userDao(): UserDao
}