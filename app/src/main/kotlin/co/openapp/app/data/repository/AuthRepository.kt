/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.openapp.app.data.repository

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.openapp.app.data.api.ApiResponse
import co.openapp.app.data.api.OpenAppNewService
import co.openapp.app.data.db.UserDao
import co.openapp.app.data.model.auth.AuthFields
import co.openapp.app.data.model.general.GeneralResponse
import co.openapp.app.data.model.request.otp.OtpFields
import co.openapp.app.data.model.user.ProfileResponse
import co.openapp.app.data.model.user.UserData
import co.openapp.app.data.prefs.OpenAppPreferences
import co.openapp.app.data.prefs.OpenAppPreferencesLiveData
import co.openapp.app.data.source.NetworkBoundResource
import co.openapp.app.data.source.Resource
import co.openapp.app.data.vo.AuthResponse
import co.openapp.app.utils.Utils
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.schedulers.AppExecutors
import com.crashlytics.android.Crashlytics
import com.google.gson.reflect.TypeToken
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository that handles User objects.
 */
@Singleton
class AuthRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val preferences: OpenAppPreferences,
        livePreference: OpenAppPreferencesLiveData,
        private val userDao: UserDao,
        private val appService: OpenAppNewService,
        utils: Utils
) : AppRepository(utils, preferences, livePreference) {

    fun doLogin(authRequest: AuthFields): LiveData<Resource<AuthResponse>> {
        return object : NetworkBoundResource<AuthResponse, AuthResponse>(appExecutors) {
            override fun saveCallResult(item: AuthResponse) {
                val data = item.data
                preferences.save(Constants.SharedKeys.ACCESS_TOKEN, data.accessToken)
                val user = data.user
                preferences.save(Constants.SharedKeys.COMPANY_ID, user.business[0].subCompanyId)
                preferences.save("LOGIN", item)
                Crashlytics.setUserIdentifier(user.id)
                Crashlytics.setUserName(user.firstName)
                Crashlytics.setUserEmail(user.email)
            }

            override fun shouldFetch(data: AuthResponse?): Boolean = true

            override fun loadFromDb(): LiveData<AuthResponse> = getAccessDataList("LOGIN")

            override fun createCall() = appService.callLoginApi(authRequest)
        }.asLiveData()
    }

    fun doRegister(authRequest: AuthFields): LiveData<Resource<AuthResponse>> {
        return object : NetworkBoundResource<AuthResponse, AuthResponse>(appExecutors) {
            override fun saveCallResult(item: AuthResponse) {
                preferences.save("REGISTER", item)
            }

            override fun shouldFetch(data: AuthResponse?): Boolean = true

            override fun loadFromDb(): LiveData<AuthResponse> = getAccessDataList("REGISTER")

            override fun createCall() = appService.callRegisterApi(authRequest)
        }.asLiveData()
    }

    fun doForgot(authRequest: AuthFields): LiveData<Resource<AuthResponse>> {
        return object : NetworkBoundResource<AuthResponse, AuthResponse>(appExecutors) {
            override fun saveCallResult(item: AuthResponse) {
                preferences.save("FORGOT", item)
            }

            override fun shouldFetch(data: AuthResponse?): Boolean = true

            override fun loadFromDb(): LiveData<AuthResponse> = getAccessDataList("FORGOT")

            override fun createCall() = appService.callForgotPasswordApi(authRequest)
        }.asLiveData()
    }

    fun doOtp(authRequest: OtpFields): LiveData<Resource<AuthResponse>> {
        return object : NetworkBoundResource<AuthResponse, AuthResponse>(appExecutors) {
            override fun saveCallResult(item: AuthResponse) {
                preferences.save("OTP", item)
            }

            override fun shouldFetch(data: AuthResponse?): Boolean = true

            override fun loadFromDb(): LiveData<AuthResponse> = getAccessDataList("OTP")

            override fun createCall() = appService.callOtpVerifyApi(authRequest)
        }.asLiveData()
    }

    fun doOtpResend(authRequest: OtpFields): LiveData<Resource<AuthResponse>> {
        return object : NetworkBoundResource<AuthResponse, AuthResponse>(appExecutors) {
            override fun saveCallResult(item: AuthResponse) {
                preferences.save("OTP Resend", item)
            }

            override fun shouldFetch(data: AuthResponse?): Boolean = true

            override fun loadFromDb(): LiveData<AuthResponse> = getAccessDataList("OTP Resend")

            override fun createCall() = appService.callOtpResendApi(authRequest)
        }.asLiveData()
    }

    fun doForgetChangePassword(authRequest: AuthFields): LiveData<Resource<AuthResponse>> {
        return object : NetworkBoundResource<AuthResponse, AuthResponse>(appExecutors) {
            override fun saveCallResult(item: AuthResponse) {
                preferences.save("Forgot Change Password", item)
            }

            override fun shouldFetch(data: AuthResponse?): Boolean = true

            override fun loadFromDb(): LiveData<AuthResponse> = getAccessDataList("Forgot Change Password")

            override fun createCall() = appService.callChangePasswordApi(authRequest)
        }.asLiveData()
    }

    fun loadProfileDetail(forceLoad: Boolean): LiveData<Resource<List<UserData>>> {
        return object : NetworkBoundResource<List<UserData>, ProfileResponse>(appExecutors) {
            override fun saveCallResult(item: ProfileResponse) {
                userDao.saveProfileDetail(item.data)
            }

            override fun shouldFetch(data: List<UserData>?): Boolean {
                return data == null || data.isEmpty() || forceLoad
            }

            override fun loadFromDb(): LiveData<List<UserData>> {
                return userDao.fetchProfileFromDb()
            }

            override fun createCall(): LiveData<ApiResponse<ProfileResponse>> {
                return appService.callProfileApi()
            }

        }.asLiveData()
    }

    fun doChangePassword(authRequest: AuthFields): LiveData<Resource<AuthResponse>> {
        return object : NetworkBoundResource<AuthResponse, AuthResponse>(appExecutors) {
            override fun saveCallResult(item: AuthResponse) {
                preferences.save("Change Password", item)
            }

            override fun shouldFetch(data: AuthResponse?): Boolean = true

            override fun loadFromDb(): LiveData<AuthResponse> = getAccessDataList("Change Password")

            override fun createCall() = appService.callProfileChangePasswordApi(authRequest)
        }.asLiveData()
    }

    fun doGeneralApi(): LiveData<Resource<GeneralResponse>> {
        return object : NetworkBoundResource<GeneralResponse, GeneralResponse>(appExecutors) {
            override fun saveCallResult(item: GeneralResponse) {
                preferences.save(Constants.SharedKeys.GENERAL, item)
                preferences.save(Constants.SharedKeys.FRESH_TIMEOUT_IN_HOURS, item.data.preferences.onlineTimeRange * 3600L)
            }

            override fun shouldFetch(data: GeneralResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<GeneralResponse> {
                return getGeneralLiveData()
            }

            override fun createCall(): LiveData<ApiResponse<GeneralResponse>> {
                return appService.callGeneralApi()
            }

        }.asLiveData()
    }

    @MainThread
    fun getAccessDataList(key: String): MutableLiveData<AuthResponse> {
        val response: MutableLiveData<AuthResponse> = MutableLiveData()
        val listType = object : TypeToken<AuthResponse>() {}.type
        val arrayList = preferences.get<AuthResponse>(key, listType)
        response.value = arrayList
        return response
    }

    fun clearProfileData() {
        userDao.deleteAll()
    }


    @MainThread
    fun getGeneralLiveData(): MutableLiveData<GeneralResponse> {
        val response: MutableLiveData<GeneralResponse> = MutableLiveData()
        val listType = object : TypeToken<GeneralResponse>() {}.type
        val generalResponse = preferences.get<GeneralResponse>(Constants.SharedKeys.GENERAL, listType)
        response.value = generalResponse
        return response
    }

    fun getGeneralData(): GeneralResponse? {
        val listType = object : TypeToken<GeneralResponse>() {}.type
        return preferences.get<GeneralResponse>(Constants.SharedKeys.GENERAL, listType)
    }
}
