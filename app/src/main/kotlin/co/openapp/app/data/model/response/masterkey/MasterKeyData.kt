package co.openapp.app.data.model.response.masterkey

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 21,August,2018
 */
open class MasterKeyData {

    @SerializedName("master_key")
    open var masterKey: String? = null

    @SerializedName("lock_id")
    open var lockId: Long? = null
}