package co.openapp.app.data.repository


import co.openapp.app.data.db.Records
import co.openapp.app.data.db.User
import co.openapp.app.data.model.request.*
import co.openapp.app.data.model.auth.AuthFields
import co.openapp.app.data.model.response.*
import co.openapp.app.data.repository.local.OpenAppLocalRepository
import co.openapp.app.data.repository.remote.OpenAppRemoteRepository
import co.openapp.app.utils.constant.Constants
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by deepanshusinha on 15/02/18.
 */

@Singleton
class OpenAppRepository
@Inject constructor(private val localRepository: OpenAppLocalRepository,
                    private val remoteRepository: OpenAppRemoteRepository) {

    // Common

    fun getVersionCode(): Long {
        return localRepository.getVersionCode()
    }

    fun getVersionName(): String {
        return localRepository.getVersionName()
    }

    fun doLogin(loginRequest: AuthFields): Single<AuthResponse> {
        return remoteRepository.callLoginApi(loginRequest)
    }

    fun onLoginSuccess(it: AuthData?) {
        if (null != it) {
            localRepository.saveUserDetails(User(it.userId, it.name, it.email, it.admin, it.token))
        }
    }

    fun isLoggedIn(): Boolean {
        val userDetails = localRepository.getUserDetails()

        if (null != userDetails) {
            return userDetails.id > 0
        }

        return false
    }

    fun doForgotPassword(email: ForgotRequest): Single<AuthResponse> {
        return remoteRepository.callForgotPasswordApi(email)
    }

    fun doChangePassword(changePasswordRequest: ChangePasswordRequest): Single<AuthResponse> {
        return remoteRepository.callChangePasswordApi(localRepository.getUserDetails()?.id.toString(), changePasswordRequest)
    }

    fun doLogout() {
        localRepository.clearSingleData(Constants.SharedKeys.USER_DETAILS)
        localRepository.clearSingleData(Constants.SharedKeys.USER_LOCKS)
    }

    fun doListApi(): Single<ListResponse> {
        return remoteRepository.callListApi(localRepository.getUserId())
    }

    fun doGeneralApi(): Single<GeneralResponse> {
        return remoteRepository.callGeneralApi()
    }

    fun saveLockList(lockList: List<ListDataItem>) {
        localRepository.saveUserLockList(lockList)
    }

    fun getUserLockList(): List<ListDataItem> {
        return localRepository.getUserLockList()
    }

    fun getUser(): User? {
        return localRepository.getUserDetails()

    }

    fun getUserId(): Int? {
        return getUser()?.id
    }

    fun addAccessData(macAddress: String?, lockType: String) {

        when (lockType) {
            Constants.LockType.PAD_LOCK -> {
                localRepository.addSyncDataAccess(macAddress)
            }
            Constants.LockType.CABINET_LOCK -> {
                localRepository.addSyncDataAccess(macAddress)
            }
            Constants.LockType.DOOR_LOCK -> {
                localRepository.addSyncDataAccess(macAddress)
            }
        }
    }

    fun setLocation(location: String) {
        localRepository.saveLocation(location)
    }

    fun getLocation(): String? {
        return localRepository.getLocation()
    }

    fun setAttemptCount(count: Int) {
        localRepository.attemptCount(count)
    }

    fun isDataAvailable(): Boolean {
        return localRepository.getAccessDataList().size > 0
    }

    fun clearAccessData() {
        localRepository.clearLogData()
    }

    // Cabinet

    fun setCabinetConnectTime(currDate: String) {
        localRepository.cabinetConnectTime(currDate)
    }

    fun setCabError(errorMsg: String) {
        localRepository.cabinetError(errorMsg)
    }

    fun setCabinetDisconnectTime(currDate: String) {
        localRepository.cabinetDisconnectTime(currDate)
    }

    fun setCabinetOperations(operations: Operations) {
        localRepository.setCabinetOperations(operations)
    }

    fun setCabinetUsage(usage: Usage) {
        localRepository.setCabinetUsage(usage)
    }

    fun getCabinetUsage(): Usage? {
        return localRepository.getCabinetUsage()
    }

    fun setCabinetInfo(info: Info) {
        localRepository.setCabinetInfo(info)
    }

    fun getCabinetInfo(): Info? {
        return localRepository.getCabinetInfo()
    }

    fun setRegisteredAdmin(userId: Int?) {
        localRepository.setRegisteredAdmin(userId)
    }

    fun setAndAddCabinetOperateTime(currDate: String) {
        localRepository.setAndAddCabinetOperateTime(currDate)
    }

    fun setAndSosTime(currDate: String) {
        localRepository.setAndSosTime(currDate)
    }

    fun doDoorPwdUpdate(pwdUpdate: DoorPwdUpdate): Single<AuthResponse> {
        return remoteRepository.callDoorPwdUpdateApi(localRepository.getUserId().toString(), pwdUpdate)
    }

    // Pad Lock

    fun setPadConnectTime(time: String) {
        localRepository.padLockConnectTime(time)
    }

    fun setPadError(errorMsg: String) {
        localRepository.padLockError(errorMsg)
    }

    fun setPadDisconnectTime(time: String) {
        localRepository.padLockDisconnectTime(time)
    }

    fun setOperations(value: Int) {
        localRepository.setOperations(value)
    }

    fun setOperations(value: String) {
        localRepository.setOperations(value)
    }

    fun setAndAddOperateTime(currDate: String) {
        localRepository.setAndAddOperateTime(currDate)
    }

    fun doPadLockPwdUpdateApi(pwdUpdate: PadlockPwdUpdate): Single<AuthResponse> {
        return remoteRepository.callPadLockPwdUpdateApi(pwdUpdate)
    }

    // Door Lock

    fun saveTTLockInit(keyRequest: KeyRequest) {
        localRepository.saveInitTTLockList(keyRequest)
    }

    fun addOrSaveDoorLock(doorLock: DoorLock) {
        localRepository.addOrSaveDoorLock(doorLock)
    }

    fun addOrSaveDoorLockError(errorMsg: String?) {
        localRepository.addOrSaveDoorLockError(errorMsg)
    }

    fun getRecords(records: String?): List<Records>? {
        return localRepository.getRecords(records)
    }

    fun doInitApi(keyRequest: KeyRequest): Single<SyncResponse> {
        val initTTLockList = localRepository.getInitTTLockList()
        initTTLockList.add(keyRequest)

        return remoteRepository.callInitApi(initTTLockList)
    }

    fun clearInitTTLockData() {
        localRepository.clearInitTTLockData()
    }

    fun saveNokePwdUpdate(pwdUpdate: PadlockPwdUpdate) {
        localRepository.saveNokePwdUpdate(pwdUpdate)
    }

    fun getNokePwdUpdate(): PadlockPwdUpdate? {
        return localRepository.getNokePwdUpdate()
    }

    fun clearNokePwdUpdate() {
        localRepository.clearNokePwdUpdate()
    }

    fun saveLockInList(jsonLock: ListDataItem) {
        localRepository.addAndSaveLockInList(jsonLock)
    }

    fun updateList() {
        Timber.e("UpdateList")
    }

    fun setNewApi(value: Boolean) {
        localRepository.setNewApi(value)
    }
}
