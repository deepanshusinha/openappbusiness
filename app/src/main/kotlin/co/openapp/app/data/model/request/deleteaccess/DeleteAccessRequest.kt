package co.openapp.app.data.model.request.deleteaccess

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 08,August,2018
 */
data class DeleteAccessRequest(

        @SerializedName("user_ids")
        var userIds: ArrayList<Long>? = null

)