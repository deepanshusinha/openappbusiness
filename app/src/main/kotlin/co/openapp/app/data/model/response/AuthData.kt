package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class AuthData(
        @SerializedName("user_id") val userId: Int,
        @SerializedName("email_verification") val emailVerification: Int,
        @SerializedName("created") val created: String,
        @SerializedName("name") val name: String,
        @SerializedName("phone_verification") val phoneVerification: Int,
        @SerializedName("email") val email: String,
        @SerializedName("admin") val admin: Boolean,
        @SerializedName("token") val token: String? = null
)