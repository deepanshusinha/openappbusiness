package co.openapp.app.data.model.lock

import com.google.gson.annotations.SerializedName

data class TripSheetRules(
        @SerializedName("tripsheetId")
        val tripSheetId: String, // 1e77w5d00000000
        @SerializedName("tripsheetPathId")
        val tripSheetPathId: String, // 1e77nfd00000000
        @SerializedName("name")
        val name: String, // INOX Lido Mall
        @SerializedName("radius")
        val radius: Long, // 50
        @SerializedName("latitude")
        val latitude: Double, // 12.9732369
        @SerializedName("longitude")
        val longitude: Double // 77.62127620000001
)