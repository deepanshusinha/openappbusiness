package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class DoorPwdUpdate(
        @SerializedName("mac_id") val macAddress: String?, // 2
        @SerializedName("adminKeyboardPwd") val adminKeyboardPwd: String? // 874222
)