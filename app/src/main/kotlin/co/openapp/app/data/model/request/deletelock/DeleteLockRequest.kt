package co.openapp.app.data.model.request.deletelock

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 02,August,2018
 */
data class DeleteLockRequest(

        @SerializedName("reset")
        val reset: Boolean? = false
)