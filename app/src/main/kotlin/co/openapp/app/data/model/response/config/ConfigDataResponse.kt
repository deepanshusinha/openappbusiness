package co.openapp.app.data.model.response.config

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class ConfigDataResponse(
        @SerializedName("code")
        var id: Int? = 0,

        @SerializedName("message")
        var message: String? = null,

        @SerializedName("data")
        var configData: ConfigData? = null
)