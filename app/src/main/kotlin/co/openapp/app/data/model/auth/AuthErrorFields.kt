package co.openapp.app.data.model.auth

data class AuthErrorFields(
        var email: Int? = null,
        var password: Int? = null,
        var rePassword: Int? = null,
        var firstName: Int? = null,
        var lastName: Int? = null,
        var fullName: Int? = null,
        var phoneNumber: Int? = null,
        var rememberMe: Int? = null,
        var error: Int? = null,
        var userName: Int? = null,
        var otp: Int? = null,
        var token: Int? = null
)