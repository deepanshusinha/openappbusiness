package co.openapp.app.data.db


import co.openapp.app.utils.constant.Operation

/**
 * Created by Administrator on 2016/7/15 0015.
 */
data class BleSession(

        /**
         * 操作
         */
        var operation: Operation? = null,

        /**
         * mac地址
         */
        var lockmac: String? = null,

        /**
         * 密码
         */
        var password: String? = null
)
