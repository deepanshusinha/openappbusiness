package co.openapp.app.data.model.response.invite

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 10,July,2018
 */
data class InvitationResponse(

        @SerializedName("data")
        val data: Data? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)