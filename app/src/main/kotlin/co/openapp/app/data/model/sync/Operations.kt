package co.openapp.app.data.model.sync

import com.google.gson.annotations.SerializedName

data class Operations(
        @SerializedName("error")
        var error: String? = null,
        @SerializedName("status")
        var status: String? = null,
        @SerializedName("operateDate")
        var operateDate: OperateDate? = null
)