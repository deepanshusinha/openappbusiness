package co.openapp.app.data.model.response.lockactivities

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 18,July,2018
 */
data class Data(

        @SerializedName("length")
        var code: Long? = 0,

        @SerializedName("activities")
        var activities: ArrayList<LocksActivity>? = null

)