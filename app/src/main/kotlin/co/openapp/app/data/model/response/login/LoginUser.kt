package co.openapp.app.data.model.response.login

import com.google.gson.annotations.SerializedName

open class LoginUser {
    @SerializedName("id")
    open var userId: Long = 0

    @SerializedName("name")
    open var name: String? = null

    @SerializedName("role")
    open var role: String? = null

    @SerializedName("email")
    open var email: String? = null

    @SerializedName("first_name")
    open var firstName: String? = null

    @SerializedName("last_name")
    open var lastName: String? = null

    @SerializedName("mobile")
    open var mobile: String? = null

    @SerializedName("status")
    open var status: String? = null

    @SerializedName("email_verified")
    open var email_verified: Boolean? = null

    @SerializedName("mobile_verified")
    open var mobile_verified: Boolean? = null

    @SerializedName("jwt_version")
    open var jwt_version: Int? = null

    @SerializedName("dp")
    open var dp: String? = null

    @SerializedName("created_at")
    open var createdAt: String? = null
}