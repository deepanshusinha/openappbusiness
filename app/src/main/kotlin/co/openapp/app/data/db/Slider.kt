package co.openapp.app.data.db

/**
 * Created by deepanshusinha on 13/03/18.
 */

data class Slider (
        var title: Int = 0,

        var json: Int = 0
)