package co.openapp.app.data.prefs

import android.content.SharedPreferences
import co.openapp.app.utils.livedata.pref.LivePreference
import co.openapp.app.utils.livedata.pref.MultiPreference
import co.openapp.app.utils.livedata.pref.MultiPreferenceAny
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OpenAppPreferencesLiveData
@Inject constructor(private val preferences: SharedPreferences) {

    private lateinit var listener: SharedPreferences.OnSharedPreferenceChangeListener
    private val updates: Observable<String>

    init {
        updates = Observable.create(ObservableOnSubscribe<String> { emitter ->
            listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key -> emitter.onNext(key) }

            emitter.setCancellable { preferences.unregisterOnSharedPreferenceChangeListener(listener) }

            preferences.registerOnSharedPreferenceChangeListener(listener)
        }).share()
    }

    fun getString(key: String?, defaultValue: String? = ""): LivePreference<String> {
        checkNotNull(key)
        checkNotNull(defaultValue)
        return LivePreference(updates, preferences, key, defaultValue)
    }

    fun getInt(key: String?, defaultValue: Int?): LivePreference<Int> {
        checkNotNull(key)
        checkNotNull(defaultValue)
        return LivePreference(updates, preferences, key, defaultValue)
    }

    fun getBoolean(key: String?, defaultValue: Boolean?): LivePreference<Boolean> {
        checkNotNull(key)
        checkNotNull(defaultValue)
        return LivePreference(updates, preferences, key, defaultValue)
    }

    fun getFloat(key: String?, defaultValue: Float?): LivePreference<Float> {
        checkNotNull(key)
        checkNotNull(defaultValue)
        return LivePreference(updates, preferences, key, defaultValue)
    }

    fun getLong(key: String?, defaultValue: Long?): LivePreference<Long> {
        checkNotNull(key)
        checkNotNull(defaultValue)
        return LivePreference(updates, preferences, key, defaultValue)
    }

    fun getStringSet(key: String?, defaultValue: Set<String>?): LivePreference<Set<String>> {
        checkNotNull(key)
        checkNotNull(defaultValue)
        return LivePreference(updates, preferences, key, defaultValue)
    }

    fun <T> listenMultiple(keys: List<String>, defaultValue: T): MultiPreference<T> {
        return MultiPreference(updates, preferences, keys, defaultValue)
    }

    fun listenUpdatesOnly(keys: List<String>): MultiPreferenceAny {
        return MultiPreferenceAny(updates, keys)
    }
}