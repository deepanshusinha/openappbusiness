package co.openapp.app.data.model.response.recentusers

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 23,August,2018
 */
data class RecentUsersResponse (

        @SerializedName("data")
        val recentUserData: ArrayList<RecentUserData>? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null

)