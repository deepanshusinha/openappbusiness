package co.openapp.app.data.model.sync

import com.google.gson.annotations.SerializedName

data class OperateDate(
        @SerializedName("sent")
        var sent: String? = null,
        @SerializedName("received")
        var received: String? = null
)