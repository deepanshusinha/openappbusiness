package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(
        @SerializedName("email") var email: String,
        @SerializedName("password") var password: String,
        @SerializedName("mobile") var mobile: String? = null,
        @SerializedName("country_id") var countryId: Int? = null,
        @SerializedName("device_id") var deviceId: String? = null,
        @SerializedName("device_type") var deviceType: String? = null,
        @SerializedName("device_name") var deviceName: String? = null,
        @SerializedName("mode") var mode: String? = null,
        @SerializedName("device_token") var deviceToken: String? = null,
        @SerializedName("tzoffset") var tzoffset: Long? = null
)