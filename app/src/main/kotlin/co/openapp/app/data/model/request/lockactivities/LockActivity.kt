package co.openapp.app.data.model.request.lockactivities

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 12,July,2018
 */
open class LockActivity {

    @SerializedName("action")
    open var action: String? = null

    @SerializedName("extra")
    open var extra: Int? = null

    @SerializedName("user_id")
    open var userId: Long? = null

    @SerializedName("password")
    open var password: String? = null

    @SerializedName("time")
    open var time: Long? = null

    @SerializedName("battery")
    open var battery: Int? = 0

    @SerializedName("lock_id")
    open var lockId: Long? = 0

    @SerializedName("latitude")
    open var latitude: Double? = null

    @SerializedName("longitude")
    open var longitude: Double? = null

    @SerializedName("location_source")
    open var locationSource: String? = null

}