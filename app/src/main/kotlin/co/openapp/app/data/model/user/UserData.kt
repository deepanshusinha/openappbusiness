package co.openapp.app.data.model.user

import androidx.room.Entity
import androidx.room.Index
import co.openapp.app.utils.constant.Constants
import com.google.gson.annotations.SerializedName

@Entity(indices = [Index(value = ["email"], unique = true)],
        primaryKeys = ["email"],
        tableName = Constants.DatabaseKeys.TABLE_USER)
data class UserData(
        @SerializedName("email")
        val email: String,
        @SerializedName("firstName")
        val firstName: String,
        @SerializedName("lastName")
        val lastName: String?,
        @SerializedName("phone")
        val phone: String?,
        @SerializedName("business")
        val business: List<Business>
)