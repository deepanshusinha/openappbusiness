package co.openapp.app.data.model.lock

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Index
import co.openapp.app.utils.constant.Constants
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Entity(indices = [Index(value = ["macAddress"], unique = true)],
        primaryKeys = ["macAddress"],
        tableName = Constants.DatabaseKeys.TABLE_LOCK)
@Parcelize
data class LockData(
        @ColumnInfo(name = "lockId")
        @SerializedName("id")
        val id: String, // 1e6rinj00000000
        @SerializedName("deployLocation")
        val location: String?, // null
        @SerializedName("lockName")
        val lockName: String, // Test Lock
        @SerializedName("currentState")
        val currentState: String, // Test Lock
        @SerializedName("lockType")
        val lockType: String, // padlock
        @SerializedName("macAddress")
        val macAddress: String, // c8:df:84:2b:97:0d
        @SerializedName("battery")
        val battery: Int, // 100
        @SerializedName("parameters")
        @Embedded
        val parameters: @RawValue Parameters,
        @SerializedName("locationRules")
        val locationRules: @RawValue ArrayList<LocationRules>,
        @SerializedName("timeRules")
        var timeRules: @RawValue ArrayList<TimeRules>,
        @SerializedName("tripsheet")
        var tripSheet: @RawValue ArrayList<TripSheetRules>
) : Parcelable