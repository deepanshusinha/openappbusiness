package co.openapp.app.data.model.response.masterkey

import com.google.gson.annotations.SerializedName

data class GetMasterKeyResponse(

        @SerializedName("data")
        var masterKeyData: MasterKeyData? = null,

        @SerializedName("code")
        var code: Int = 0,

        @SerializedName("message")
        var message: String? = null
)