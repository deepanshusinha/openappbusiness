package co.openapp.app.data.model.response.editaccess

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 31,July,2018
 */
data class Data (

        @SerializedName("code")
        val code: Int = 0

)