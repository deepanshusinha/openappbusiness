package co.openapp.app.data.model.request

import co.openapp.app.data.model.lock.LockVersion
import com.google.gson.annotations.SerializedName

data class KeyRequest(
        @SerializedName("mac") var lockMac: String,
        @SerializedName("lockKey") val unlockKey: String,
        @SerializedName("adminPwd") val adminPs: String,
        @SerializedName("aesKey") val aesKeyStr: String,
        @SerializedName("pwdInfo") val pwdInfo: String,
        @SerializedName("noKeyPwd") val noKeyPwd: String,
        @SerializedName("deletePwd") val deletePwd: String?,
        @SerializedName("firmwareRevision") val firmwareRevision: String,
        @SerializedName("hardwareRevision") val hardwareRevision: String,
        @SerializedName("lockFlagPos") val lockFlagPos: Int,
        @SerializedName("lockName") var lockName: String,
        @SerializedName("lockVersion") val lockVersion: LockVersion,
        @SerializedName("modelNumber") val modelNumber: String,
        @SerializedName("specialValue") val specialValue: Int,
        @SerializedName("timestamp") val timestamp: Long,
        @SerializedName("timezoneRawOffset") val timezoneRawOffset: Long,
        @SerializedName("productId") var productId: String? = ""
)