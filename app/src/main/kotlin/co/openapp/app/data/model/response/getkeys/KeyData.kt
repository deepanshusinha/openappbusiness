package co.openapp.app.data.model.response.getkeys

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27,June,2018
 */
open class KeyData {

    @SerializedName("lock_id")
    open var lockId: Long? = null

    @SerializedName("key")
    open var key: Key? = null

    @SerializedName("id")
    open var id: Int? = null

    @SerializedName("is_tagged")
    open var isTagged: Boolean? = null

    @SerializedName("lock_type")
    open var lockType: String? = null

    @SerializedName("location_tags")
    open var locationTags: List<Location>? = null

    @SerializedName("location_offset")
    open var locationOffset: Int? = null
}