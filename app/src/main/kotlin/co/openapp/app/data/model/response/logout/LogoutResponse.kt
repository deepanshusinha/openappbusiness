package co.openapp.app.data.model.response.logout

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 10,August,2018
 */
data class LogoutResponse (

        @SerializedName("data")
        var data: Boolean? = null,

        @SerializedName("code")
        var code: Int = 0,

        @SerializedName("message")
        var message: String? = null

)