package co.openapp.app.data.model.response.myplaces

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27,July,2018
 */
data class MyPlacesResponse(

        @SerializedName("data")
        var data: ArrayList<PlacesData>? = null,

        @SerializedName("code")
        var code: Int = 0,

        @SerializedName("message")
        var message: String? = null
)