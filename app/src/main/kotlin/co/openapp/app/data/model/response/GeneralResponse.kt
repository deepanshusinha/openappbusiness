package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class GeneralResponse(
        @SerializedName("min_android_ver") val minAndroidVer: Int, // 1.0.0
        @SerializedName("max_android_ver") val maxAndroidVer: Int // 1.0.1
)