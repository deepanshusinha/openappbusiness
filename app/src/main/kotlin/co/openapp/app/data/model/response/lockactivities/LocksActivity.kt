package co.openapp.app.data.model.response.lockactivities

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 18July2018
 */
open class LocksActivity {

    @SerializedName("id")
    open var id: Long? = 0

    @SerializedName("action")
    open var action: String? = null

    @SerializedName("battery")
    open var battery: Int? = null

    @SerializedName("extra")
    open var extra: String? = null

    @SerializedName("password")
    open var password: String? = null

    @SerializedName("time")
    open var time: String? = null

    @SerializedName("lock_id")
    open var lockId: Long? = null

    @SerializedName("lock_name")
    open var lockName: String? = null

    @SerializedName("user_id")
    open var userId: Long? = null

    @SerializedName("f_name")
    open var fName: String? = null

    @SerializedName("l_name")
    open var lName: String? = null

    @SerializedName("dp")
    open var dp: String? = null

    open var date: String? = null

    open var localTime: String? = null

    open var errorText: String? = null

    open var itemType: Int? = null

}