package co.openapp.app.data.model.response.error

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 13,June,2018
 */
class ErrorResponse(

        @SerializedName("code")
        var code: Int = 0,

        @SerializedName("data")
        var data: String? = null,

        @SerializedName("message")
        var message: String? = null
)