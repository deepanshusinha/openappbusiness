package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName
import com.polidea.rxandroidble2.RxBleDevice

data class ListDataItem(
        @SerializedName("user_id") val userId: Int, //2
        @SerializedName("name") val name: String, //Deepanshu Sinha
        @SerializedName("email") val email: String, //deepanshu@openapp.co
        @SerializedName("lock_id") val lockId: Int, //2
        @SerializedName("lock_name") val lockName: String, //H15_efb720
        @SerializedName("location") val location: String, //HSR Office
        @SerializedName("address") val address: String, //HSR Layout, Bengaluru
        @SerializedName("mac_address") val macAddress: String, //C7:E3:30:20:B7:EF
        @SerializedName("product_id") val productId: String, //H15_efb720
        @SerializedName("type") val type: String, //DoorLock
        @SerializedName("userlock_id") val userlockId: Int, //2
        @SerializedName("created") val created: String, //2018-05-09 15:00:00
        @SerializedName("expiry") val expiry: String, //2018-07-09 15:00:00
        @SerializedName("status") val status: Int, //1
        @SerializedName("security_info") val securityInfo: SecurityInfo? = null,
        @SerializedName("location_constraint") val locationConstraint: List<LocationConstraint>? = null,
        @SerializedName("time_constraint") val timeConstraint: List<TimeConstraint>? = null,
        @SerializedName("loto_engaged") val lotoEngaged: Boolean = false,
        @SerializedName("doorlock_parameter") val doorLockKey: DoorLockKey? = null,
        @SerializedName("padlock_parameter") val padLockKey: PadLockKey? = null
) {
    lateinit var bleDevice: RxBleDevice // rxbledevice

}