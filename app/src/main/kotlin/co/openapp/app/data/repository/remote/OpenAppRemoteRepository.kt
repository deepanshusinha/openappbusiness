package co.openapp.app.data.repository.remote

import co.openapp.app.data.model.request.*
import co.openapp.app.data.model.auth.AuthFields
import co.openapp.app.data.model.response.AuthResponse
import co.openapp.app.data.model.response.GeneralResponse
import co.openapp.app.data.model.response.ListResponse
import co.openapp.app.data.model.response.SyncResponse
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by deepanshu on 1/12/17.
 */

@Singleton
class OpenAppRemoteRepository
@Inject constructor() {

    fun callLoginApi(request: AuthFields): Single<AuthResponse> {
        return Single.create {  }
    }

    fun callRegisterApi(registerRequest: AuthFields): Single<AuthResponse> {
        return Single.create {  }
    }

    fun callForgotPasswordApi(email: ForgotRequest): Single<AuthResponse> {
        return Single.create {  }
    }

    fun callChangePasswordApi(userId: String, changePasswordRequest: ChangePasswordRequest): Single<AuthResponse> {
        return Single.create {  }
    }

    fun callListApi(userId: Int?): Single<ListResponse> {
        return Single.create {  }
    }

    fun callSyncApi(syncRequest: ArrayList<SyncRequest>): Single<SyncResponse> {
        return Single.create {  }
    }

    fun callInitApi(ttLockList: ArrayList<KeyRequest>): Single<SyncResponse> {
        return Single.create {  }
    }

    fun callGeneralApi(): Single<GeneralResponse> {
        return Single.create {  }
    }

    fun callDoorPwdUpdateApi(userId: String, pwdUpdate: DoorPwdUpdate): Single<AuthResponse> {
        return Single.create {  }
    }

    fun callPadLockPwdUpdateApi(pwdUpdate: PadlockPwdUpdate): Single<AuthResponse> {
        return Single.create {  }
    }
}