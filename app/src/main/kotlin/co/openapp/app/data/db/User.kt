package co.openapp.app.data.db


/**
 * Created by deepanshusinha on 14/02/18.
 */

data class User(
        var id: Int,

        var name: String,

        var email: String,

        var isAdmin: Boolean,

        var key: String? = null
)