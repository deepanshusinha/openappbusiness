package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class DoorLockKey(
        @SerializedName("aes_key_str") val aesKeyString: String, // 123156
        @SerializedName("admin_ps") val adminPs: String, // M201-d9223
        @SerializedName("lock_flag_pos") val lockFlagPos: Int, // M201-d9223
        @SerializedName("lock_version") val lockVersion: String, // {\"showAdminKbpwdFlag\":true,\"groupId\":10,\"protocolVersion\":3,\"protocolType\":5,\"orgId\":3,\"logoUrl\":\"\",\"scene\":2}
        @SerializedName("is_admin") val isAdmin: Boolean, // M201-d9223
        @SerializedName("unlock_key") val unlockKey: String, // 52:A6:D8:B2:C1:00
        @SerializedName("timezone_raw_offset") val timezoneRawOffset: Long // 1235545850000
)