package co.openapp.app.data.model.response.refreshtoken

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,July,2018
 */
data class RefreshAccessTokenResponse(

        @SerializedName("code")
        var code: Int = 0,

        @SerializedName("data")
        var data: Data? = null,

        @SerializedName("message")
        var message: String? = null

)