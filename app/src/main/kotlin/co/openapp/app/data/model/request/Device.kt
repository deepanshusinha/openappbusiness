package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class Device(
        @SerializedName("shackle") val shackle: String,
        @SerializedName("temperature") val temperature: String,
        @SerializedName("battery") val battery: String,
        @SerializedName("battery_cover") val batteryCover: String
)