package co.openapp.app.data.db

import com.google.gson.annotations.SerializedName

data class Records(
        @SerializedName("deleteDate") val deleteDate: Long, // 0
        @SerializedName("operateDate") val operateDate: Long, // 1529671601000
        @SerializedName("electricQuantity") val electricQuantity: Int, // 100
        @SerializedName("keyId") val keyId: Int, // 100
        @SerializedName("recordId") val recordId: Int, // 1529671602
        @SerializedName("recordType") val recordType: Int, // 1
        @SerializedName("uid") val uid: Int, // 158458
        @SerializedName("password") val password: String, // 158458
        @SerializedName("newPassword") val newPassword: String // 158458
)