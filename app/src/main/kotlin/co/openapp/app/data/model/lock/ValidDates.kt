package co.openapp.app.data.model.lock

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ValidDates(
        @SerializedName("endDate")
        val endDate: Long, // 1551036600
        @SerializedName("startDate")
        val startDate: Long // 1551252600
) : Parcelable