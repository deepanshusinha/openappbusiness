package co.openapp.app.data.db

import androidx.room.TypeConverter
import co.openapp.app.data.model.lock.*
import co.openapp.app.data.model.user.Business
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*
import kotlin.collections.ArrayList


object OpenAppTypeConverters {
    val gson = Gson()

    @TypeConverter
    @JvmStatic
    fun stringToBusinessList(data: String?): List<Business> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Business>>() {}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    @JvmStatic
    fun businessListToString(Business: List<Business>): String {
        return gson.toJson(Business)
    }

    @TypeConverter
    @JvmStatic
    fun stringToLocationRulesList(data: String?): ArrayList<LocationRules> {
        if (data == null) {
            return ArrayList()
        }

        val listType = object : TypeToken<ArrayList<LocationRules>>() {}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    @JvmStatic
    fun locationRulesListToString(locationRules: ArrayList<LocationRules>): String {
        return gson.toJson(locationRules)
    }

    @TypeConverter
    @JvmStatic
    fun stringToTimeRulesList(data: String?): ArrayList<TimeRules> {
        if (data == null) {
            return ArrayList()
        }

        val listType = object : TypeToken<ArrayList<TimeRules>>() {}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    @JvmStatic
    fun timeRulesListToString(TimeRules: ArrayList<TimeRules>): String {
        return gson.toJson(TimeRules)
    }

    @TypeConverter
    @JvmStatic
    fun stringToTripSheet(data: String?): ArrayList<TripSheetRules> {
        if (data == null) {
            return ArrayList()
        }

        val listType = object : TypeToken<ArrayList<TripSheetRules>>() {}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    @JvmStatic
    fun tripSheetToString(tripSheetRules: ArrayList<TripSheetRules>): String {
        return gson.toJson(tripSheetRules)
    }

    @TypeConverter
    @JvmStatic
    fun stringToValidDatesList(data: String?): List<ValidDates> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<ValidDates>>() {}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    @JvmStatic
    fun validDatesListToString(ValidDates: List<ValidDates>): String {
        return gson.toJson(ValidDates)
    }

    @TypeConverter
    @JvmStatic
    fun stringToLockVersion(data: String?): LockVersion? {

        val listType = object : TypeToken<LockVersion>() {}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    @JvmStatic
    fun lockVersionToString(lockVersion: LockVersion?): String {
        return gson.toJson(lockVersion)
    }
}