package co.openapp.app.data.model.sync

import com.google.gson.annotations.SerializedName

data class PadLock(
        @SerializedName("battery")
        var battery: Int? = -1, // 54
        @SerializedName("connected")
        var connected: String? = null, // 2020-12-31 23:59:59
        @SerializedName("disconnected")
        var disconnected: String? = null, // 2020-12-31 23:59:59
        @SerializedName("error")
        var error: String? = null, // lock error
        @SerializedName("operations")
        var operations: ArrayList<Operations>? = null,
        @SerializedName("token")
        var token: String? = null
)