package co.openapp.app.data.model.request.register

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 13,June,2018
 */
data class RegisterRequest(


        @SerializedName("first_name")
        var firstName: String? = null,

        @SerializedName("last_name")
        var lastName: String? = null,

        @SerializedName("email")
        var email: String? = null,

        @SerializedName("country_id")
        var country_id: Int = 1,

        @SerializedName("language_id")
        var language_id: Int = 1,

        @SerializedName("mobile")
        var mobile: String? = null,

        @SerializedName("product_id_number")
        var productId: String? = null,

        @SerializedName("password")
        var password: String? = null,

        @SerializedName("otp")
        var otp: String? = null
)