package co.openapp.app.data.model.response.getkeys

import com.google.gson.annotations.SerializedName


/**
 * Created by abhijeethallur on 27,June,2018
 */
open class Key {

    @SerializedName("date")
    open var date: Long? = null

    @SerializedName("specialValue")
    open var specialValue: Long? = null

    @SerializedName("lockAlias")
    open var lockAlias: String? = null

    @SerializedName("keyStatus")
    open var keyStatus: String? = null

    @SerializedName("endDate")
    open var endDate: Long? = null

    @SerializedName("noKeyPwd")
    open var noKeyPwd: String? = null

    @SerializedName("lockMac")
    open var lockMac: String? = null

    @SerializedName("deletePwd")
    open var deletePwd: String? = null

    @SerializedName("timezoneRawOffset")
    open var timezoneRawOffset: Long? = null

    @SerializedName("lockId")
    open var lockId: Long? = null

    @SerializedName("electricQuantity")
    open var electricQuantity: Long? = null

    @SerializedName("adminPwd")
    open var adminPwd: String? = null

    @SerializedName("lockFlagPos")
    open var lockFlagPos: Long? = null

    @SerializedName("keyboardPwdVersion")
    open var keyboardPwdVersion: Long? = null

    @SerializedName("aesKeyStr")
    open var aesKeyStr: String? = null

    @SerializedName("remoteEnable")
    open var remoteEnable: Long? = null

    @SerializedName("lockVersion")
    open var lockVersion: LockVersion? = null

    @SerializedName("userType")
    open var userType: String? = null

    @SerializedName("lockKey")
    open var lockKey: String? = null

    @SerializedName("lockName")
    open var lockName: String? = null

    @SerializedName("startDate")
    open var startDate: Long? = null

    @SerializedName("remarks")
    open var remarks: String? = null

    @SerializedName("keyRight")
    open var keyRight: Long? = null
}