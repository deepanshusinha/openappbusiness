package co.openapp.app.data.model.response.editassignedtag

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 31,July,2018
 */
data class EditAssignedTagResponse (

        @SerializedName("data")
        val data: EditedTag? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null

)