package co.openapp.app.data.model.response.assigntags

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class AssignTagsResponse(
        @SerializedName("code")
        var id: Int? = 0,

        @SerializedName("message")
        var message: String? = null,

        @SerializedName("data")
        var data: ArrayList<Data>? = null
)