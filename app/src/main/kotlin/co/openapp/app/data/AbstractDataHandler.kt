package co.openapp.app.data

import android.content.SharedPreferences

abstract class AbstractDataHandler {

    abstract val sharedPreferences: SharedPreferences

    fun setSharedIntData(key: String, value: Int) {
        editor.putInt(key, value).apply()
    }

    fun getSharedIntData(key: String, defVal: Int): Int {
        return sharedPreferences.getInt(key, defVal)
    }

    fun setSharedStringData(key: String, value: String) {
        editor.putString(key, value).apply()
    }

    fun getSharedStringData(key: String): String {
        return sharedPreferences.getString(key, null)
    }

    fun setSharedLongData(key: String, value: Long) {
        editor.putLong(key, value).apply()
    }

    fun getSharedLongData(key: String, defVal: Long): Long {
        return sharedPreferences.getLong(key, defVal)
    }

    fun setSharedDoubleData(key: String, value: Double) {
        editor.putLong(key, java.lang.Double.doubleToRawLongBits(value)).apply()
    }

    fun getSharedDoubleData(key: String, defVal: Double): Double {
        return sharedPreferences.getLong(key, java.lang.Double.doubleToLongBits(defVal)).toDouble()
    }

    fun setSharedBooleanData(key: String, value: Boolean) {
        editor.putBoolean(key, value).apply()
    }

    fun getSharedBooleanData(key: String, defVal: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defVal)
    }

    fun isKeyShared(key: String): Boolean {
        return sharedPreferences.contains(key)
    }

    private val editor: SharedPreferences.Editor
        get() = sharedPreferences.edit()

    fun clearData(key: String) {
        if (isKeyShared(key)) {
            editor.remove(key).apply()
        }
    }

    fun clearAll() {
        editor.clear().apply()
    }

}