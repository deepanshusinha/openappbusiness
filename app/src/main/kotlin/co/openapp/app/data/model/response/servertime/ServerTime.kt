package co.openapp.app.data.model.response.servertime

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 03,August,2018
 */
data class ServerTime (

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("data")
        val data: Long? = null,

        @SerializedName("message")
        val message: String? = null

)