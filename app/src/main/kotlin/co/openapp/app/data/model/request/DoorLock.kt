package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class DoorLock(
        @SerializedName("deleteDate") val deleteDate: String? = null, // 0
        @SerializedName("operateDate") val operateDate: String? = null,
        @SerializedName("battery") val battery: Int? = 0, // 100
        @SerializedName("keyId") val keyId: Int? = 0, // 100
        @SerializedName("recordType") val recordType: Int? = 0, // 1
        @SerializedName("uid") val uid: Int? = 0, // 158458
        @SerializedName("password") val password: String? = null, // 158458
        @SerializedName("newPassword") val newPassword: String? = null,
        @SerializedName("error") var error: String? = null
)