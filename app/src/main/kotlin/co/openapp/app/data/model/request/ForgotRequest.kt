package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

/**
 * Created by deepanshusinha on 27/03/18.
 */

data class ForgotRequest(
        @SerializedName("email") var email: String? = null
)