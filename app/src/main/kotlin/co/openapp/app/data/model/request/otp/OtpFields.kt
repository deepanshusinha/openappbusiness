package co.openapp.app.data.model.request.otp

import com.google.gson.annotations.SerializedName

data class OtpFields(
        @SerializedName("phone")
        var phone: String = "",
        @SerializedName("otp")
        var otp: String = "",
        @SerializedName("countryCode")
        var countryCode: String = ""
)