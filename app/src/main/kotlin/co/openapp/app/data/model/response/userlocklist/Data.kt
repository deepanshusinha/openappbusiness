package co.openapp.app.data.model.response.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class Data (

        @SerializedName("locks")
        var locks: ArrayList<UserLock>? = null,

        @SerializedName("length")
        var length: Int? = 0
)