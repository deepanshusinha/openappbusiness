package co.openapp.app.data.model.response.getkeys

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27,June,2018
 */
open class LockVersion {

    @SerializedName("showAdminKbpwdFlag")
    open var showAdminKbpwdFlag: Boolean? = null

    @SerializedName("groupId")
    open var groupId: Int? = null

    @SerializedName("protocolVersion")
    open var protocolVersion: Int? = null

    @SerializedName("protocolType")
    open var protocolType: Int? = null

    @SerializedName("orgId")
    open var orgId: Int? = null

    @SerializedName("logoUrl")
    open var logoUrl: String? = null

    @SerializedName("scene")
    open var scene: Int? = null
}