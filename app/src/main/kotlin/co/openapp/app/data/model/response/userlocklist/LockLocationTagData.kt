package co.openapp.app.data.model.response.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27,June,2018
 */
open class LockLocationTagData {

    @SerializedName("latitude")
    open var latitude: Double? = null

    @SerializedName("longitude")
    open var longitude: Double? = null

    @SerializedName("name")
    open var name: String? = null

    @SerializedName("label")
    open var label: String? = null

    @SerializedName("user_id")
    open var userId: Long? = null

    @SerializedName("id")
    open var id: Long? = null
}