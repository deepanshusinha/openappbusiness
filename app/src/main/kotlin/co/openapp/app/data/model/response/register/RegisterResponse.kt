package co.openapp.app.data.model.response.register

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 11,June,2018
 */
data class RegisterResponse(

        @SerializedName("code")
        var code: Int = 0,

        @SerializedName("data")
        var registerData: RegisterData? = null,

        @SerializedName("message")
        var message: String? = null
)