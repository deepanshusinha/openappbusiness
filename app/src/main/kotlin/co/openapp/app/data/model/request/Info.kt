package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class Info(
        @SerializedName("master") var master: String, //2344
        @SerializedName("otp") val otp: String, //3563
        @SerializedName("flag") var flag: String, //1
        @SerializedName("user_id") var uid: String //34
)