package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class SyncRequest(
        @SerializedName("location") var location: String? = null, //location
        @SerializedName("user_id") var userId: Int? = 0,
        @SerializedName("door_lock") var doorLock: ArrayList<DoorLock>? = null,
        @SerializedName("cabinet_lock") var cabinetLock: ArrayList<CabinetLock>? = null,
        @SerializedName("pad_lock") var padLock: ArrayList<PadLock>? = null,
        @SerializedName("attempted") var attempted: String? = null,
        @SerializedName("device_id") var deviceId: String? = null,
        @SerializedName("device_name") var deviceName: String? = null,
        @SerializedName("lock_id") var lockId: Int? = 0,
        @SerializedName("attempt_count") var attemptCount: Int? = 0
)