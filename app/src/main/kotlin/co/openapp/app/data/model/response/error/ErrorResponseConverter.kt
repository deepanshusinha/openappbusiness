package co.openapp.app.data.model.response.error

import android.util.Log
import com.google.gson.Gson
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.SSLPeerUnverifiedException

/**
 * Created by abhijeethallur on 13,June,2018
 */
class ErrorResponseConverter {

    companion object {
        fun convert(throwable: Throwable): ErrorResponse {
            Log.e("ErrorResponseConverter", "=" + Gson().toJson(throwable))
            return when (throwable) {
                is HttpException -> {
                    Log.e("Code", "==" + Gson().toJson(throwable))
                    val errorBody = throwable.response().errorBody()?.string()
                    Gson().fromJson(errorBody, ErrorResponse::class.java)
                }
                is ConnectException -> ErrorResponse(0, null, "Something went wrong, we are working on it. Please try after some time!")
                is SocketTimeoutException -> ErrorResponse(0, null, "Connection timeout, Please check your internet connection.")
                is UnknownHostException -> ErrorResponse(0, null, "Unknown host")
                is SSLPeerUnverifiedException -> ErrorResponse(0, null, "Peer not authenticated")
                is Exception -> ErrorResponse(throwable.cause?.message?.toInt()!!, null, throwable.message)
                else -> ErrorResponse(0, null, "Something went wrong, Please try again.")
            }
        }
    }
}