package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class PadLockKey(
        @SerializedName("lock_key") val lockKey: String, // null
        @SerializedName("lock_pwd") var lockPwd: String // null
)