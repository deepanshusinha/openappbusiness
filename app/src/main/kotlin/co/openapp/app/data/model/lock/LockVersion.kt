package co.openapp.app.data.model.lock


import com.google.gson.annotations.SerializedName

data class LockVersion(
        @SerializedName("groupId")
        val groupId: Int?, // 10
        @SerializedName("orgId")
        val orgId: Int?, // 12
        @SerializedName("protocolType")
        val protocolType: Int?, // 5
        @SerializedName("protocolVersion")
        val protocolVersion: Int?, // 3
        @SerializedName("scene")
        val scene: Int? // 2
)