package co.openapp.app.data.vo

import co.openapp.app.data.model.lock.LockData
import com.polidea.rxandroidble2.RxBleDevice

sealed class CommonMergedData
data class ScanningData(val passwordItems: RxBleDevice) : CommonMergedData()
data class SavedData(val categoryItems: List<LockData>) : CommonMergedData()