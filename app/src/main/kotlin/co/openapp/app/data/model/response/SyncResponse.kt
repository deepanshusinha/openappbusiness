package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by deepanshusinha on 27/02/18.
 */

class SyncResponse(
        @SerializedName("success") val success: Boolean,
        @SerializedName("message") val message: String
)