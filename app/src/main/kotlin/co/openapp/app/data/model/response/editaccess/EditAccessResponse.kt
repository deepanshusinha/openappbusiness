package co.openapp.app.data.model.response.editaccess

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 31,July,2018
 */
data class EditAccessResponse (

        @SerializedName("data")
        val data: Boolean? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null

)