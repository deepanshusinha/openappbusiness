package co.openapp.app.data.model.response.config

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class ConfigData(

        @SerializedName("offline_period")
        var offlinePeriod: Long? = null,

        @SerializedName("location_offset")
        var locationOffset: Int? = null,

        @SerializedName("activity_limit")
        var activityLimit: Int? = null,

        @SerializedName("correction_offset")
        var correctionOffset: Int? = null
)