package co.openapp.app.data.model.lock


import com.google.gson.annotations.SerializedName

data class VerifyTripId(
        @SerializedName("tripsheetId")
        val tripSheetId: String?, // 123
        @SerializedName("tripId")
        val tripId: String? // 123456
)