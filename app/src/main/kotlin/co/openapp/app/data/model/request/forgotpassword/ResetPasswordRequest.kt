package co.openapp.app.data.model.request.forgotpassword

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 13,June,2018
 */
data class ResetPasswordRequest(

        @SerializedName("mode")
        var mode: String? = null,

        @SerializedName("mobile")
        var mobile: String? = null,

        @SerializedName("email")
        var email: String? = null,

        @SerializedName("country_id")
        var countryId: Int? = 0,

        @SerializedName("password")
        var password: String? = null,

        @SerializedName("otp")
        var otp: String? = null

)