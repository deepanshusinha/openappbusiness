package co.openapp.app.data.model.request.otp

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData

class OtpForm : BaseObservable() {

    val otp = OtpFields()
    val otpFields = MutableLiveData<OtpFields>()

    val isValid: Boolean
        @Bindable
        get() {
            return true
        }
}