package co.openapp.app.data.model.request.editplace

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class LocationTag(

        @SerializedName("id")
        var id: Long? = null,

        @SerializedName("latitude")
        var latitude: Double? = null,

        @SerializedName("longitude")
        var longitude: Double? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("label")
        var label: String? = null
) : Serializable