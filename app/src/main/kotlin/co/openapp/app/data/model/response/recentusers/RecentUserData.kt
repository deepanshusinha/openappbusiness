package co.openapp.app.data.model.response.recentusers

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 23,August,2018
 */
data class RecentUserData(

        @SerializedName("first_name")
        val firstName: String? = null,

        @SerializedName("last_name")
        val lastName: String? = null,

        @SerializedName("dp")
        val dp: String? = null
)