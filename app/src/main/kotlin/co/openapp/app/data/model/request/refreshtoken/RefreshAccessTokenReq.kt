package co.openapp.app.data.model.request.refreshtoken

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,July,2018
 */
data class RefreshAccessTokenReq(

        @SerializedName("access_token")
        var accessToken: String? = null,

        @SerializedName("refresh_token")
        var refreshToken: String? = null,

        @SerializedName("device_type")
        var deviceType: String? = null,

        @SerializedName("device_id")
        var deviceId: String? = null
)