package co.openapp.app.data.source


data class State(val status: Status, val message: String?) {
    companion object {
        fun success(message: String): State {
            return State(Status.SUCCESS, message)
        }

        fun error(message: String): State {
            return State(Status.ERROR, message)
        }

        fun loading(message: String): State {
            return State(Status.LOADING, message)
        }
    }
}