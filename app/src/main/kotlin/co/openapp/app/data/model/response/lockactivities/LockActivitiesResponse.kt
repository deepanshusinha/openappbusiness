package co.openapp.app.data.model.response.lockactivities

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 18,July,2018
 */
data class LockActivitiesResponse(

        @SerializedName("code")
        var code: Int? = 0,

        @SerializedName("data")
        var activities: ArrayList<LocksActivity>? = null,

        @SerializedName("message")
        var message: String? = null

)