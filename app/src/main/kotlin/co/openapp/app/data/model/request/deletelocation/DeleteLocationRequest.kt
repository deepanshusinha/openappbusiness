package co.openapp.app.data.model.request.deletelocation

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 08,August,2018
 */
data class DeleteLocationRequest(

        @SerializedName("location_tag_ids")
        var locationTagIds: ArrayList<Long>? = null

)