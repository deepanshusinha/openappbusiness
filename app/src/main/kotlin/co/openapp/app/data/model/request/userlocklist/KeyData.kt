package co.openapp.app.data.model.request.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class KeyData(

        @SerializedName("id")
        var id: Long? = null,

        @SerializedName("accessToken")
        var accessToken: String? = null,

        @SerializedName("keyStatus")
        var keyStatus: String? = null,

        @SerializedName("lockId")
        var lockId: Int? = null,

        @SerializedName("keyId")
        var keyId: Int? = null,

        @SerializedName("isAdmin")
        var isAdmin: Boolean? = null,

        @SerializedName("lockVersion")
        var lockVersion: String? = null,

        @SerializedName("lockName")
        var lockName: String? = null,

        @SerializedName("lockAlias")
        var lockAlias: String? = null,

        @SerializedName("lockMac")
        var lockMac: String? = null,

        @SerializedName("battery")
        var battery: Int? = null,

        @SerializedName("lockFlagPos")
        var lockFlagPos: Int? = null,

        @SerializedName("adminPs")
        var adminPs: String? = null,

        @SerializedName("unlockKey")
        var unlockKey: String? = null,

        @SerializedName("adminKeyboardPwd")
        var adminKeyboardPwd: String? = null,

        @SerializedName("deletePwd")
        var deletePwd: String? = null,

        @SerializedName("pwdInfo")
        var pwdInfo: String? = null,

        @SerializedName("timestamp")
        var timestamp: Long? = null,

        @SerializedName("aesKeystr")
        var aesKeystr: String? = null,

        @SerializedName("startDate")
        var startDate: Long? = null,

        @SerializedName("endDate")
        var endDate: Long? = null,

        @SerializedName("specialValue")
        var specialValue: Int? = null,

        @SerializedName("timezoneRawOffset")
        var timezoneRawOffset: Int? = null,

        @SerializedName("modelNumber")
        var modelNumber: String? = null,

        @SerializedName("hardwareRevision")
        var hardwareRevision: String? = null,

        @SerializedName("firmwareRevision")
        var firmwareRevision: String? = null
)