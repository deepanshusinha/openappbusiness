package co.openapp.app.data.model.request.editaccess

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by abhijeethallur on 10,July,2018
 */
data class Schedule (

        @SerializedName("start_date")
        var startDate: Long? = null,

        @SerializedName("end_date")
        var endDate: Long? = null,

        @SerializedName("all_day")
        var allDay: Boolean? = null,

        @SerializedName("end_type")
        var endType: String? = null,

        @SerializedName("repeat_unit")
        var repeatUnit: String? = null,

        @SerializedName("repeat_value")
        var repeatValue: Int? = 0,

        @SerializedName("repeat_on")
        var repeatOn: ArrayList<Int>? = null,

        @SerializedName("end_value")
        var endValue: Long? = null,

        @SerializedName("occurrences")
        var occurrences: Int? = 0

) : Serializable