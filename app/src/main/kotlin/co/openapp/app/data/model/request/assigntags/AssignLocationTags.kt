package co.openapp.app.data.model.request.assigntags

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class AssignLocationTags(

        @SerializedName("location_tag_ids")
        var locationTagIds: ArrayList<Long>? = null
)