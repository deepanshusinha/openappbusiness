package co.openapp.app.data.model.response.guestprofile

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
open class GuestSchedule {

    @SerializedName("start_date")
    open var startDate: Long? = null

    @SerializedName("end_date")
    open var endDate: Long? = null

    @SerializedName("all_day")
    open var allDay: Boolean? = null

    @SerializedName("end_type")
    open var endType: String? = null

    @SerializedName("repeat_unit")
    open var repeatUnit: String? = null

    @SerializedName("repeat_value")
    open var repeatValue: Int? = 0

    @SerializedName("repeat_on")
    open var repeatOn: List<Int>? = null

    @SerializedName("end_value")
    open var endValue: Long? = null

    @SerializedName("occurrences")
    open var occurrences: Int? = 0

}