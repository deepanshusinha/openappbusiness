package co.openapp.app.data.model.response.registerlock

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class ProductData (

        @SerializedName("product_id")
        val productId: String? = null,

        @SerializedName("manufacturer")
        val manufacturer: String? = null
)