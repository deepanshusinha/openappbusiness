package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class PadlockPwdUpdate(
        @SerializedName("mac_address") val macAddress: String, // 4
        @SerializedName("passcode") val passcode: String // 32423
)