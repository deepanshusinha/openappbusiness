package co.openapp.app.data.model.request.registerlock

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class RegisterLockRequest(

        @SerializedName("product_id")
        val productId: String? = null,

        @SerializedName("name")
        val name: String? = null,

        @SerializedName("location")
        val location: String? = null,

        @SerializedName("master_key")
        val masterKey: String? = null,

        @SerializedName("tag_id_array")
        var tagIdArray: ArrayList<Long>? = null
)