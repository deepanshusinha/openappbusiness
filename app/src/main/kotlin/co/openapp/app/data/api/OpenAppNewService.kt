package co.openapp.app.data.api

import androidx.lifecycle.LiveData
import co.openapp.app.data.model.request.addadmin.AddAdminData
import co.openapp.app.data.model.request.addlocationtag.AddLocationTags
import co.openapp.app.data.model.request.assigntags.AssignLocationTags
import co.openapp.app.data.model.auth.AuthFields
import co.openapp.app.data.model.request.deleteaccess.DeleteAccessRequest
import co.openapp.app.data.model.request.deletelocation.DeleteLocationRequest
import co.openapp.app.data.model.request.deletelock.DeleteLockRequest
import co.openapp.app.data.model.request.editaccess.EditAccessRequest
import co.openapp.app.data.model.request.editassignedtag.EditAssignedTagRequest
import co.openapp.app.data.model.request.editmastercode.EditMasterCodeReq
import co.openapp.app.data.model.request.firebasetoken.FirebaseTokenRequest
import co.openapp.app.data.model.request.forgotpassword.ResetPasswordRequest
import co.openapp.app.data.model.request.invite.InviteGuestRequest
import co.openapp.app.data.model.request.lockactivities.LockActivity
import co.openapp.app.data.model.request.otp.OtpFields
import co.openapp.app.data.model.request.refreshtoken.RefreshAccessTokenReq
import co.openapp.app.data.model.request.register.CheckUserRequest
import co.openapp.app.data.model.request.register.RegisterRequest
import co.openapp.app.data.model.request.registerlock.RegisterLockRequest
import co.openapp.app.data.model.request.resendverificationmail.ResendVerificationMailRequest
import co.openapp.app.data.model.response.addadmin.AddAdminResponse
import co.openapp.app.data.model.response.addlocationtag.AddLocationTagResponse
import co.openapp.app.data.model.response.assigntags.AssignTagsResponse
import co.openapp.app.data.model.response.countries.CountriesResponse
import co.openapp.app.data.model.response.deeplinking.DeepLinkResponse
import co.openapp.app.data.model.response.deleteaccess.DeleteAccessResponse
import co.openapp.app.data.model.response.deleteassignedtag.DeleteAssignedTagResponse
import co.openapp.app.data.model.response.deletelocation.DeleteLocationResponse
import co.openapp.app.data.model.response.deletelock.delete.DeleteLockResponse
import co.openapp.app.data.model.response.deletelock.status.LockDeleteStatusResponse
import co.openapp.app.data.model.response.deleteuser.DeleteUserResponse
import co.openapp.app.data.model.response.editaccess.EditAccessResponse
import co.openapp.app.data.model.response.editassignedtag.EditAssignedTagResponse
import co.openapp.app.data.model.response.editlocation.EditLocationResponse
import co.openapp.app.data.model.response.editlocation.LocationTag
import co.openapp.app.data.model.response.editmastercode.EditMasterCodeResponse
import co.openapp.app.data.model.response.firebasetoken.FirebaseTokenResponse
import co.openapp.app.data.model.response.forgotpassword.ResetPasswordResponse
import co.openapp.app.data.model.response.getkeys.UserLockKeysResponse
import co.openapp.app.data.model.response.guestprofile.GuestProfileResponse
import co.openapp.app.data.model.response.invite.InvitationResponse
import co.openapp.app.data.model.response.lockactivities.LockActivitiesResponse
import co.openapp.app.data.model.response.lockactivities.OperateLogUploadResponse
import co.openapp.app.data.model.response.logout.LogoutResponse
import co.openapp.app.data.model.response.masterkey.GetMasterKeyResponse
import co.openapp.app.data.model.response.myguests.MyGuestsResponse
import co.openapp.app.data.model.response.myplaces.MyPlacesResponse
import co.openapp.app.data.model.response.myprofile.MyProfileResponse
import co.openapp.app.data.model.response.profilepicture.ProfilePicUploadResponse
import co.openapp.app.data.model.response.recentusers.RecentUsersResponse
import co.openapp.app.data.model.response.refreshtoken.RefreshAccessTokenResponse
import co.openapp.app.data.model.response.register.CheckUserResponse
import co.openapp.app.data.model.response.register.RegisterResponse
import co.openapp.app.data.model.response.registerlock.ProductInfoResponse
import co.openapp.app.data.model.response.registerlock.RegisterLockResponse
import co.openapp.app.data.model.response.registerotp.OtpResponse
import co.openapp.app.data.model.response.resendverificationmail.ResendVerificationMailResponse
import co.openapp.app.data.model.response.servertime.ServerTime
import co.openapp.app.data.model.response.userlocklist.UserLockListResponse
import co.openapp.app.data.model.sync.SyncData
import co.openapp.app.data.model.sync.SyncResponse
import co.openapp.app.data.model.user.ProfileResponse
import co.openapp.app.data.model.general.GeneralResponse
import co.openapp.app.data.model.lock.*
import co.openapp.app.data.model.request.KeyRequest
import co.openapp.app.data.vo.AuthResponse
import co.openapp.app.data.vo.LockResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface OpenAppNewService {

    @POST("/api/v1/auth/login-business")
    fun callLoginApi(@Body authFields: AuthFields): LiveData<ApiResponse<AuthResponse>>

    @POST("/api/v1/auth/signup-business")
    fun callRegisterApi(@Body authFields: AuthFields): LiveData<ApiResponse<AuthResponse>>

    @POST("/api/v1/auth/app/forgot-password")
    fun callForgotPasswordApi(@Body authFields: AuthFields): LiveData<ApiResponse<AuthResponse>>

    @POST("/api/v1/auth/app/change-password")
    fun callChangePasswordApi(@Body authFields: AuthFields): LiveData<ApiResponse<AuthResponse>>

    @POST("api/v1/auth/otp/verify")
    fun callOtpVerifyApi(@Body otpFields: OtpFields): LiveData<ApiResponse<AuthResponse>>

    @POST("api/v1/auth/otp/resend")
    fun callOtpResendApi(@Body otpFields: OtpFields): LiveData<ApiResponse<AuthResponse>>

    @POST("/api/refresh_token")
    fun callRefreshAccessToken(@Body request: RefreshAccessTokenReq): Call<RefreshAccessTokenResponse>

    @GET("/api/v1/business/app/locks")
    fun callListApi(): LiveData<ApiResponse<LockResponse>>

    @POST("/api/v1.1/business/app/activities/sync")
    fun callSyncListApi(@Body syncData: List<SyncData>): LiveData<ApiResponse<SyncResponse>>

    @GET("/api/v1/profile")
    fun callProfileApi(): LiveData<ApiResponse<ProfileResponse>>

    @POST("/api/v1/profile/change-password")
    fun callProfileChangePasswordApi(@Body authFields: AuthFields): LiveData<ApiResponse<AuthResponse>>

    @GET("/api/v1/business/app/general")
    fun callGeneralApi(): LiveData<ApiResponse<GeneralResponse>>

    @POST("/api/v1/business/app/doorlock/onboard")
    fun callInitLockApi(@Body keyData: KeyRequest): LiveData<ApiResponse<SyncResponse>>

    @POST("/api/v1/business/app/tripsheet/verify-otp")
    fun callTripSheetOtpApi(@Body verifyTripOtp: VerifyTripOtp): LiveData<ApiResponse<TripSheetResponse>>

    @POST("/api/v1/business/app/tripsheet/verify-tripid")
    fun callTripSheetIdApi(@Body verifyTripId: VerifyTripId): LiveData<ApiResponse<TripSheetResponse>>

    @POST("/api/v1/business/app/doorlock/{lockId}/passcode")
    fun callGeneratePassCodeApi(@Body passCode: PassCode, @Path("lockId") lockId: String): LiveData<ApiResponse<PassCodeResponse>>

    @Headers("Accept: application/json")
    @GET
    fun genericFirebaseCall(@Url url: String): Single<DeepLinkResponse>

    @POST("/api/verify_email")
    fun callResendVerificationMail(@Body body: ResendVerificationMailRequest): Single<ResendVerificationMailResponse>

    @POST("/api/users")
    fun callRegisterApi(@Body body: RegisterRequest): Single<RegisterResponse>

    @FormUrlEncoded
    @POST("/api/otp")
    fun callRequestMobileOtpApi(@Field("mobile") mobile: String, @Field("country_id") countryId: Int): Single<OtpResponse>

    @GET("/api/countries")
    fun callCountriesApi(): Single<CountriesResponse>

    @POST("/api/user/change_password")
    fun callResetPasswordApi(@Body resetPasswordRequest: ResetPasswordRequest): Single<ResetPasswordResponse>

    @GET("/api/user/{id}/locks")
    fun callGetUserLocks(@Path("id") userId: Long): Single<UserLockListResponse>

    @POST("/api/lock/{id}/setup")
    fun callAddAdminToLock(@Path("id") lockId: Long, @Body addAdminData: AddAdminData): Single<AddAdminResponse>

    @POST("/api/user/{id}/locks")
    fun callRegisterLockApi(@Path("id") userId: Long, @Body registerLockRequest: RegisterLockRequest): Single<RegisterLockResponse>

    @GET("/api/user/{id}/keys")
    fun callGetKeysApi(@Path("id") userId: Long): Single<UserLockKeysResponse>

    @POST("/api/invitations")
    fun callInvitationsApi(@Body inviteGuestRequest: InviteGuestRequest): Single<InvitationResponse>

    @POST("/api/activities")
    fun callUploadActivitiesApi(@Body lockActivityList: ArrayList<LockActivity>): Single<OperateLogUploadResponse>

    @GET("/api/user/{id}/activities")
    fun callGetUserLocksActivities(@Path("id") userId: Long, @Query("page") page: Int,
                                   @Query("limit") limit: Int): Single<LockActivitiesResponse>

    @GET("/api/user/{id}")
    fun callGetUserProfile(@Path("id") userId: Long): Single<MyProfileResponse>

    @Multipart
    @PUT("/api/user/{id}")
    fun callUploadProfilePicture(@Part image: MultipartBody.Part, @Path("id") userId: Long): Single<ProfilePicUploadResponse>

    @Multipart
    @PUT("/api/user/{id}")
    fun callUpdateMyProfile(@Part image: MultipartBody.Part?, @Path("id") userId: Long,
                            @Part("first_name") fName: RequestBody, @Part("last_name") lName: RequestBody,
                            @Part("old_password") oldPassword: RequestBody, @Part("password") newPassword: RequestBody): Single<ProfilePicUploadResponse>

    @GET("/api/user/{id}/my_users")
    fun callGetMyGuestsList(@Path("id") userId: Long): Single<MyGuestsResponse>

    @GET("/api/user/{id}/profile")
    fun callGetGuestProfile(@Path("id") guestId: Long, @Query("owner") ownerId: Long): Single<GuestProfileResponse>

    @PUT("/api/lock/{id}")
    fun callEditMasterCode(@Path("id") lockId: Long, @Body editMasterCodeReq: EditMasterCodeReq): Single<EditMasterCodeResponse>

    @PUT("/api/lock/{id}/access")
    fun callEditAccess(@Path("id") lockId: Long, @Body editAccessRequest: EditAccessRequest): Single<EditAccessResponse>

    @GET("/api/user/{id}/lock/{lid}/delete_status")
    fun callGetLockDeleteStatus(@Path("id") userId: Long, @Path("lid") lockId: Long): Single<LockDeleteStatusResponse>

    @POST("/api/user/{id}/lock/{lid}")
    fun callDeleteLock(@Path("id") userId: Long, @Path("lid") lockId: Long, @Body deleteLockRequest: DeleteLockRequest): Single<DeleteLockResponse>

    @GET("/api/server_time")
    fun callGetServerTime(): Single<ServerTime>

    @DELETE("/api/my_users/{id}")
    fun callDeleteUser(@Path("id") userId: Long): Single<DeleteUserResponse>

    @POST("/api/lock/{id}/users")
    fun callDeleteAccess(@Path("id") lockId: Long, @Body deleteAccessRequest: DeleteAccessRequest): Single<DeleteAccessResponse>

    @PUT("/api/user/{id}/device_token")
    fun callUpdateDeviceToken(@Path("id") userId: Long?, @Body firebaseTokenRequest: FirebaseTokenRequest): Single<FirebaseTokenResponse>

    @POST("/api/logout")
    fun callLogoutApi(): Single<LogoutResponse>

    @GET("/api/lock/{id}/master_key")
    fun callGetMasterKeyApi(@Path("id") lockId: Long): Single<GetMasterKeyResponse>

    @GET("/api/lock/{id}/recent_users")
    fun callGetRecentUsers(@Path("id") lockId: Long): Single<RecentUsersResponse>

    @POST("/api/check-user")
    fun callCheckUserForRegistration(@Body checkUserRequest: CheckUserRequest): Single<CheckUserResponse>

    @GET("/api/product/{id}/info")
    fun callGetProductInfo(@Path("id") productId: String): Single<ProductInfoResponse>

    @GET("/api/user/{id}/my_places")
    fun callGetMyPlaces(@Path("id") userId: Long): Single<MyPlacesResponse>

    @POST("/api/user/{id}/location_tags")
    fun callAddLocationTag(@Path("id") userId: Long, @Body addLocationTags: AddLocationTags): Single<AddLocationTagResponse>

    @PUT("/api/user/{id}/location_tag/{ltid}")
    fun callEditLocationTag(@Path("id") userId: Long, @Path("ltid") location_tag_id: Long, @Body editLocationTag: LocationTag): Single<EditLocationResponse>

    @POST("/api/user/{id}/location_tag")
    fun callDeleteLocationTag(@Path("id") userId: Long, @Body deleteLocationRequest: DeleteLocationRequest): Single<DeleteLocationResponse>

    @POST("/api/lock/{id}/location_tags")
    fun callAssignTagsToLock(@Path("id") lockId: Long, @Body assignLocationTags: AssignLocationTags): Single<AssignTagsResponse>

    @PUT("/api/lock/{id}/location_tag/{ltid}")
    fun callEditAssignedTag(@Path("id") lockId: Long, @Path("ltid") locationTagId: Long, @Body editAssignedTagRequest: EditAssignedTagRequest): Single<EditAssignedTagResponse>

    @DELETE("/api/lock/{id}/location_tag/{ltid}")
    fun callDeleteAssignedTag(@Path("id") lockId: Long, @Path("ltid") locationTagId: Long): Single<DeleteAssignedTagResponse>
}