package co.openapp.app.data.model.response.deletelock.delete

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 02,August,2018
 */
class DeleteLockResponse (

        @SerializedName("data")
        val data: Boolean? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)