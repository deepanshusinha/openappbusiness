package co.openapp.app.data.model.response.myprofile

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 26,July,2018
 */
open class ProfileData {

    @SerializedName("id")
    open var id: Long? = null

    @SerializedName("role")
    open var role: String? = null

    @SerializedName("email")
    open var email: String? = null

    @SerializedName("first_name")
    open var firstName: String? = null

    @SerializedName("last_name")
    open var lastName: String? = null

    @SerializedName("access_token")
    open var accessToken: String? = null

    @SerializedName("mobile")
    open var mobile: String? = null

    @SerializedName("status")
    open var status: String? = null

    @SerializedName("email_verified")
    open var emailVerified: Boolean? = null

    @SerializedName("mobile_verified")
    open var mobileVerified: Boolean? = null

    @SerializedName("jwt_version")
    open var jwtVersion: Int? = null

    @SerializedName("dp")
    open var dp: String? = null

    @SerializedName("created_at")
    open var createdAt: String? = null

    @SerializedName("name")
    open var name: String? = null
}