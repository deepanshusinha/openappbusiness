package co.openapp.app.data.model.response.deleteaccess

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 02,August,2018
 */
class DeleteAccessResponse (

        @SerializedName("data")
        val data: Boolean? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)