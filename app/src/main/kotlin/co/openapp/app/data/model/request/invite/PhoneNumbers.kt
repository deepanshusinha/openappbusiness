package co.openapp.app.data.model.request.invite

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by abhijeethallur on 10,July,2018
 */
data class PhoneNumbers(

        @SerializedName("mobile")
        var mobile: String? = null,

        @SerializedName("country_id")
        var countryId: Int? = null

) : Serializable