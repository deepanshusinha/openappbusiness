package co.openapp.app.data.vo

import co.openapp.app.data.model.lock.LockData
import com.google.gson.annotations.SerializedName

data class LockResponse(
        @SerializedName("data")
        val data: List<LockData>,
        @SerializedName("status")
        val status: Boolean, // true
        @SerializedName("message")
        val message: String // true
)