package co.openapp.app.data.model.response.addlocationtag

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */

data class AddLocationTagResponse(
        @SerializedName("code")
        var id: Int? = 0,

        @SerializedName("message")
        var message: String? = null,

        @SerializedName("data")
        var data: ArrayList<LocationTag>? = null
)