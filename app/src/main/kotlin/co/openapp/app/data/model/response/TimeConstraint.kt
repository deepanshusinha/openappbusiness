package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class TimeConstraint(
        @SerializedName("start_date") val startDate: String, // UTC Time
        @SerializedName("end_date") val endDate: String // UTC Time
)