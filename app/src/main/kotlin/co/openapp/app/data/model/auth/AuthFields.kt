package co.openapp.app.data.model.auth

import com.google.gson.annotations.SerializedName

data class AuthFields(
        @field:SerializedName("email")
        var email: String = "".trim(),
        @field:SerializedName("password")
        var password: String = "".trim(),
        @Transient var rePassword: String = "".trim(),
        @field:SerializedName("firstName")
        var firstName: String = "".trim(),
        @field:SerializedName("lastName")
        var lastName: String = "".trim(),
        @Transient var fullName: String = "$firstName $lastName",
        @field:SerializedName("phone")
        var phoneNumber: String = "".trim(),
        @Transient var rememberMe: Boolean = false,
        @field:SerializedName("countryCode")
        var countryCode: String = "91".trim(),
        @field:SerializedName("companyId")
        var companyId: String = "".trim(),
        @field:SerializedName("userName")
        var userName: String = "".trim(),
        @field:SerializedName("token")
        var token: String = "".trim(),
        @field:SerializedName("otp")
        var otp: String = "".trim(),
        @SerializedName("newPassword")
        var newPassword: String = "".trim(), // asdfgh123
        @SerializedName("oldPassword")
        var oldPassword: String = "".trim() // asdf123
)