package co.openapp.app.data.model.response.forgotpassword

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 13,June,2018
 */
class ResetPasswordResponse(

        @SerializedName("data")
        val data: Boolean? = false,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)