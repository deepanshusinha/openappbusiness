package co.openapp.app.data.model.response.deeplinking

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 06,June,2018
 */
data class DeepLinkResponse(

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)