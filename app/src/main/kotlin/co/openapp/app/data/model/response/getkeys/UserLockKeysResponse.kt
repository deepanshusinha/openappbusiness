package co.openapp.app.data.model.response.getkeys

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27,June,2018
 */
data class UserLockKeysResponse(

        @SerializedName("data")
        val keyData: ArrayList<KeyData>? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)