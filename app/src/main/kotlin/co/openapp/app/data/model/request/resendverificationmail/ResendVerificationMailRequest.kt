package co.openapp.app.data.model.request.resendverificationmail

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class ResendVerificationMailRequest(

        @SerializedName("event")
        var event: String? = null,

        @SerializedName("mobile")
        var mobile: String? = null,

        @SerializedName("country_id")
        var countryId: Int? = null,

        @SerializedName("email")
        var email: String? = null
)