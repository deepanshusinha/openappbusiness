package co.openapp.app.data.model.response.myguests

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27July2018
 */
open class GuestData {

    @SerializedName("id")
    open var id: Long? = null

    @SerializedName("access_level")
    open var accessLevel: String? = null

    @SerializedName("user")
    open var myUser: MyUser? = null

    open var itemType: Int? = null

    open var headerText: String? = null

}