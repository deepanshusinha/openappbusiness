package co.openapp.app.data.model.response.refreshtoken

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,July,2018
 */
data class Data(
        @SerializedName("access_token")
        var accessToken: String? = null
)