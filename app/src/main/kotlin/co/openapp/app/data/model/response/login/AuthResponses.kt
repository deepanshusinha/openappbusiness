package co.openapp.app.data.model.response.login

import com.google.gson.annotations.SerializedName

data class AuthResponses(

        @SerializedName("data")
        val data: Data? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)