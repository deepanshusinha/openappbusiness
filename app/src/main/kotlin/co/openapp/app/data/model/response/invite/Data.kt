package co.openapp.app.data.model.response.invite

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 10,July,2018
 */
data class Data(

        @SerializedName("successes")
        val successes: ArrayList<InviteItem>? = null,

        @SerializedName("failures")
        val failures: ArrayList<InviteItem>? = null
)