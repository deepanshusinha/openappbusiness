package co.openapp.app.data.model.request.updatemyprofile

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 01,August,2018
 */
data class UpdateMyProfileReq (
        @SerializedName("first_name")
        var firstName: String? = null,

        @SerializedName("last_name")
        var lastName: String? = null
)