package co.openapp.app.data.model.response.deletelock.status

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 02,August,2018
 */
data class DeleteStatus(

        @SerializedName("owner")
        val ownerCount: Int? = null,

        @SerializedName("co_owner")
        val coOwnerCount: Int? = null,

        @SerializedName("guests")
        val guestsCount: Int? = null,

        @SerializedName("reset")
        val reset: Boolean? = null

)