package co.openapp.app.data.model.request.forgotpassword

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by abhijeethallur on 13,June,2018
 */
data class ForgotPasswordRequest(

        @SerializedName("mode")
        var mode: String? = null,

        @SerializedName("mobile")
        var mobile: String? = null,

        @SerializedName("email")
        var email: String? = null,

        @SerializedName("country_id")
        var countryId: Int? = 0,

        var telephoneCode: String? = null

) : Serializable