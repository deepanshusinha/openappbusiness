package co.openapp.app.data.model.response.countries

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 06,June,2018
 */
open class LanguageData {

    @SerializedName("id")
    open var id: Int = 0

    @SerializedName("name")
    open var languageName: String? = null

    @SerializedName("status")
    open var status: String? = null

    @SerializedName("localized_text")
    open var localizedText: String? = null
}