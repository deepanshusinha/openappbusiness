package co.openapp.app.data.model.response.guestprofile

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 30,July,2018
 */
open class UserAccessData {

    @SerializedName("id")
    open var id: Int? = 0

    @SerializedName("status")
    open var status: String? = null

    @SerializedName("granted")
    open var granted: Boolean? = null

    @SerializedName("granted_on")
    open var grantedOn: String? = null

    @SerializedName("user")
    open var guestUser: GuestUser? = null

    @SerializedName("access_level")
    open var accessLevel: String? = null

    @SerializedName("schedule_type")
    open var scheduleType: String? = null

    @SerializedName("schedule")
    open var guestSchedule: GuestSchedule? = null

    @SerializedName("provider_user_id")
    open var providerUserId: Long? = null

    @SerializedName("provider_lock_id")
    open var providerLockId: Long? = null

    @SerializedName("activity_count")
    open var activityCount: Long? = null

    @SerializedName("notifications")
    open var notification: Boolean = false

    @SerializedName("notif_recievers")
    open var notificationReceivers: List<Long>? = null

    @SerializedName("lock")
    open var guestLock: GuestLock? = null

}