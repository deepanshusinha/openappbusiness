package co.openapp.app.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import androidx.room.Transaction
import co.openapp.app.data.model.user.UserData

/**
 * Interface for database access on Lock related operations.
 */
@Dao
abstract class UserDao : BaseDao<UserData> {

    @Transaction
    open fun saveProfileDetail(item: UserData) {
        delete(item)
        insert(item)
    }

    @Query("SELECT * FROM user")
    abstract fun fetchProfileFromDb(): LiveData<List<UserData>>

    @Query("DELETE FROM user")
    abstract fun deleteAll()
}