package co.openapp.app.data.model.response.guestprofile

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 30,July,2018
 */
open class GuestUser {

    @SerializedName("id")
    open var id: Long? = 0

    @SerializedName("role")
    open var role: String? = null

    @SerializedName("email")
    open var email: String? = null

    @SerializedName("first_name")
    open var firstName: String? = null

    @SerializedName("last_name")
    open var lastName: String? = null

    @SerializedName("mobile")
    open var mobile: String? = null

    @SerializedName("status")
    open var status: String? = null

    @SerializedName("email_verified")
    open var emailVerified: Boolean? = null

    @SerializedName("mobile_verified")
    open var mobileVerified: Boolean? = null

    @SerializedName("jwt_version")
    open var jwtVersion: Int? = 0

    @SerializedName("dp")
    open var dp: String? = null

    @SerializedName("name")
    open var name: String? = null

    @SerializedName("created_at")
    open var createdAt: String? = null

    @SerializedName("org_id")
    open var orgId: String? = null

    @SerializedName("hierarchy_id")
    open var hierarchyId: String? = null

}