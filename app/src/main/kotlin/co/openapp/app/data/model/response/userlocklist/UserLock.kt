package co.openapp.app.data.model.response.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
open class UserLock  {

    @SerializedName("id")
    open var id: Int? = 0

    @SerializedName("status")
    open var status: String? = null

    @SerializedName("granted")
    open var granted: Boolean? = null

    @SerializedName("granted_on")
    open var grantedOn: String? = null

    @SerializedName("user")
    open var user: User? = null

    @SerializedName("granted_by")
    open var grantedBy: GrantedBy? = null

    @SerializedName("lock")
    open var lockData: LockData? = null

    @SerializedName("access_level")
    open var accessLevel: String? = null

    @SerializedName("updated_at")
    open var updatedAt: Long? = null

    @SerializedName("schedule_type")
    open var scheduleType: String? = null

    @SerializedName("schedule")
    open var schedule: Schedule? = null

    @SerializedName("isSelected")
    open var isSelected: Boolean = false

}