package co.openapp.app.data.db


/**
 * Created by deepanshusinha on 14/02/18.
 */

data class Country(
        var name: String = "",

        var code: String = ""
)