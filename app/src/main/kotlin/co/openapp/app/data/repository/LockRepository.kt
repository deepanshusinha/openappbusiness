package co.openapp.app.data.repository

import androidx.lifecycle.LiveData
import co.openapp.app.data.api.ApiResponse
import co.openapp.app.data.api.OpenAppNewService
import co.openapp.app.data.db.LockDao
import co.openapp.app.data.db.Records
import co.openapp.app.data.model.lock.*
import co.openapp.app.data.model.request.DoorLock
import co.openapp.app.data.model.request.KeyRequest
import co.openapp.app.data.model.sync.*
import co.openapp.app.data.prefs.OpenAppPreferences
import co.openapp.app.data.prefs.OpenAppPreferencesLiveData
import co.openapp.app.data.source.NetworkBoundResource
import co.openapp.app.data.source.Resource
import co.openapp.app.data.vo.LockResponse
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.Utils
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.schedulers.AppExecutors
import com.google.gson.reflect.TypeToken
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Repository that handles Locks.
 */
@Singleton
class LockRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val lockDao: LockDao,
        private val preferences: OpenAppPreferences,
        livePreference: OpenAppPreferencesLiveData,
        private val appService: OpenAppNewService,
        utils: Utils
) : AppRepository(utils, preferences, livePreference) {

    fun loadLockList(forceLoad: Boolean): LiveData<Resource<List<LockData>>> {
        return object : NetworkBoundResource<List<LockData>, LockResponse>(appExecutors) {
            override fun saveCallResult(item: LockResponse) {
                preferences.save(Constants.SharedKeys.DATA_SAVED_TIME, AppSnippet.currDateEpochSecond)
                // Clear Remaining Access
                preferences.getAllKeys()?.keys?.forEach { key ->
                    if (key.startsWith(Constants.SharedKeys.RULE_STARTS_WITH)) {
                        preferences.clearData(key)
                    }
                }

                val data = item.data

                data.forEach { lock ->
                    saveLockStatus(lock.id, lock.currentState.equals("Unlocked", true))
                }

                // Update Lock List
                lockDao.updateLockAll(data)
            }

            override fun shouldFetch(data: List<LockData>?): Boolean {
                return data == null || data.isEmpty() || forceLoad || isForceOnline(AppSnippet.currDateEpochSecond)
            }

            override fun loadFromDb(): LiveData<List<LockData>> {
                return lockDao.fetchLockFromDb()
            }

            override fun createCall(): LiveData<ApiResponse<LockResponse>> {
                return appService.callListApi()
            }
        }.asLiveData()
    }

    fun pushSyncData(): LiveData<Resource<SyncResponse>> {
        return object : NetworkBoundResource<SyncResponse, SyncResponse>(appExecutors) {
            private var synDataResponse = SyncResponse()

            override fun saveCallResult(item: SyncResponse) {
                synDataResponse = item
                if (item.status) {
                    clearLogData()
                }
            }

            override fun shouldFetch(data: SyncResponse?): Boolean {
                return getAccessDataList().isNotEmpty()
            }

            override fun loadFromDb(): LiveData<SyncResponse> {
                return object : LiveData<SyncResponse>() {
                    override fun onActive() {
                        super.onActive()
                        value = synDataResponse
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<SyncResponse>> {
                return appService.callSyncListApi(getAccessDataList())
            }
        }.asLiveData()
    }

    fun pushLockData(lockData: KeyRequest): LiveData<Resource<SyncResponse>> {
        return object : NetworkBoundResource<SyncResponse, SyncResponse>(appExecutors) {
            private var synDataResponse = SyncResponse()

            override fun saveCallResult(item: SyncResponse) {
                synDataResponse = item
            }

            override fun shouldFetch(data: SyncResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<SyncResponse> {
                return object : LiveData<SyncResponse>() {
                    override fun onActive() {
                        super.onActive()
                        value = synDataResponse
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<SyncResponse>> {
                return appService.callInitLockApi(lockData)
            }
        }.asLiveData()
    }

    fun verifyTripSheetOtp(verifyTripOtp: VerifyTripOtp): LiveData<Resource<TripSheetResponse>> {
        return object : NetworkBoundResource<TripSheetResponse, TripSheetResponse>(appExecutors) {
            private var synDataResponse = TripSheetResponse()

            override fun saveCallResult(item: TripSheetResponse) {
                synDataResponse = item
            }

            override fun shouldFetch(data: TripSheetResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<TripSheetResponse> {
                return object : LiveData<TripSheetResponse>() {
                    override fun onActive() {
                        super.onActive()
                        value = synDataResponse
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<TripSheetResponse>> {
                return appService.callTripSheetOtpApi(verifyTripOtp)
            }
        }.asLiveData()
    }

    fun verifyTripSheetId(verifyTripId: VerifyTripId): LiveData<Resource<TripSheetResponse>> {
        return object : NetworkBoundResource<TripSheetResponse, TripSheetResponse>(appExecutors) {
            private var synDataResponse = TripSheetResponse()

            override fun saveCallResult(item: TripSheetResponse) {
                synDataResponse = item
            }

            override fun shouldFetch(data: TripSheetResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<TripSheetResponse> {
                return object : LiveData<TripSheetResponse>() {
                    override fun onActive() {
                        super.onActive()
                        value = synDataResponse
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<TripSheetResponse>> {
                return appService.callTripSheetIdApi(verifyTripId)
            }
        }.asLiveData()
    }

    fun generatePassCode(passCode: PassCode, lockId: String): LiveData<Resource<PassCodeResponse>> {
        return object : NetworkBoundResource<PassCodeResponse, PassCodeResponse>(appExecutors) {
            private var passCodeDataResponse = PassCodeResponse()

            override fun saveCallResult(item: PassCodeResponse) {
                passCodeDataResponse = item
            }

            override fun shouldFetch(data: PassCodeResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<PassCodeResponse> {
                return object : LiveData<PassCodeResponse>() {
                    override fun onActive() {
                        super.onActive()
                        value = passCodeDataResponse
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<PassCodeResponse>> {
                return appService.callGeneratePassCodeApi(passCode, lockId)
            }
        }.asLiveData()
    }

    fun isForceOnline(time: Long): Boolean {
        val shouldFetch: Boolean
        val savedTime: Long
        if (preferences.isKeyShared(Constants.SharedKeys.FRESH_TIMEOUT_IN_HOURS) &&
                preferences.getLong(Constants.SharedKeys.FRESH_TIMEOUT_IN_HOURS, 24) < 0) {
            shouldFetch = false
        } else if (preferences.isKeyShared(Constants.SharedKeys.DATA_SAVED_TIME)) {
            savedTime = preferences.getLong(Constants.SharedKeys.DATA_SAVED_TIME)
            shouldFetch = time - savedTime > preferences.getLong(Constants.SharedKeys.FRESH_TIMEOUT_IN_HOURS, 24)
        } else {
            shouldFetch = false
        }

        /*if (!hasConnection()) {
            shouldFetch = false
        }*/
        return shouldFetch
    }

    fun loadSaveLockList() = lockDao.fetchLockFromDb()

    fun fetchLock(macAddress: String?) = lockDao.fetchLockByMacAddress(macAddress)

    // Sync Data
    fun createSyncData(lockId: String) {
        val syncDataList = getAccessDataList()

        val syncRequest = SyncData()
        syncRequest.lockId = lockId
        syncRequest.attempted = AppSnippet.currDate
        val location = getCurrentLocation()
        if (location != null) {
            syncRequest.latitude = location.latitude
            syncRequest.longitude = location.longitude
        }
        syncDataList.add(syncRequest)

        saveAccessDataList(syncDataList)

        // Engaged Sync
        setSyncEngaged(true)
    }

    fun syncRuleError(timeRules: ArrayList<TimeRules>, locationRules: ArrayList<LocationRules>, tripSheetRules: ArrayList<TripSheetRules>, errorList: ArrayList<String>) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val syncData = accessDataList[lastItem()]
            syncData.timeRule = timeRules
            syncData.locationRule = locationRules
            syncData.tripSheetRule = tripSheetRules
            syncData.error = errorList

            accessDataList[lastItem()] = syncData
        }

        saveAccessDataList(accessDataList)
    }

    private fun saveAccessDataList(list: ArrayList<SyncData>) {
        preferences.save(Constants.SharedKeys.PUSH_DATA, list)
    }

    fun getAccessDataList(): ArrayList<SyncData> {
        val listType = object : TypeToken<ArrayList<SyncData>>() {}.type
        val arrayList = preferences.get<ArrayList<SyncData>>(Constants.SharedKeys.PUSH_DATA, listType)
        return if (arrayList != null && arrayList.size > 0) {
            arrayList
        } else {
            ArrayList()
        }
    }

    private fun lastItem(): Int {
        return getAccessDataList().size - 1
    }

    fun clearLogData() {
        preferences.clearData(Constants.SharedKeys.PUSH_DATA)
    }

    // Add or Set Door LockData
    private fun getRecords(records: String?): List<Records>? {
        val listType = object : TypeToken<List<Records>>() {}.type
        if (records != null) {
            return preferences.getObject<List<Records>>(records, listType)
        }
        return ArrayList()
    }

    private fun getDoorLockData(accessDataList: ArrayList<SyncData>): ArrayList<DoorLock> {
        val doorLock = accessDataList[lastItem()].doorLock

        return if (doorLock != null && doorLock.size > 0)
            doorLock
        else ArrayList()
    }

    fun addOrSaveDoorLock(records: String?) {
        val recordsList = getRecords(records)
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val doorLockData = getDoorLockData(accessDataList)

            recordsList?.forEach { record ->
                doorLockData.add(DoorLock(
                        AppSnippet.getDateStringFromMilliseconds(record.deleteDate),
                        AppSnippet.getDateStringFromMilliseconds(record.operateDate),
                        record.electricQuantity,
                        record.keyId,
                        record.recordType,
                        record.uid,
                        record.password,
                        record.newPassword
                ))
            }

            accessDataList[lastItem()].doorLock = doorLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun addOrSaveDoorLockError(errorMsg: String?) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val doorLockData = getDoorLockData(accessDataList)

            val doorLock = DoorLock()
            doorLock.error = errorMsg

            doorLockData.add(doorLock)

            accessDataList[lastItem()].doorLock = doorLockData
        }

        saveAccessDataList(accessDataList)
    }

    // PadLock LockData
    fun padLockConnectTime(time: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLock = PadLock()
            padLock.connected = time

            padLockData.add(padLock)

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun padLockBattery(value: Int) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            if (padLockLastItem >= 0)
                padLockData[padLockLastItem].battery = value

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun padLockToken(value: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            if (padLockLastItem >= 0)
                padLockData[padLockLastItem].token = value

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun padLockError(error: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLock = PadLock()
            padLock.error = error

            padLockData.add(padLock)

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun padLockDisconnectTime(time: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            if (padLockLastItem >= 0)
                padLockData[padLockLastItem].disconnected = time

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setAndAddOperateError(value: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            var operationsList = padLockData[padLockLastItem].operations
            if (operationsList == null)
                operationsList = ArrayList()

            val operationsLastItem = operationsList.size - 1
            val operations = if (operationsLastItem > 0) {
                operationsList[operationsLastItem]
            } else {
                Operations()
            }

            operations.error = value
            operationsList.add(operations)

            padLockData[padLockLastItem].operations = operationsList

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setAndAddOperateUnlockedSentTime(currDate: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            accessDataList[lastItem()].lockId?.let { saveLockStatus(it, true) }

            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            var operationsList = padLockData[padLockLastItem].operations
            if (operationsList == null)
                operationsList = ArrayList()

            val operations = Operations()

            val operateDate = OperateDate()
            operateDate.sent = currDate

            operations.operateDate = operateDate
            operations.status = "unlock"

            operationsList.add(operations)
            padLockData[padLockLastItem].operations = operationsList

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setAndAddOperateUnlockedReceivedTime(currDate: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            var operationsList = padLockData[padLockLastItem].operations
            if (operationsList == null)
                operationsList = ArrayList()
            val operationsLastItem = operationsList.size - 1

            var operateDate = operationsList[operationsLastItem].operateDate
            if (operateDate == null)
                operateDate = OperateDate()
            operateDate.received = currDate

            operationsList[operationsLastItem].operateDate = operateDate
            operationsList[operationsLastItem].status = "unlock"

            padLockData[padLockLastItem].operations = operationsList

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    fun setAndAddOperateLockedSentTime(currDate: String) {
        val accessDataList = getAccessDataList()

        if (lastItem() >= 0) {
            accessDataList[lastItem()].lockId?.let { saveLockStatus(it, false) }

            val padLockData = getPadLockData(accessDataList)
            val padLockLastItem = padLockData.size - 1

            var operationsList = padLockData[padLockLastItem].operations
            if (operationsList == null)
                operationsList = ArrayList()

            val operations = Operations()

            val operateDate = OperateDate()
            operateDate.sent = currDate

            operations.operateDate = operateDate
            operations.status = "lock"

            operationsList.add(operations)
            padLockData[padLockLastItem].operations = operationsList

            accessDataList[lastItem()].padLock = padLockData
        }

        saveAccessDataList(accessDataList)
    }

    private fun getPadLockData(accessDataList: ArrayList<SyncData>): ArrayList<PadLock> {
        val padLock = accessDataList[lastItem()].padLock

        return if (padLock != null && padLock.size > 0)
            padLock
        else ArrayList()
    }

    fun saveCurrentLocation(location: DeviceLocation) {
        preferences.save(Constants.SharedKeys.USER_LOCATION, location)
    }

    fun getCurrentLocation(): DeviceLocation? {
        val locationType = object : TypeToken<DeviceLocation>() {}.type
        return preferences.get<DeviceLocation>(Constants.SharedKeys.USER_LOCATION, locationType)
    }

    fun isSyncEngaged(): Boolean {
        return preferences.getBoolean(Constants.SharedKeys.SYNC)
    }

    fun setSyncEngaged(value: Boolean) {
        preferences.save(Constants.SharedKeys.SYNC, value)
    }

    fun saveRemainingAccess(key: String) {
        val remainingAccess = getRemainingAccess(key) + 1
        preferences.save("${Constants.SharedKeys.RULE_STARTS_WITH}$key", remainingAccess)
    }

    fun getRemainingAccess(key: String): Int = preferences.getInt("${Constants.SharedKeys.RULE_STARTS_WITH}$key")

    fun saveLockStatus(key: String, status: Boolean) {
        preferences.save(key, status)
    }

    fun isLockUnlocked(lockId: String): Boolean = preferences.getBoolean(lockId)
}