package co.openapp.app.data.model.response.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class UserLockListResponse(

        @SerializedName("code")
        var code : Int? = 0,

        @SerializedName("data")
        var Data: Data? = null,

        @SerializedName("message")
        var message: String? = null
)