package co.openapp.app.data.model.response.login

import com.google.gson.annotations.SerializedName

data class Data(

        @SerializedName("user")
        val loginUser: LoginUser? = null,

        @SerializedName("access_token")
        val access_token: String? = null,

        @SerializedName("first_login")
        val firstLogin: Boolean? = null,

        @SerializedName("refresh_token")
        val refreshToken: String? = null,

        @SerializedName("timezone_raw_offset")
        val timezoneRawOffset: Long? = null


)