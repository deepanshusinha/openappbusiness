package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class SecurityInfo(
        @SerializedName("passcode_id") val passcodeId: Int, //1
        @SerializedName("user_id") val userId: Int, //6
        @SerializedName("flag") val flag: Int, //1
        @SerializedName("passcode") val passcode: String, //3563
        @SerializedName("created") val created: String, //2018-05-25 16:10:03
        @SerializedName("name") val name: String //Jerin Kuriakose
)