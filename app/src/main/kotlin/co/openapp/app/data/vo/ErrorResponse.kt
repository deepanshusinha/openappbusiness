package co.openapp.app.data.vo

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("message")
        var message: String,
        @SerializedName("errorCode")
        var errorCode: Int
)