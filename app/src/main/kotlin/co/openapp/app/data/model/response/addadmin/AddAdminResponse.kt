package co.openapp.app.data.model.response.addadmin

import com.google.gson.annotations.SerializedName

data class AddAdminResponse(
        @SerializedName("code")
        var id: Int? = 0,

        @SerializedName("message")
        var message: String? = null,

        @SerializedName("data")
        var data: Boolean? = null
)