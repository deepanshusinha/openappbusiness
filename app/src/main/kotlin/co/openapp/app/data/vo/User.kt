/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.openapp.app.data.vo

import com.google.gson.annotations.SerializedName

data class User(
        @field:SerializedName("id")
        val id: String,
        @field:SerializedName("firstName")
        val firstName: String,
        @field:SerializedName("lastName")
        val lastName: String,
        @field:SerializedName("email")
        val email: String?,
        @field:SerializedName("phone")
        val phone: String?,
        @field:SerializedName("business")
        val business: List<Business>
) {
    data class Business(
            @field:SerializedName("id")
            val id: String,
            @field:SerializedName("authUserId")
            val authUserId: String,
            @field:SerializedName("organisationId")
            val organisationId: String,
            @field:SerializedName("companyId")
            val companyId: String,
            @field:SerializedName("subCompanyId")
            val subCompanyId: String,
            @field:SerializedName("dp")
            val dp: String?,
            @field:SerializedName("nickName")
            val nickName: String?,
            @field:SerializedName("subCompanyName")
            val subCompanyName: String

    )
}
