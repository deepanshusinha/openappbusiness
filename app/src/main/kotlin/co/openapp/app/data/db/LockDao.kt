package co.openapp.app.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import co.openapp.app.data.model.lock.LockData

/**
 * Interface for database access on Lock related operations.
 */
@Dao
abstract class LockDao : BaseDao<LockData> {

    @Transaction
    open fun updateLock(lockList: LockData) {
        delete(lockList)
        insert(lockList)
    }

    @Transaction
    open fun updateLockAll(lockList: List<LockData>) {
        deleteAll()
        insertAll(lockList)
    }

    @Query("SELECT * FROM lock")
    abstract fun fetchLockFromDb(): LiveData<List<LockData>>

    @Query("SELECT * FROM lock WHERE macAddress = :macAddress")
    abstract fun fetchLockByMacAddress(macAddress: String?): LiveData<LockData>

    @Query("SELECT lockId FROM lock WHERE macAddress = :macAddress")
    abstract fun getLockId(macAddress: String): LiveData<String>?

    @Query("DELETE FROM lock")
    abstract fun deleteAll()
}