package co.openapp.app.data.model.response.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25June2018
 */
open class LockData {

    @SerializedName("id")
    open var id: Long? = null

    @SerializedName("name")
    open var name: String? = null

    @SerializedName("mac_address")
    open var macAddress: String? = null

    @SerializedName("product_id")
    open var productId: String? = null

    @SerializedName("current_status")
    open var currentStatus: String? = null

    @SerializedName("registration_date")
    open var registrationDate: String? = null

    @SerializedName("location")
    open var location: String? = null

    @SerializedName("battery_level")
    open var batteryLevel: Int? = null

    @SerializedName("client_user_id")
    open var clientUserId: String? = null

    @SerializedName("manufacturer")
    open var manufacturer: String? = null

    @SerializedName("is_setup")
    open var isSetup: Boolean? = null

    @SerializedName("master_key")
    open var masterKey: String? = null

    @SerializedName("description")
    open var description: String? = null

    @SerializedName("is_tagged")
    open var isTagged: Boolean? = null

    @SerializedName("location_tags")
    open var lockLocationTags: List<LockLocationTag>? = null

}