package co.openapp.app.data.model.lock


import com.google.gson.annotations.SerializedName

data class VerifyTripOtp(
        @SerializedName("lockId")
        val lockId: String?, // 123
        @SerializedName("otp")
        val otp: Int? // 123456
)