package co.openapp.app.data.model.user

import com.google.gson.annotations.SerializedName

data class Business (
        @SerializedName("subCompanyName")
        val subCompanyName: String
)