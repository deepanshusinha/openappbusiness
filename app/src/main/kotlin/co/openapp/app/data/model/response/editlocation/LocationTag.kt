package co.openapp.app.data.model.response.editlocation

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class LocationTag(

        @SerializedName("latitude")
        var latitude: Double? = null,

        @SerializedName("longitude")
        var longitude: Double? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("label")
        var label: String? = null,

        @SerializedName("id")
        var id: Long? = null,

        @SerializedName("user_id")
        var userId: Long? = null
)