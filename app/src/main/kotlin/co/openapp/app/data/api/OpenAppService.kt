package co.openapp.app.data.api

import co.openapp.app.data.model.request.*
import co.openapp.app.data.model.auth.AuthFields
import co.openapp.app.data.model.response.AuthResponse
import co.openapp.app.data.model.response.GeneralResponse
import co.openapp.app.data.model.response.ListResponse
import co.openapp.app.data.model.response.SyncResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface OpenAppService {

    @POST("/api/v1/auth/login-business")
    fun callLoginApi(@Body body: AuthFields): Single<AuthResponse>

    @POST("/api/v1/auth/signup-business ")
    fun callRegisterApi(@Body body: AuthFields): Single<AuthResponse>

    @POST("/password/forgot")
    fun callForgotPasswordApi(@Body email: ForgotRequest): Single<AuthResponse>

    @POST("/password/update/{user_id}")
    fun callChangePasswordApi(@Path("user_id") userId: String, @Body body: ChangePasswordRequest): Single<AuthResponse>

    @GET("/lock/list/{user_id}")
    fun callListApi(@Path("user_id") userId: Int?): Single<ListResponse>

    @POST("/user/lock/sync")
    fun callSyncApi(@Body syncRequest: ArrayList<SyncRequest>): Single<SyncResponse>

    @POST("/onboard/new/lock")
    fun callInitApi(@Body ttLockList: ArrayList<KeyRequest>): Single<SyncResponse>

    @GET("/general")
    fun callGeneralApi(): Single<GeneralResponse>

    @POST("/onboard/lock/update/{user_id}")
    fun callDoorPwdUpdateApi(@Path("user_id") userId: String, @Body pwdUpdate: DoorPwdUpdate): Single<AuthResponse>

    @POST("/onboard/lock/passcode/update")
    fun callPadLockPwdUpdateApi(@Body pwdUpdate: PadlockPwdUpdate): Single<AuthResponse>
}