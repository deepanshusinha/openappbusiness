package co.openapp.app.data.model.response.lockactivities

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 12,July,2018
 */
data class OperateLogUploadResponse(

        @SerializedName("code")
        var code: Int? = 0,

        @SerializedName("data")
        var data: Boolean? = null,

        @SerializedName("message")
        var message: String? = null

)