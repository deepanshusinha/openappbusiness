package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class LocationConstraint(
        @SerializedName("locklocation_id") val locklocationId: Int, //2
        @SerializedName("user_id") val userId: Int, //6
        @SerializedName("latlng") val latlng: String, //12.54645, 71.54546546
        @SerializedName("radius") val radius: Float, //100 meters
        @SerializedName("name") val name: String, //Open Appliances
        @SerializedName("address") val address: String,
        @SerializedName("location") val location: String, // name of location
        @SerializedName("created") val created: String //2018-05-30 16:50:00
)