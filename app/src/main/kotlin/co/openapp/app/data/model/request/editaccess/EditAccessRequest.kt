package co.openapp.app.data.model.request.editaccess

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by abhijeethallur on 31,July,2018
 */
data class EditAccessRequest(

        @SerializedName("access_level")
        var access_level: String? = null,

        @SerializedName("schedule_type")
        var schedule_type: String? = null,

        @SerializedName("schedule")
        var schedule: Schedule? = null,

        @SerializedName("user_ids")
        var userIds: ArrayList<Long>? = null,

        @SerializedName("notifications")
        var notificationId: Boolean = false


) : Serializable