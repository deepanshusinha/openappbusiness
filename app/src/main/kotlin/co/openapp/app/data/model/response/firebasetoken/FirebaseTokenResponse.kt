package co.openapp.app.data.model.response.firebasetoken

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 10,August,2018
 */
data class FirebaseTokenResponse (

        @SerializedName("data")
        val data: Boolean? = false,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)