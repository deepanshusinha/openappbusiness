package co.openapp.app.data.model.sync

import com.google.gson.annotations.SerializedName

data class SyncResponse(
        @SerializedName("status")
        val status: Boolean = false,
        @SerializedName("message")
        var message: String = "",
        @SerializedName("errorCode")
        var errorCode: Int = 0
)