package co.openapp.app.data.model.response.countries

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 06,June,2018
 */
open class CountryData {

    @SerializedName("id")
    open var countryId: Int = 0

    @SerializedName("name")
    open var countryFullName: String? = null

    @SerializedName("country_code")
    open var countryCode: String? = null

    @SerializedName("telephone_code")
    open var telephoneCode: String? = null

    @SerializedName("languages")
    open var languages: List<LanguageData?>? = null
}