package co.openapp.app.data.model.lock

import com.google.gson.annotations.SerializedName

data class PassCodeData(
        @SerializedName("passcode")
        val passCode: String
)
