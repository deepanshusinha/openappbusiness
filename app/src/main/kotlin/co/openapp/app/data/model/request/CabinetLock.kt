package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class CabinetLock(
        @SerializedName("connected") var connected: String? = null,
        @SerializedName("disconnected") var disconnected: String? = null,
        @SerializedName("operations") var operations: Operations? = null,
        @SerializedName("error") var error: String? = null
)