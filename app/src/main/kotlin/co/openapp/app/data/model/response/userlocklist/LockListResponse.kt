package co.openapp.app.data.model.response.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class LockListResponse(

        @SerializedName("code")
        var code: Int? = 0,

        @SerializedName("data")
        var data: Data? = null,

        @SerializedName("message")
        var message: String? = null

)