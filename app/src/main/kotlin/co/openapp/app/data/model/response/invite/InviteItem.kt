package co.openapp.app.data.model.response.invite

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 10,July,2018
 */
data class InviteItem(

        @SerializedName("mobile")
        var mobile: String? = null,

        @SerializedName("lock_id")
        var lockId: Long? = null,

        var status: Boolean? = null,
        var lockName: String? = null,
        var contactName: String? = null

)