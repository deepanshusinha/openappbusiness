package co.openapp.app.data.model.request

import com.google.gson.annotations.SerializedName

data class Usage(
        @SerializedName("lock") val lock: String, //3456
        @SerializedName("master") val master: String, //2345
        @SerializedName("otp") var otp: String, //2345
        @SerializedName("flag") var flag: String, //0
        @SerializedName("user_id") var uid: String //24
)