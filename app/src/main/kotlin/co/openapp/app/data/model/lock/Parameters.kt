package co.openapp.app.data.model.lock

import com.google.gson.annotations.SerializedName

data class Parameters(
        @SerializedName("aesKey")
        val aesKey: String, // 17,99,54,79,17,83,94,77,73,20,37,14,87,38,51,32
        @SerializedName("blePassword")
        val blePassword: String?, // 567123
        @SerializedName("adminPwd")
        val adminPwd: String?, // OTIsODgsOTAsOTEsOTQsOTMsOTQsODgsODgsODQsMTg=
        @SerializedName("lockFlagPos")
        val lockFlagPos: Int?, // 0
        @SerializedName("lockKey")
        val lockKey: String?, // NSwyLDEyLDQsNiw2LDEyLDEzLDEsMiw3NQ==
        @SerializedName("lockVersion")
        val lockVersion: LockVersion?,
        @SerializedName("pwdInfo")
        val pwdInfo: String?, // a1kObwH2kY0VAPiAcjYJrYzl8+Z6/YiuhMg1pUW5KId/zfwnfK252KShlhuLIofL0KHBEzHoZemNM14GaN2iQJ7q+Je6FfCCYvvcnPOINchlGfI89p4nVbRg2kOUcU8ZjDUzdeD3oV1MwbdD37mBku5lMepK2bCRPqGKMxJhBrjdWlIqjAiMlTvKdcuhpgwLsXiudNymVu77zYcQ5uXI8cpF6IS434Lvi6Y7ZXFGMQZmvL94lsoClg4sRy/OgkH9UpOu2lFtkKUrmKjwOhlhZcgMeSmZVvcY3mi7WY1P3hv32TmOywh+eVINr7uZ3nf5b9ynItwDpejH8USqru/A/R+m0szpC4NEjV1Yyc+jl9eOREhaDus3EXdKkl1J8VzZNECVpeAX8t+F++klvJWptU7GshBU4MjgLkgd2VilpNGNDugrx1J9bu4FknENmgpLboLSprHfsExKxwrZoavQDc9mfWZIl8/n9hXYtG+iekct6C1+Zkd81Oo+0wQIxYw5L48aophqHeWKmOqzdJyfb8XCtFvlR5P/wXleGf57W7WDBJcasLyWn2/b7/YFqmIyS7cDHZg3zo8oIokvQRfPAYHAhRBAnrYpgCx7cTLfc6hnuibZ5JK+LNmNe9+zYaykr1O39D5uwt0+U4oKWCl7680Dvc8m4OwVFkNDydSq/Jqb/bJY+njW//oIWDLBH5fYcSV5MYUJ+i5fCUj2eNUGuomcsawhdpfz/hg5GMB6tJPAEz+tZATDw4Hrs/dKkxHexwHYDZv76smLUSoOb8zQ5hmkSxDedYEqB+OiTn+AQ/qEEngeJLUMey3A6DVrIfWtMdHA1ma5Ig2V/8adIFixzUDKMNguW1T3LQhUAKPcq5k=
        @SerializedName("timezoneRawOffset")
        val timezoneRawOffset: Long?, // 19800000
        @SerializedName("uid")
        val uid: Int? // 1e888nd00000000
)