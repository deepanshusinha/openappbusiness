package co.openapp.app.data.model.response.registerlock

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class Data(
        @SerializedName("id")
        var id: Int? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("mac_address")
        var macAddress: String? = null,

        @SerializedName("product_id")
        var productId: String? = null,

        @SerializedName("current_status")
        var currentStatus: String? = null,

        @SerializedName("registration_date")
        var registrationDate: String? = null,

        @SerializedName("location")
        var location: String? = null,

        @SerializedName("battery_level")
        var batteryLevel: Int? = null,

        @SerializedName("client_user_id")
        var clientUserId: String? = null,

        @SerializedName("manufacturer")
        var manufacturer: String? = null,

        @SerializedName("is_setup")
        var isSetup: Boolean? = null,

        @SerializedName("description")
        var description: String? = null
)