package co.openapp.app.data.model.response.guestprofile

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by abhijeethallur on 30,July,2018
 */
data class GuestProfileResponse (

        @SerializedName("data")
        val userAccessDataList: ArrayList<UserAccessData>? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
) : Serializable