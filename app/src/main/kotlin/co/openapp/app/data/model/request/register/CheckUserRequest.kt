package co.openapp.app.data.model.request.register

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 13,June,2018
 */
data class CheckUserRequest(

        @SerializedName("email")
        var email: String? = null,

        @SerializedName("country_id")
        var country_id: Int = 1,

        @SerializedName("mobile")
        var mobile: String? = null
)