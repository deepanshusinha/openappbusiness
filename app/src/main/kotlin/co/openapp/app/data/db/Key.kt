package co.openapp.app.data.db

data class Key(
        val uid: Int,
        val lockMac: String,
        val unlockKey: String,
        val adminPs: String,
        val aesKeyStr: String,
        val pwdInfo: String,
        val lockFlagPos: Int,
        var lockName: String,
        val lockVersion: String,
        val timezoneRawOffset: Long
)