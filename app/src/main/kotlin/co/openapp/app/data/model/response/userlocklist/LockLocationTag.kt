package co.openapp.app.data.model.response.userlocklist

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 27,June,2018
 */
open class LockLocationTag {

    @SerializedName("lock_id")
    open var lockId: Long? = null

    @SerializedName("tag_data")
    open var tagData: LockLocationTagData? = null

    @SerializedName("status")
    open var status: String? = null

    @SerializedName("id")
    open var id: Long? = null
}