package co.openapp.app.data.model.sync

data class DeviceLocation(
        val latitude: Double,
        val longitude: Double
)