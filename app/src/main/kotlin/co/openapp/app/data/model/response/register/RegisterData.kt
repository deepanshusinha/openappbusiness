package co.openapp.app.data.model.response.register

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 11,June,2018
 */
data class RegisterData(

        @SerializedName("id")
        var id: Int = 0,

        @SerializedName("role")
        var role: String? = null,

        @SerializedName("email")
        var email: String? = null,

        @SerializedName("first_name")
        val firstName: String? = null,

        @SerializedName("last_name")
        val lastName: String? = null,

        @SerializedName("mobile")
        val mobile: String? = null,

        @SerializedName("status")
        val status: String? = null,

        @SerializedName("email_verified")
        val emailVerified: Boolean = false,

        @SerializedName("mobile_verified")
        val mobileVerified: Boolean = false,

        @SerializedName("jwt_version")
        val jwtVersion: Int = 0,

        @SerializedName("name")
        val name: String? = null
)