package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

data class ListResponse(
        @SerializedName("success") val success: Boolean, //true
        @SerializedName("data") val data: List<ListDataItem>,
        @SerializedName("status") val message: String
)