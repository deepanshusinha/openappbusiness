package co.openapp.app.data.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 11,June,2018
 */
data class AuthResponse(

        @SerializedName("code")
        var code: Int = 0,

        @SerializedName("success")
        var success: Boolean,

        @SerializedName("data")
        var data: AuthData? = null,

        @SerializedName("message")
        var message: String
)