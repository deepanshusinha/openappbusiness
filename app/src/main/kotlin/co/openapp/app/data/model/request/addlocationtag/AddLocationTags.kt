package co.openapp.app.data.model.request.addlocationtag

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 25,June,2018
 */
data class AddLocationTags(

        @SerializedName("location_tags")
        var locationTags: ArrayList<LocationTag>? = null,

        @SerializedName("lock_ids")
        var lockIds: ArrayList<Long>? = ArrayList()
)