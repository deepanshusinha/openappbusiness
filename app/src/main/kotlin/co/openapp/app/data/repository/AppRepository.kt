package co.openapp.app.data.repository

import co.openapp.app.data.prefs.OpenAppPreferences
import co.openapp.app.data.prefs.OpenAppPreferencesLiveData
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.Utils
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.livedata.pref.LivePreference
import javax.inject.Singleton

@Singleton
open class AppRepository constructor(
        private val utils: Utils,
        private val preferences: OpenAppPreferences,
        private val livePreference: OpenAppPreferencesLiveData
) {

    fun getString(stringResId: Int): String {
        return utils.getMessage(stringResId)
    }

    fun hasConnection(): Boolean {
        return utils.hasConnection()
    }

    fun isLogin(): Boolean {
        return !preferences.getString(Constants.SharedKeys.ACCESS_TOKEN).isNullOrBlank()
    }

    fun isLoginLiveData(): LivePreference<String> {
        return livePreference.getString(Constants.SharedKeys.ACCESS_TOKEN)
    }

    fun clearToken() {
        preferences.clearData(Constants.SharedKeys.ACCESS_TOKEN)
        preferences.clearData(Constants.SharedKeys.COMPANY_ID)
    }

    fun getAccessToken(): String? {
        return preferences.getString(Constants.SharedKeys.ACCESS_TOKEN)
    }

    fun getCompanyId(): String? {
        return preferences.getString(Constants.SharedKeys.COMPANY_ID)
    }

    fun saveVersionCode(versionCode: Long) {
        preferences.save(Constants.SharedKeys.VERSION_CODE, versionCode)
    }

    fun getVersionCode(): Long {
        return preferences.getLong(Constants.SharedKeys.VERSION_CODE)
    }

    fun saveVersionName(versionName: String) {
        preferences.save(Constants.SharedKeys.VERSION_NAME, versionName)
    }

    fun getVersionName(): String {
        return "v" + preferences.getString(Constants.SharedKeys.VERSION_NAME)
    }

    fun clearAllData() {
        preferences.clearAllData()
    }
}