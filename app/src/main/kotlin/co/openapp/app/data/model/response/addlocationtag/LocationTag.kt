package co.openapp.app.data.model.response.addlocationtag

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 05,October,2018
 */

data class LocationTag(
        @SerializedName("latitude")
        var latitude: Double? = null,

        @SerializedName("longitude")
        var longitude: Double? = null,

        @SerializedName("name")
        var name: String? = null,

        @SerializedName("label")
        var label: String? = null,

        @SerializedName("user_id")
        var userId: Long? = null,

        @SerializedName("status")
        var status: Int? = null
)