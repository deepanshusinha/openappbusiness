package co.openapp.app.data.model.response.profilepicture

import co.openapp.app.data.model.response.login.LoginUser
import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 26,July,2018
 */
data class ProfilePicUploadResponse(

        @SerializedName("data")
        val data: LoginUser? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null
)