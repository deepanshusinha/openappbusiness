package co.openapp.app.data.model.response.deletelock.status

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 02,August,2018
 */
data class LockDeleteStatusResponse (

        @SerializedName("data")
        val data: DeleteStatus? = null,

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("message")
        val message: String? = null

)