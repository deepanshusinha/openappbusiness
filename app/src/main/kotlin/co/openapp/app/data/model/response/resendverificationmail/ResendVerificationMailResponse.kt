package co.openapp.app.data.model.response.resendverificationmail

import com.google.gson.annotations.SerializedName

/**
 * Created by abhijeethallur on 11,June,2018
 */
data class ResendVerificationMailResponse(

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("data")
        val data: Boolean? = null,

        @SerializedName("message")
        val message: String? = null
)