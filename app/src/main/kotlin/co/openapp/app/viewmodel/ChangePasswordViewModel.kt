package co.openapp.app.viewmodel

import co.openapp.app.R
import co.openapp.app.data.db.BottomSheet
import co.openapp.app.data.model.request.ChangePasswordRequest
import co.openapp.app.data.repository.OpenAppRepository
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.SingleLiveEvent
import co.openapp.app.utils.schedulers.AppScheduler
import javax.inject.Inject

/**
 * Created by deepanshusinha on 23/02/18.
 */

class ChangePasswordViewModel @Inject constructor(private val repository: OpenAppRepository) : BaseViewModel() {

    val showBottomSheet = SingleLiveEvent<BottomSheet>()

    fun doChangePassword(currentStr: String, newStr: String) {
        loading.set(true)

        if (hasConnection()) {
            mCompositeDisposable.add(repository
                    .doChangePassword(ChangePasswordRequest(currentStr, newStr))
                    .subscribeOn(AppScheduler.io())
                    .observeOn(AppScheduler.mainThread())
                    .subscribe({
                        loading.set(false)
                        showErrorToast.value = it.message
                    }) { throwable ->
                        loading.set(false)
                        showErrorToast.value = throwable.localizedMessage
                    }
            )
        } else {
            loading.set(false)
            handleError(BottomSheet(R.string.error, R.string.no_internet_connection, R.string.got_it))
        }
    }

    private fun handleError(bottomSheet: BottomSheet) {
        showBottomSheet.value = bottomSheet
    }
}