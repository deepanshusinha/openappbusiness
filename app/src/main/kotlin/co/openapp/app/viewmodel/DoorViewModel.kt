package co.openapp.app.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import co.openapp.app.R
import co.openapp.app.data.db.Key
import co.openapp.app.data.model.lock.PassCode
import co.openapp.app.data.model.lock.PassCodeResponse
import co.openapp.app.data.model.request.DoorLock
import co.openapp.app.data.model.request.DoorPwdUpdate
import co.openapp.app.data.repository.LockRepository
import co.openapp.app.data.source.Resource
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.ble.TTLockBle
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by deepanshu on 01-01-2018.
 */

class DoorViewModel @Inject constructor(private val lockRepository: LockRepository) : BaseViewModel() {

    private lateinit var mTTLock: TTLockBle

    private val _passCode: MutableLiveData<Pair<PassCode, String>> = MutableLiveData()
    private val passCode: LiveData<Pair<PassCode, String>>
        get() = _passCode

    fun initTTLock(context: Context, doorKey: Key) {
        mTTLock = TTLockBle(context)

        mTTLock.startBleService()

        mTTLock.setKey(doorKey)
    }

    fun stopBleService() {
        mTTLock.stopBleService()
    }

    fun doUnlock() {
        mTTLock.doUnlock()
    }

    fun doOperateLog() {
        mTTLock.doOperateLog()
    }

    fun addPeriodKeyboardPassword(password: String) {
        mTTLock.doAddPeriodKeyboardPassword(password)
    }

    fun doSetAdminKeyboardPassword(password: String) {
        loading.set(true)
        mTTLock.doSetAdminKeyboardPassword(password)
        mTTLock.doAddPeriodKeyboardPassword(password)
    }

    fun setDoorLock(records: String?) {
        lockRepository.addOrSaveDoorLock(records)
    }

    fun setDoorLockError(errorMsg: String?) {
        lockRepository.addOrSaveDoorLockError(errorMsg)
    }

    fun updateAdminKeyboardPwd(doorPwdUpdate: DoorPwdUpdate) {
        /*if (hasConnection()) {
            mCompositeDisposable.add(repository
                    .doDoorPwdUpdate(doorPwdUpdate)
                    .subscribeOn(AppScheduler.io())
                    .observeOn(AppScheduler.mainThread())
                    .subscribe({
                    }) {
                    }
            )
        } else {
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }*/
    }

    fun setAttemptCount(count: Int) {
//        repository.setAttemptCount(count)
    }

    fun setSyncEngaged(value: Boolean) {
        Timber.e("setsync - $value")
        lockRepository.setSyncEngaged(value)
    }

    fun generatePassCode(passCodeValue: PassCode, lockId: String) {
        loading.set(true)

        if (hasConnection()) {
            _passCode.value = Pair(passCodeValue, lockId)
        } else {
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    // Generate Door Lock PassCode
    val generatePassCodeRepositories: LiveData<Resource<PassCodeResponse>> = Transformations
            .switchMap(passCode) { passCode ->
                lockRepository.generatePassCode(passCode.first, passCode.second)
            }
}