package co.openapp.app.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import co.openapp.app.R
import co.openapp.app.data.model.auth.AuthFields
import co.openapp.app.data.model.auth.AuthForm
import co.openapp.app.data.model.request.otp.OtpFields
import co.openapp.app.data.model.user.UserData
import co.openapp.app.data.repository.AuthRepository
import co.openapp.app.data.source.Resource
import co.openapp.app.data.vo.AuthResponse
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.AbsentLiveData
import co.openapp.app.utils.SingleLiveEvent
import co.openapp.app.utils.schedulers.AppScheduler
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AuthViewModel @Inject constructor(private val authRepository: AuthRepository) : BaseViewModel() {

    private val intervalDuration = 5000L

    val changeAnimation = SingleLiveEvent<Unit>()

    fun startAnimation() {
        mCompositeDisposable.add(
                Observable.interval(intervalDuration, TimeUnit.MILLISECONDS)
                        .observeOn(AppScheduler.mainThread())
                        .subscribe {
                            changeAnimation.call()
                        }
        )
    }

    private val _loginRequest: MutableLiveData<AuthFields> = MutableLiveData()
    private val _registerRequest: MutableLiveData<AuthFields> = MutableLiveData()
    private val _forgotRequest: MutableLiveData<AuthFields> = MutableLiveData()
    private val _changeRequest: MutableLiveData<AuthFields> = MutableLiveData()
    private val _otpRequest: MutableLiveData<OtpFields> = MutableLiveData()
    private val _profileRequest: MutableLiveData<Boolean> = MutableLiveData()

    val isLogin: Boolean
        get() = authRepository.isLogin()

    /**
     * Attempts to sign in the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun doLogin(authFields: AuthFields) {
        loading.set(true)

        if (hasConnection()) {
            loading.set(false)
            _loginRequest.value = authFields
        } else {
            loading.set(false)
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    val loginResult: LiveData<Resource<AuthResponse>> = Transformations
            .switchMap(_loginRequest) {
                if (it == null)
                    AbsentLiveData.create()
                else
                    authRepository.doLogin(it)
            }

    /**
     * Attempts to register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun doRegister(authFields: AuthFields) {
        loading.set(true)

        if (hasConnection()) {
            loading.set(false)
            _registerRequest.value = authFields
        } else {
            loading.set(false)
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    val registerResult: LiveData<Resource<AuthResponse>> = Transformations
            .switchMap(_registerRequest) {
                if (it == null)
                    AbsentLiveData.create()
                else
                    authRepository.doRegister(it)
            }

    var auth: AuthForm = AuthForm()
        private set

    val authFields: MutableLiveData<AuthFields>?
        get() = auth.authFields

    fun onLogIn() {
        auth.register = false
        if (auth.isValid) {
            doLogin(auth.fields)
        } else {
            showErrorSheet.value = auth.error?.let { getString(it) }
        }
    }

    fun onRegister() {
        auth.register = true
        if (auth.isValid) {
            doRegister(auth.fields)
        } else {
            showErrorSheet.value = auth.error?.let { getString(it) }
        }
    }

    fun onForgot() {
        auth.reset = true
        if (auth.isResetValid) {
            doForgot(auth.fields)
        } else {
            showErrorSheet.value = auth.error?.let { getString(it) }
        }
    }

    private fun doForgot(fields: AuthFields) {
        loading.set(true)

        if (hasConnection()) {
            loading.set(false)
            _forgotRequest.value = fields
        } else {
            loading.set(false)
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    val forgotResult: LiveData<Resource<AuthResponse>> = Transformations
            .switchMap(_forgotRequest) {
                if (it == null)
                    AbsentLiveData.create()
                else
                    authRepository.doForgot(it)
            }

    fun onForgetPassword() {
        auth.reset = false
        if (auth.isResetValid) {
            doForgetPassword(auth.fields)
        } else {
            showErrorSheet.value = auth.error?.let { getString(it) }
        }
    }

    private fun doForgetPassword(fields: AuthFields) {
        loading.set(true)

        if (hasConnection()) {
            loading.set(false)
            _changeRequest.value = fields
        } else {
            loading.set(false)
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    val forgetResult: LiveData<Resource<AuthResponse>> = Transformations
            .switchMap(_changeRequest) {
                if (it == null)
                    AbsentLiveData.create()
                else
                    authRepository.doForgetChangePassword(it)
            }

    fun doOtp(fields: OtpFields) {
        loading.set(true)

        if (hasConnection()) {
            loading.set(false)
            _otpRequest.value = fields
        } else {
            loading.set(false)
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    val otpResult: LiveData<Resource<AuthResponse>> = Transformations
            .switchMap(_otpRequest) {
                when {
                    it == null -> AbsentLiveData.create()
                    it.otp.isEmpty() -> authRepository.doOtpResend(it)
                    else -> authRepository.doOtp(it)
                }
            }

    fun profileDetailList() {
        _profileRequest.value = hasConnection()
    }

    val profileResult: LiveData<Resource<List<UserData>>> = Transformations
            .switchMap(_profileRequest) {
                if (it == null)
                    AbsentLiveData.create()
                else
                    authRepository.loadProfileDetail(false)
            }

    fun onChangePassword() {
        if (auth.isChangeValid) {
            doChangePassword(auth.fields)
        } else {
            showErrorSheet.value = auth.error?.let { getString(it) }
        }
    }

    private fun doChangePassword(fields: AuthFields) {
        loading.set(true)

        if (hasConnection()) {
            loading.set(false)
            _changeRequest.value = fields
        } else {
            loading.set(false)
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    fun getVersionName(): String = authRepository.getVersionName()

    val changeResult: LiveData<Resource<AuthResponse>> = Transformations
            .switchMap(_changeRequest) {
                if (it == null)
                    AbsentLiveData.create()
                else
                    authRepository.doChangePassword(it)
            }
}