package co.openapp.app.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import co.openapp.app.OpenApp
import co.openapp.app.data.model.lock.LockData
import co.openapp.app.data.repository.LockRepository
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.SingleLiveEvent
import co.openapp.app.utils.schedulers.AppScheduler
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.RxBleDevice
import com.polidea.rxandroidble2.exceptions.BleScanException
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by deepanshu on 01-01-2018.
 */

class ScanLocksViewModel @Inject constructor(private val lockRepository: LockRepository) : BaseViewModel() {

    private var mRxBleClient: RxBleClient = OpenApp.rxBleClient

    private lateinit var mScanDisposable: Disposable

    val doLocPer = SingleLiveEvent<Void>()

    val doStartScan = SingleLiveEvent<Void>()

    val doLocOn = SingleLiveEvent<Void>()

    val doBleOn = SingleLiveEvent<Void>()

    private var itemList: ArrayList<LockData> = ArrayList()

    val scannedList = SingleLiveEvent<ArrayList<LockData>>()
    private val scanningList = MutableLiveData<RxBleDevice>()

//    private val SORTING_COMPARATOR = { lhs: LockData, rhs: LockData -> lhs.bleDevice?.macAddress?.compareTo(rhs.bleDevice.macAddress) }

    val matchLock: LiveData<LockData> = Transformations.switchMap(scanningList) { rxBleDevice ->
        lockRepository.fetchLock(rxBleDevice.macAddress)
    }

    /*
     * Start scanning in onResume callback.
     */
    fun startScan() {

        mScanDisposable = mRxBleClient.observeStateChanges()
                .startWith(mRxBleClient.state)
                .switchMap { t: RxBleClient.State? ->
                    when (t) {
                        RxBleClient.State.READY -> {
                            // everything should work
                            Timber.e("rxclient${t.name}")
                            doStartScan.call()
                            itemList.clear()

                            mRxBleClient.scanBleDevices(
                                    ScanSettings.Builder()
                                            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                                            .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                                            .build(),
                                    ScanFilter.Builder()
                                            .build()
                            )
                        }
                        RxBleClient.State.BLUETOOTH_NOT_AVAILABLE -> {
                            // basically no functionality will work here
                            Timber.e("rxclient${t.name}")

                            // TODO: Show Message
                            Observable.empty()
                        }
                        RxBleClient.State.LOCATION_PERMISSION_NOT_GRANTED -> {
                            // scanning and connecting will not work
                            Timber.e("rxclient${t.name}")

                            doLocPer.call()
                            Observable.empty()
                        }
                        RxBleClient.State.BLUETOOTH_NOT_ENABLED -> {
                            // scanning and connecting will not work
                            Timber.e("rxclient${t.name}")

                            doBleOn.call()
                            Observable.empty()
                        }
                        RxBleClient.State.LOCATION_SERVICES_NOT_ENABLED -> {
                            // scanning will not work
                            Timber.e("rxclient${t.name}")

                            doLocOn.call()
                            Observable.empty()
                        }
                        else -> {
                            Timber.e("rxclient${t?.name}")

                            // TODO: Show Message
                            Observable.empty()
                        }
                    }
                }
                .observeOn(AppScheduler.mainThread())
                .subscribe({ scanResult: ScanResult? ->
                    scanningList.postValue(scanResult?.bleDevice)
                }) { throwable ->
                    run {
                        if (throwable is BleScanException)
                            handleBleScanException(throwable)
                    }
                }
    }

    /*
     * Stop scanning in onPause callback.
     */
    fun stopScan() {
        Timber.e("stop Scan")

        mScanDisposable.dispose()
    }

    private fun handleBleScanException(bleScanException: BleScanException) {
        val text: String

        when (bleScanException.reason) {
            BleScanException.BLUETOOTH_NOT_AVAILABLE -> text = "Bluetooth is not available"
            BleScanException.BLUETOOTH_DISABLED -> text = "Enable bluetooth and try again"
            BleScanException.LOCATION_PERMISSION_MISSING -> text = "On Android 6.0 location permission is required. Implement Runtime Permissions"
            BleScanException.LOCATION_SERVICES_DISABLED -> text = "DeviceLocation services needs to be enabled on Android 6.0"
            BleScanException.SCAN_FAILED_ALREADY_STARTED -> text = "Scan with the same filters is already started"
            BleScanException.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED -> text = "Failed to register application for bluetooth scan"
            BleScanException.SCAN_FAILED_FEATURE_UNSUPPORTED -> text = "Scan with specified parameters is not supported"
            BleScanException.SCAN_FAILED_INTERNAL_ERROR -> text = "Scan failed due to internal error_network"
            BleScanException.SCAN_FAILED_OUT_OF_HARDWARE_RESOURCES -> text = "Scan cannot start due to limited hardware resources"
            BleScanException.UNDOCUMENTED_SCAN_THROTTLE -> text = String.format(
                    Locale.getDefault(),
                    "Android 7+ does not allow more scans. Try in %d seconds",
                    secondsTill(bleScanException.retryDateSuggestion)
            )
            BleScanException.UNKNOWN_ERROR_CODE, BleScanException.BLUETOOTH_CANNOT_START -> text = "Unable to start scanning"
            else -> text = "Unable to start scanning"
        }
        Timber.tag("EXCEPTION").log(bleScanException.reason, text)
    }

    private fun secondsTill(retryDateSuggestion: Date?): Long {
        return TimeUnit.MILLISECONDS.toSeconds(retryDateSuggestion!!.time - System.currentTimeMillis())
    }

    fun setData(lockDataList: LockData) {
        if (itemList.contains(lockDataList))
            return

        itemList.add(lockDataList)
        scannedList.value = itemList
    }
}