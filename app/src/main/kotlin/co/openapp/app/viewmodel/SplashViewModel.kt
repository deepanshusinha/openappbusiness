package co.openapp.app.viewmodel

import co.openapp.app.BuildConfig
import co.openapp.app.data.repository.OpenAppRepository
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.SingleLiveEvent
import co.openapp.app.utils.schedulers.AppScheduler
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by deepanshu on 01-01-2018.
 */

class SplashViewModel @Inject constructor(private val repository: OpenAppRepository) : BaseViewModel() {

    val navigateToIntroActivity = SingleLiveEvent<Unit>()
    val navigateToHomeActivity = SingleLiveEvent<Unit>()

    fun startSplashing() {
        when (BuildConfig.APPLICATION_ID) {
            "co.openapp.app.newb2b",
            "co.openapp.app.newb2c" -> repository.setNewApi(true)
            "co.openapp.app.beta",
            "co.openapp.app" -> repository.setNewApi(false)
            else -> repository.setNewApi(false)
        }
        mCompositeDisposable.add(Single.just(1)
                .delay(3, TimeUnit.SECONDS)
                .observeOn(AppScheduler.mainThread())
                .ignoreElement()
                .doOnComplete {
                    if (repository.isLoggedIn()) {
                        navigateToHomeActivity.call()
                    } else {
                        navigateToIntroActivity.call()
                    }
                }
                .subscribe()
        )
    }
}
