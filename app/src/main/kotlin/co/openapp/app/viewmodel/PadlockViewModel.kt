package co.openapp.app.viewmodel

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import co.openapp.app.OpenApp
import co.openapp.app.R
import co.openapp.app.data.model.lock.*
import co.openapp.app.data.repository.AuthRepository
import co.openapp.app.data.repository.LockRepository
import co.openapp.app.data.source.Resource
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.AppAnalytics
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.SingleLiveEvent
import co.openapp.app.utils.constant.Constants
import com.sunshine.blelibrary.config.Config
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by deepanshu on 01-01-2018.
 */
class PadlockViewModel @Inject constructor(
        private val authRepository: AuthRepository,
        private val lockRepository: LockRepository
) : BaseViewModel() {

    lateinit var mTimeRules: ArrayList<TimeRules>

    val unLockedState = SingleLiveEvent<Boolean>()
    val onBackPressed = SingleLiveEvent<Unit>()

    val temp = SingleLiveEvent<Boolean>()

    private val _tripOtp: MutableLiveData<VerifyTripOtp> = MutableLiveData()
    private val tripOtp: LiveData<VerifyTripOtp>
        get() = _tripOtp

    private val _tripId: MutableLiveData<VerifyTripId> = MutableLiveData()
    private val tripId: LiveData<VerifyTripId>
        get() = _tripId

    /**
     * Noke Lock Activities
     */
    fun setConnectTime() {
        lockRepository.padLockConnectTime(AppSnippet.currDate)
    }

    fun setDisconnectTime() {
        lockRepository.padLockDisconnectTime(AppSnippet.currDate)
    }

    fun setAndAddOperateTimeSent() {
        if (this::mTimeRules.isInitialized) {
            mTimeRules.forEach { timeRule ->
                timeRule.validDates.forEach { timeItem ->
                    if (AppSnippet.currDateEpochSecond in timeItem.startDate..timeItem.endDate) {
                        if (timeRule.remainingAccess >= 0)
                            lockRepository.saveRemainingAccess(timeRule.ruleId)
                    }
                }
            }
        }
        lockRepository.setAndAddOperateUnlockedSentTime(AppSnippet.currDate)
    }

    fun setAndAddOperateLockedTime() {
        lockRepository.setAndAddOperateLockedSentTime(AppSnippet.currDate)
    }

    fun setSyncEngaged(value: Boolean) {
        Timber.e("setsync - $value")
        lockRepository.setSyncEngaged(value)
    }

    fun isLockUnlocked(lockId: String): Boolean = lockRepository.isLockUnlocked(lockId)
    fun isLockClosingData(): Boolean {
        val generalData = authRepository.getGeneralData()?.data
        return generalData != null && generalData.preferences.lockClosingData
    }

    fun verifyTripSheetOtp(verifyTripOtp: VerifyTripOtp) {
        loading.set(true)

        if (hasConnection()) {
            _tripOtp.value = verifyTripOtp
        } else {
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    fun verifyTripSheetId(verifyTripId: VerifyTripId) {
        loading.set(true)

        if (hasConnection()) {
            _tripId.value = verifyTripId
        } else {
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    val broadcastReceiver: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                val action = intent.action
                val data = intent.getStringExtra("data")
                Timber.e(action)
                when (action) {
                    Config.BATTERY_ACTION -> {
                        lockRepository.padLockBattery(Integer.parseInt(data, 16))
                    }
                    Config.OPEN_ACTION -> {
                        AppAnalytics.logCustom(
                                Constants.AnalyticsKeys.OPERATIONS,
                                Constants.AnalyticsKeys.UNLOCKED,
                                AppSnippet.currDate,
                                AppSnippet.currDateEpochSecond.toString()
                        )
                        setAndAddOperateTimeSent()
                    }
                    Config.CLOSE_ACTION -> {
                        lockRepository.setAndAddOperateUnlockedReceivedTime(AppSnippet.currDate)
                        if (isLockClosingData()) {
                            // temp
                            temp.value = true
                        } else {
                            onBackPressed.call()
                        }
                    }
                    Config.TOKEN_ACTION -> {
//                        lockRepository.padLockToken(data)
                        OpenApp.instance?.getIBLE()?.battery
                    }
                    Config.PASSWORD_ACTION -> {
                    }
                    else -> {
                        Timber.e("$action - $data")
                        unLockedState.postValue(false)
                        lockRepository.setAndAddOperateError("$action - $data")
                    }
                }
            }
        }
    }

    // Verify Trip Sheet OTP
    val verifyTripSheetOtpRepositories: LiveData<Resource<TripSheetResponse>> = Transformations
            .switchMap(tripOtp) { verifyTripData ->
                lockRepository.verifyTripSheetOtp(verifyTripData)
            }

    // Verify Trip Sheet ID
    val verifyTripSheetIdRepositories: LiveData<Resource<TripSheetResponse>> = Transformations
            .switchMap(tripId) { verifyTripData ->
                lockRepository.verifyTripSheetId(verifyTripData)
            }
}