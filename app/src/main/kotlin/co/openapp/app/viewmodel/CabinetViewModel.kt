package co.openapp.app.viewmodel

import co.openapp.app.data.model.request.Info
import co.openapp.app.data.model.request.Operations
import co.openapp.app.data.model.request.Usage
import co.openapp.app.data.repository.OpenAppRepository
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.AppSnippet
import javax.inject.Inject

/**
 * Created by deepanshu on 01-01-2018.
 */

class CabinetViewModel @Inject constructor(private val repository: OpenAppRepository) : BaseViewModel() {

    fun setAttemptCount(count: Int) {
        repository.setAttemptCount(count)
    }

    fun setCabinetConnectTime() {
        repository.setCabinetConnectTime(AppSnippet.currDate)
    }

    fun setCabinetDisconnectTime() {
        repository.setCabinetDisconnectTime(AppSnippet.currDate)
    }

    fun getUserId(): Int? {
        return repository.getUserId()
    }

    fun setCabinetOperations(it: Operations) {
        repository.setCabinetOperations(it)
    }

    fun setUsage(usage: Usage) {
//        repository.setCabinetUsage(usage)
    }

    fun setInfo(info: Info) {
//        repository.setCabinetInfo(info)
    }

    /*fun getInfo(): Info? {
        return repository.getCabinetInfo()
    }*/

    fun setCabinetOtp(otpCode: String) {
        val usage = repository.getCabinetUsage()
        if (usage != null) {
            usage.otp = otpCode
            usage.flag = "1"
            usage.uid = repository.getUserId().toString()
        }

//        usage?.let { setUsage(usage) }
    }

    fun setCabinetMaster(masterCode: String) {
        /*val info = getInfo()
        if (info != null) {
            info.master = masterCode
            info.flag = "1"
            info.uid = repository.getUserId().toString()
        }*/

//        info?.let { setInfo(it) }
    }

    fun setAndAddOperateTime(value: String) {
        repository.setAndAddCabinetOperateTime(value)
    }

    fun setRegisteredAdmin(userId: Int?) {
//        repository.setRegisteredAdmin(userId)
    }

    fun setAndAddSosTime(currDate: String) {
        repository.setAndSosTime(currDate)
    }
}