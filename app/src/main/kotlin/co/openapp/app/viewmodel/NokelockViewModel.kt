package co.openapp.app.viewmodel

import co.openapp.app.data.repository.LockRepository
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.AppSnippet
import javax.inject.Inject

/**
 * Created by deepanshu on 01-01-2018.
 */
class NokelockViewModel @Inject constructor(private val lockRepository: LockRepository) : BaseViewModel() {

    fun setConnectTime() {
        lockRepository.padLockConnectTime(AppSnippet.currDate)
    }

    fun setDisconnectTime() {
        lockRepository.padLockDisconnectTime(AppSnippet.currDate)
    }

    fun setPadLockError(error: String) {
        lockRepository.padLockError(error)
    }

    fun setBattery(batteryValue: Int) {
        lockRepository.padLockBattery(batteryValue)
    }

    fun setOperationError(error: String) {
        lockRepository.setAndAddOperateError(error)
    }

    fun setAndAddOperateTime() {
//        lockRepository.setAndAddOperateTime(AppSnippet.currDate)
    }
}