package co.openapp.app.viewmodel

import co.openapp.app.R
import co.openapp.app.data.model.request.KeyRequest
import co.openapp.app.data.model.response.ListDataItem
import co.openapp.app.data.model.response.PadLockKey
import co.openapp.app.data.repository.OpenAppRepository
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.AES
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.SingleLiveEvent
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.schedulers.AppScheduler
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by deepanshusinha on 26/02/18.
 */

class HomeViewModel @Inject constructor(private val repository: OpenAppRepository) : BaseViewModel() {

    val navigateToIntroActivity = SingleLiveEvent<Unit>()
    val navigateToScanActivity = SingleLiveEvent<Unit>()
    val showForceDialog = SingleLiveEvent<Unit>()

    fun versionCode() = repository.getVersionName()

    fun attemptLogout() {
        repository.doLogout()
        navigateToIntroActivity.call()
    }

    fun startScanning() {
        navigateToScanActivity.call()
    }

    fun fetchLockList() {

        if (hasConnection()) {
            mCompositeDisposable.add(repository
                    .doListApi()
                    .doOnSuccess { if (it.success) repository.saveLockList(it.data) }
                    .subscribeOn(AppScheduler.io())
                    .observeOn(AppScheduler.mainThread())
                    .subscribe({
                        Timber.e(it.success.toString())
                        if (!it.success)
                            showErrorToast.value = it.message
                    }) { throwable ->
                        showErrorToast.value = throwable.localizedMessage
                    }
            )
        } else {
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    /*fun pushDataToServer() {
        if (repository.isDataAvailable()) {

            if (hasConnection()) {
                mCompositeDisposable.add(repository
                        .doSyncApi()
                        .doOnSuccess { if (it.success) repository.clearAccessData() }
                        .subscribeOn(AppScheduler.io())
                        .observeOn(AppScheduler.mainThread())
                        .subscribe({
                            if (!it.success)
                                showErrorToast.value = it.message
                        }) { throwable ->
                            showErrorToast.value = throwable.localizedMessage
                        }
                )
            } else {
                showErrorSheet.value = getString(R.string.no_internet_connection)
            }
        }
    }*/

    fun initApi(keyRequest: KeyRequest) {
        Timber.e("key - $keyRequest")
        if (hasConnection())
        // call the api directly
            mCompositeDisposable.add(repository
                    .doInitApi(keyRequest)
                    .doOnSuccess { if (it.success) repository.clearInitTTLockData() }
                    .subscribeOn(AppScheduler.io())
                    .observeOn(AppScheduler.mainThread())
                    .subscribe({
                        if (!it.success)
                            repository.saveTTLockInit(keyRequest)
                    }) {
                        repository.saveTTLockInit(keyRequest)
                    }
            )
        else
            repository.saveTTLockInit(keyRequest)
    }

    fun callGeneralApi() {
        if (hasConnection())
            mCompositeDisposable.add(repository
                    .doGeneralApi()
                    .subscribeOn(AppScheduler.io())
                    .observeOn(AppScheduler.mainThread())
                    .subscribe({
                        val versionCode = repository.getVersionCode()
                        if (versionCode < it.minAndroidVer) {
                            showForceDialog.call()
                        }
                    }) {
                        Timber.e(it.localizedMessage)
                    }
            )
    }

    fun isAdmin(): Boolean {
        val user = repository.getUser()
        return user?.isAdmin ?: false
    }

    fun pushNokeLockPwdUpdateToServer() {
        val nokePwdUpdate = repository.getNokePwdUpdate()

        if (nokePwdUpdate != null)
            if (hasConnection())
                mCompositeDisposable.add(repository
                        .doPadLockPwdUpdateApi(nokePwdUpdate)
                        .doOnSuccess { }
                        .subscribeOn(AppScheduler.io())
                        .observeOn(AppScheduler.mainThread())
                        .subscribe({
                            if (it.success)
                                repository.clearNokePwdUpdate()
                        }) {

                        }
                )
    }

    fun decryptMessage(text: String) {
        val key = repository.getUser()?.key.toString()
        val decrypt = AES.decrypt(text.trim(), key).toString()
        val decryptArray = decrypt.split(",")
        val pwdList = mutableListOf(
                decryptArray[5],
                decryptArray[6],
                decryptArray[7],
                decryptArray[8],
                decryptArray[9],
                decryptArray[10],
                decryptArray[11],
                decryptArray[12],
                decryptArray[13],
                decryptArray[14],
                decryptArray[15],
                decryptArray[16],
                decryptArray[17],
                decryptArray[18],
                decryptArray[19],
                decryptArray[20]
        )
        repository.saveLockInList(ListDataItem(
                decryptArray[3].toInt(),
                "",
                "",
                decryptArray[1].toInt(),
                "SMS_PADLOCK",
                "",
                "",
                decryptArray[2],
                "",
                Constants.LockType.PAD_LOCK,
                0,
                AppSnippet.currDate,
                AppSnippet.getDateStringFromTimestamp(decryptArray[0]),
                1,
                null,
                null,
                null,
                false,
                null,
                PadLockKey(pwdList.joinToString(","), decryptArray[4])
        ))
    }
}