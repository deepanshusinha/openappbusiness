package co.openapp.app.viewmodel

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import co.openapp.app.R
import co.openapp.app.data.model.lock.*
import co.openapp.app.data.model.request.KeyRequest
import co.openapp.app.data.model.sync.DeviceLocation
import co.openapp.app.data.model.sync.SyncResponse
import co.openapp.app.data.model.user.UserData
import co.openapp.app.data.repository.AuthRepository
import co.openapp.app.data.repository.LockRepository
import co.openapp.app.data.source.Resource
import co.openapp.app.ui.base.BaseViewModel
import co.openapp.app.utils.AbsentLiveData
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.livedata.pref.LivePreference
import timber.log.Timber
import javax.inject.Inject

class GeneralViewModel @Inject constructor(
        private val authRepository: AuthRepository,
        private val lockRepository: LockRepository
) : BaseViewModel() {

    private val _refreshProfile: MutableLiveData<Boolean> = MutableLiveData()
    private val refreshProfile: LiveData<Boolean>
        get() = _refreshProfile

    private val _refreshLock: MutableLiveData<Boolean> = MutableLiveData()
    private val refreshLock: LiveData<Boolean>
        get() = _refreshLock

    private val _refreshSync: MutableLiveData<Boolean> = MutableLiveData()
    private val refreshSync: LiveData<Boolean>
        get() = _refreshSync

    private val _initLock: MutableLiveData<KeyRequest> = MutableLiveData()
    private val initLock: LiveData<KeyRequest>
        get() = _initLock

    private val savedLockList = MediatorLiveData<List<LockData>>()

    fun getSavedLockList() = lockRepository.loadLockList(false)

    private var value = true

    val isLogin: Boolean
        get() = lockRepository.isLogin()

    val isForceOnline: Boolean
        get() = lockRepository.isForceOnline(AppSnippet.currDateEpochSecond)

    fun refreshProfile() {
        value = !value
        _refreshProfile.value = value
    }

    fun refreshLock() {
        value = !value
        _refreshLock.value = value
    }

    fun refreshSync() {
        value = !value
        _refreshSync.value = value
    }

    val clickedLock = MediatorLiveData<LockData>()

    fun isLockRule(macAddress: String, lockId: String) {
        lockRepository.createSyncData(lockId)
        var clicked = true

        // Do Business logic here
        val fetchLockLiveData = lockRepository.fetchLock(macAddress)

        clickedLock.addSource(fetchLockLiveData) { lockItem ->
            if (clicked) {
                clicked = false
                val tripSheetSuccessList = ArrayList<TripSheetRules>()
                val tripSheetErrorList = ArrayList<TripSheetRules>()
                val locationSuccessList = ArrayList<LocationRules>()
                val locationErrorList = ArrayList<LocationRules>()
                val timeSuccessList = ArrayList<TimeRules>()
                val timeErrorList = ArrayList<TimeRules>()
                var message = ""

                // Check for time constraint
                var timeRules = lockItem.timeRules
                if (timeRules.isNotEmpty()) {
                    val currentTime = AppSnippet.currDateEpochSecond
                    var isTimeAccess = false
                    run loop@{
                        timeRules.forEach { time ->
                            time.validDates.forEach { timeItem ->
                                if (currentTime in timeItem.startDate..timeItem.endDate) {
                                    isTimeAccess = if (time.remainingAccess >= 0) {
                                        val savedRemainingAccess = getRemainingAccess(time.ruleId)
                                        savedRemainingAccess >= 0 && savedRemainingAccess < time.remainingAccess
                                    } else {
                                        true
                                    }
                                }
                                Timber.d("${timeItem.startDate} - $currentTime -${timeItem.endDate}")
                            }

                            if (isTimeAccess) {
                                timeSuccessList.add(time)
                            } else {
                                timeErrorList.add(time)
                            }
                        }
                    }
                }

                // Check for location constraint
                var locationRules = lockItem.locationRules
                if (locationRules.isNotEmpty()) {
                    val currentLocation = getCurrentLocation()
                    var isLocationAccess: Boolean
                    run loop@{
                        locationRules.forEach { location ->
                            val start = Location("Start DeviceLocation")
                            if (currentLocation != null) {
                                start.latitude = currentLocation.latitude
                                start.longitude = currentLocation.longitude
                            }
                            val end = Location("End DeviceLocation")
                            end.latitude = location.latitude
                            end.longitude = location.longitude

                            val distance = start.distanceTo(end)
                            isLocationAccess = distance <= location.radius

                            if (isLocationAccess) {
                                locationSuccessList.add(location)
                            } else {
                                locationErrorList.add(location)
                            }
                            Timber.d("${start.latitude},${start.longitude}-${end.latitude},${end.longitude}-$distance")
                        }
                    }
                }

                // Check for trip sheet constraint
                var tripSheetRules = lockItem.tripSheet
                if (tripSheetRules.isNotEmpty()) {
                    val currentLocation = getCurrentLocation()
                    var isLocationAccess: Boolean
                    run loop@{
                        tripSheetRules.forEach { location ->
                            val start = Location("Start DeviceLocation")
                            if (currentLocation != null) {
                                start.latitude = currentLocation.latitude
                                start.longitude = currentLocation.longitude
                            }
                            val end = Location("End DeviceLocation")
                            end.latitude = location.latitude
                            end.longitude = location.longitude

                            val distance = start.distanceTo(end)
                            isLocationAccess = distance <= location.radius

                            if (isLocationAccess) {
                                tripSheetSuccessList.add(location)
                            } else {
                                tripSheetErrorList.add(location)
                            }
                            Timber.d("${start.latitude},${start.longitude}-${end.latitude},${end.longitude}-$distance")
                        }
                    }
                }

                val errorList = ArrayList<String>()

                tripSheetRules = if (tripSheetRules.isNotEmpty() && tripSheetSuccessList.isEmpty()) {
                    errorList.add("app_unauthorized_geofence")
                    message = "Out of Geofenced Location, Trip Sheet"
                    tripSheetErrorList
                } else {
                    tripSheetSuccessList
                }

                locationRules = if (locationRules.isNotEmpty() && locationSuccessList.isEmpty()) {
                    errorList.add("app_unauthorized_geofence")
                    message = "Out of Geofenced Location"
                    locationErrorList
                } else {
                    locationSuccessList
                }

                timeRules = if (timeRules.isNotEmpty() && timeSuccessList.isEmpty()) {
                    errorList.add("app_unauthorized_time")
                    message = "Access Time Expired"
                    timeErrorList
                } else {
                    timeSuccessList
                }

                setRuleError(timeRules, locationRules, tripSheetRules, errorList)

                if (errorList.isEmpty()) {
                    lockItem.timeRules = timeRules
                    lockItem.tripSheet = tripSheetRules
                    clickedLock.value = lockItem
                } else {
                    setSyncEngaged(false)
                    showErrorSheet.postValue(message)
                }
            }
        }
    }

    fun logOut() {
        lockRepository.clearToken()
        authRepository.clearProfileData()
    }

    fun saveCurrentLocation(deviceLocation: DeviceLocation) {
        lockRepository.saveCurrentLocation(deviceLocation)
    }

    private fun getCurrentLocation(): DeviceLocation? {
        return lockRepository.getCurrentLocation()
    }

    private fun setRuleError(timeRules: ArrayList<TimeRules>,
                             locationRules: ArrayList<LocationRules>,
                             tripSheetRules: ArrayList<TripSheetRules>,
                             errorList: ArrayList<String> = ArrayList()) {
        lockRepository.syncRuleError(timeRules, locationRules, tripSheetRules, errorList)
    }

    fun swipeRefreshLockList() {
        loading.set(true)

        if (hasConnection()) {
            loading.set(false)
            refreshLock()
        } else {
            loading.set(false)
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    fun initApi(key: KeyRequest) {
        loading.set(true)

        if (hasConnection()) {
            _initLock.value = key
        } else {
            showErrorSheet.value = getString(R.string.no_internet_connection)
        }
    }

    // load lock from network
    val liveLockListRepositories: LiveData<Resource<List<LockData>>> = Transformations
            .switchMap(refreshLock) {
                if (hasConnection()) {
                    lockRepository.loadLockList(true)
                } else {
                    AbsentLiveData.create()
                }
            }

    // push data to network
    val liveSyncDataListRepositories: LiveData<Resource<SyncResponse>> = Transformations
            .switchMap(refreshSync) {
                if (hasConnection()) {
                    lockRepository.pushSyncData()
                } else {
                    AbsentLiveData.create()
                }
            }

    // push data to network
    val liveProfileDetailRepositories: LiveData<Resource<List<UserData>>> = Transformations
            .switchMap(refreshProfile) {
                if (hasConnection()) {
                    authRepository.loadProfileDetail(true)
                } else {
                    AbsentLiveData.create()
                }
            }

    // init door lock to server
    val liveInitLockRepositories: LiveData<Resource<SyncResponse>> = Transformations
            .switchMap(initLock) { lock ->
                lockRepository.pushLockData(lock)
            }

    fun isSyncEngaged(): Boolean {
        return lockRepository.isSyncEngaged()
    }

    private fun setSyncEngaged(value: Boolean) {
        Timber.e("setsync - $value")
        lockRepository.setSyncEngaged(value)
    }

    fun callGeneralApi() = authRepository.doGeneralApi()
    fun getAppVersion(): Long = authRepository.getVersionCode()
    private fun getRemainingAccess(key: String) = lockRepository.getRemainingAccess(key)

    init {
        val liveLockList = Transformations
                .switchMap(refreshLock) {
                    lockRepository.loadSaveLockList()
                }

        savedLockList.addSource(liveLockList, savedLockList::setValue)
    }
}