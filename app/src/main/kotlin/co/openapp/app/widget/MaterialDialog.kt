package co.openapp.app.widget

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Dialog
import android.content.Context
import android.view.ViewPropertyAnimator
import co.openapp.app.R
import co.openapp.app.utils.AnimationUtils
import co.openapp.app.utils.extensions.onClick
import co.openapp.app.utils.extensions.scale
import kotlinx.android.synthetic.main.dialog_item.*

/**
 * Created by deepanshu on 10.09.2017.
 */
class MaterialDialog(context: Context) : Dialog(context, R.style.MaterialDialogSheet) {

    private var otpValue: String = ""

    init {
        setContentView(R.layout.dialog_item)
        setCanceledOnTouchOutside(false)
        otpView.setOtpCompletionListener { otp ->
            otpValue = otp
        }
    }

    override fun onDetachedFromWindow() {
        super.dismiss()
        super.onDetachedFromWindow()
    }

    fun title(title: String): MaterialDialog {
        titleTv.text = title
        return this
    }

    fun message(message: String): MaterialDialog {
        messageTv.text = message
        return this
    }

    fun getProductId(): String {
        return messageEt.text.toString()
    }

    fun getOtp(): String {
        return otpValue
    }

    fun onClickButton(text: String, action: (MaterialDialog.() -> Unit)?): MaterialDialog {
        positiveButton.text = text
        positiveButton.onClick {
            action?.let { it() }
            otpValue = ""
            otpView.setText("")
        }
        return this
    }

    override fun show() {
        super.show()
        with(container) {
            alpha = 0F
            scale = .85F
            animate()
                    .scale(1F)
                    .alpha(1F)
                    .setDuration(120)
                    .setInterpolator(AnimationUtils.FAST_OUT_LINEAR_IN_INTERPOLATOR)
                    .withLayer()
                    .start()
        }
    }

    override fun hide() {
        with(container) {
            animate()
                    .scale(.85F)
                    .alpha(0F)
                    .setDuration(110)
                    .setInterpolator(AnimationUtils.FAST_OUT_LINEAR_IN_INTERPOLATOR)
                    .withLayer()
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator?) {
                            this@MaterialDialog.dismiss()
                        }
                    })
                    .start()
        }
    }

    private fun ViewPropertyAnimator.scale(scale: Float): ViewPropertyAnimator {
        scaleX(scale)
        scaleY(scale)
        return this
    }

    override fun onBackPressed() {
        if (!isShowing)
            super.onBackPressed()
    }
}