package co.openapp.app.widget.bottomsheet

import android.content.Context
import androidx.core.content.ContextCompat
import co.openapp.app.R
import co.openapp.app.utils.extensions.onClick
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.dialog_bottom_single.*

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    SingleButtonDialogFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 *
 */

class SingleButtonDialog(context: Context) : BottomSheetDialog(context) {

    init {
        val sheetView = layoutInflater.inflate(R.layout.dialog_bottom_single, null)
        setContentView(sheetView)
    }

    fun setTitle(message: String): SingleButtonDialog {
        title.text = message
        return this
    }

    fun setMessage(message: String): SingleButtonDialog {
        desc.text = message
        return this
    }

    fun setButtonTitle(message: String): SingleButtonDialog {
        btn.text = message
        return this
    }

    fun setAnimation(rawIds: Int): SingleButtonDialog {
        lottieAnimationView.setAnimation(rawIds)
        lottieAnimationView.playAnimation()
        lottieAnimationView.repeatCount = 10
        return this
    }

    fun onClickButton(action: SingleButtonDialog.() -> Unit): SingleButtonDialog {
        btn.onClick {
            action()
        }
        return this
    }

    fun setBackGroundColor(colorResId: Int, drawableResId: Int) {
        bottom_sheet_constraint.setBackgroundColor(ContextCompat.getColor(context, colorResId))
//        btn.backgroundTintList = ContextCompat.getColorStateList(context, drawableResId)
    }
}
