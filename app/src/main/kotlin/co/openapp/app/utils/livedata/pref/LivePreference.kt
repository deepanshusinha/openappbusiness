package co.openapp.app.utils.livedata.pref

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import co.openapp.app.utils.schedulers.AppScheduler
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver

@Suppress("UNCHECKED_CAST")
class LivePreference<T> constructor(updates: Observable<String>,
                                    private val preferences: SharedPreferences,
                                    private val key: String,
                                    private val defaultValue: T) : MutableLiveData<T>() {

    private val disposable = CompositeDisposable()

    init {
        disposable.add(updates.filter { t -> t == key }.subscribeOn(AppScheduler.io())
                .observeOn(AppScheduler.mainThread()).subscribeWith(object : DisposableObserver<String>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: String) {
                        postValue((preferences.all[t] as T) ?: defaultValue)
                    }

                    override fun onError(e: Throwable) {

                    }
                }))
    }

    override fun onActive() {
        super.onActive()
        value = (preferences.all[key] as T) ?: defaultValue
    }

    override fun onInactive() {
        super.onInactive()
        disposable.dispose()
    }
}