package co.openapp.app.utils

import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.crashlytics.android.answers.LoginEvent
import com.crashlytics.android.answers.SignUpEvent
import javax.inject.Singleton

@Singleton
object AppAnalytics {

    fun logSignUp(method: String, value: Boolean) {
        Answers.getInstance().logSignUp(SignUpEvent()
                .putMethod(method)
                .putSuccess(value)
        )
    }

    fun logLogin(method: String, value: Boolean) {
        Answers.getInstance().logLogin(LoginEvent()
                .putMethod(method)
                .putSuccess(value)
        )
    }

    fun logCustom(method: String, key: String, value1: String, value2:String = "") {
        if (value2.isEmpty()) {
            Answers.getInstance().logCustom(CustomEvent(method)
                    .putCustomAttribute(key, value1))
        } else {
            Answers.getInstance().logCustom(CustomEvent(method)
                    .putCustomAttribute(key, value1)
                    .putCustomAttribute(key, value2))
        }
    }
}