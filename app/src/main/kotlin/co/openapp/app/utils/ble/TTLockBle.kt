package co.openapp.app.utils.ble

import android.content.Context
import co.openapp.app.R
import co.openapp.app.data.db.Key
import co.openapp.app.data.model.request.DoorPwdUpdate
import co.openapp.app.utils.constant.Operation
import co.openapp.app.utils.constant.Operation.*
import com.ttlock.bl.sdk.api.TTLockAPI
import com.ttlock.bl.sdk.callback.TTLockCallback
import com.ttlock.bl.sdk.entity.DeviceInfo
import com.ttlock.bl.sdk.entity.Error
import com.ttlock.bl.sdk.entity.LockData
import com.ttlock.bl.sdk.scanner.ExtendedBluetoothDevice
import timber.log.Timber

class TTLockBle(context: Context) : TTLockCallback {

    private val mContext: Context = context

    private var mTTLockAPI: TTLockAPI = TTLockAPI(mContext, this)

    private var mOnFoundDeviceListener: OnFoundDeviceListener? = null

    private var mOnTTLockDeviceListener: OnTTLockDeviceListener? = null

    private lateinit var mKey: Key

    private lateinit var mOperation: Operation

    fun startBleService() {
        mTTLockAPI.startBleService(mContext)
    }

    fun stopBleService() {
        mTTLockAPI.stopBleService(mContext)
    }

    fun startBTDeviceScan() {
        mTTLockAPI.startBleService(mContext)
        mTTLockAPI.startBTDeviceScan()

        mOnFoundDeviceListener = mContext as OnFoundDeviceListener
    }

    fun stopBTDeviceScan() {
        mTTLockAPI.stopBTDeviceScan()
        mTTLockAPI.stopBleService(mContext)
    }

    fun setKey(key: Key) {
        mKey = key

        mOnTTLockDeviceListener = mContext as OnTTLockDeviceListener
    }

    fun doAddAdministrator(extendedBluetoothDevice: ExtendedBluetoothDevice) {
        mOperation = ADD_ADMIN
        mTTLockAPI.connect(extendedBluetoothDevice)
    }

    fun doGetLockTime() {
        if (mTTLockAPI.isConnected(mKey.lockMac))
            mTTLockAPI.getLockTime(
                    null,
                    mKey.lockVersion,
                    mKey.aesKeyStr,
                    mKey.timezoneRawOffset
            )
        else {
            mOperation = GET_LOCK_TIME
            mTTLockAPI.connect(mKey.lockMac)
        }
    }

    fun doUnlock() {

        if (mTTLockAPI.isConnected(mKey.lockMac)) {
            // In connected state, call unlock interface directly
            /*if (mKey.isAdmin)
                mTTLockAPI.unlockByAdministrator(
                        null,
                        mKey.uid,
                        mKey.lockVersion,
                        mKey.adminPs,
                        mKey.unlockKey,
                        mKey.lockFlagPos,
                        System.currentTimeMillis(),
                        mKey.aesKeyStr,
                        mKey.timezoneRawOffset
                )
            else
                mTTLockAPI.unlockByUser(
                        null,
                        mKey.uid,
                        mKey.lockVersion,
                        3453453,
                        3453454,
                        mKey.unlockKey,
                        mKey.lockFlagPos,
                        mKey.aesKeyStr,
                        mKey.timezoneRawOffset
                )*/
            mTTLockAPI.unlockByAdministrator(
                    null,
                    mKey.uid,
                    mKey.lockVersion,
                    mKey.adminPs,
                    mKey.unlockKey,
                    mKey.lockFlagPos,
                    System.currentTimeMillis(),
                    mKey.aesKeyStr,
                    mKey.timezoneRawOffset
            )
        } else {
            // Connect lock
            mOperation = UNLOCK
            mTTLockAPI.connect(mKey.lockMac)
        }
    }

    fun doAddPeriodKeyboardPassword(password: String) {
//        mKey.password = password

        /*if (mTTLockAPI.isConnected(mKey.lockMac))
            mTTLockAPI.addPeriodKeyboardPassword(
                    null,
                    mKey.uid,
                    mKey.lockVersion,
                    mKey.adminPs,
                    mKey.unlockKey,
                    mKey.lockFlagPos,
                    mKey.password,
                    1538764672,
                    1538765772,
                    mKey.aesKeyStr,
                    mKey.timezoneRawOffset
            )
        else {
            mOperation = ADD_PERIOD_KEYBOARD_PASSWORD
            mTTLockAPI.connect(mKey.lockMac)
        }*/
    }

    fun doSetAdminKeyboardPassword(password: String) {
        // TODO: Set Kybd Password
//        mKey.password = password

        /*if (mTTLockAPI.isConnected(mKey.lockMac))
            mTTLockAPI.setAdminKeyboardPassword(
                    null,
                    mKey.uid,
                    mKey.lockVersion,
                    mKey.adminPs,
                    mKey.unlockKey,
                    mKey.lockFlagPos,
                    mKey.aesKeyStr,
                    mKey.password
            )
        else {
            mOperation = SET_ADMIN_KEYBOARD_PASSWORD
            mTTLockAPI.connect(mKey.lockMac)
        }*/
    }

    fun doOperateLog() {

        if (mTTLockAPI.isConnected(mKey.lockMac))
            mTTLockAPI.getOperateLog(
                    null,
                    mKey.lockVersion,
                    mKey.aesKeyStr,
                    mKey.timezoneRawOffset
            )
        else {
            mOperation = GET_OPERATE_LOG
            mTTLockAPI.connect(mKey.lockMac)
        }
    }

    override fun onDeleteFingerPrint(p0: ExtendedBluetoothDevice?, p1: Int, p2: Long, p3: Error?) {
    }

    override fun onDeviceConnected(device: ExtendedBluetoothDevice?) {
        when (mOperation) {
            ADD_ADMIN -> mTTLockAPI.lockInitialize(device)
            /*ADD_PERIOD_KEYBOARD_PASSWORD -> mTTLockAPI.addPeriodKeyboardPassword(
                    device,
                    mKey.uid,
                    mKey.lockVersion,
                    mKey.adminPs,
                    mKey.unlockKey,
                    mKey.lockFlagPos,
                    mKey.password,
                    1538764672,
                    1538765772,
                    mKey.aesKeyStr,
                    mKey.timezoneRawOffset
            )*/
            /*DELETE_ONE_KEYBOARD_PASSWORD -> mTTLockAPI.deleteOneKeyboardPassword(
                    device,
                    mKey.uid,
                    mKey.lockVersion,
                    mKey.adminPs,
                    mKey.unlockKey,
                    mKey.lockFlagPos,
                    3,
                    "",
                    mKey.aesKeyStr

            )*/
            GET_LOCK_TIME -> mTTLockAPI.getLockTime(
                    device,
                    mKey.lockVersion,
                    mKey.aesKeyStr,
                    mKey.timezoneRawOffset
            )
            GET_OPERATE_LOG -> mTTLockAPI.getOperateLog(
                    device,
                    mKey.lockVersion,
                    mKey.aesKeyStr,
                    mKey.timezoneRawOffset
            )
            /*MODIFY_KEYBOARD_PASSWORD -> mTTLockAPI.modifyKeyboardPassword(
                    device,
                    mKey.uid,
                    mKey.lockVersion,
                    mKey.adminPs,
                    mKey.unlockKey,
                    mKey.lockFlagPos,
                    0,
                    "",
                    "",
                    2353453,
                    34536,
                    mKey.aesKeyStr,
                    mKey.timezoneRawOffset
            )*/
            /*SET_ADMIN_KEYBOARD_PASSWORD -> mTTLockAPI.setAdminKeyboardPassword(
                    device,
                    mKey.uid,
                    mKey.lockVersion,
                    mKey.adminPs,
                    mKey.unlockKey,
                    mKey.lockFlagPos,
                    mKey.aesKeyStr,
                    mKey.password
            )*/
            UNLOCK -> {
                /*if (mKey.isAdmin)
                    mTTLockAPI.unlockByAdministrator(
                            device,
                            mKey.uid,
                            mKey.lockVersion,
                            mKey.adminPs,
                            mKey.unlockKey,
                            mKey.lockFlagPos,
                            System.currentTimeMillis(),
                            mKey.aesKeyStr,
                            mKey.timezoneRawOffset
                    )
                else
                    mTTLockAPI.unlockByUser(
                            device,
                            mKey.uid,
                            mKey.lockVersion,
                            3453453,
                            3453454,
                            mKey.unlockKey,
                            mKey.lockFlagPos,
                            mKey.aesKeyStr,
                            mKey.timezoneRawOffset
                    )*/
                mTTLockAPI.unlockByAdministrator(
                        device,
                        mKey.uid,
                        mKey.lockVersion,
                        mKey.adminPs,
                        mKey.unlockKey,
                        mKey.lockFlagPos,
                        System.currentTimeMillis(),
                        mKey.aesKeyStr,
                        mKey.timezoneRawOffset
                )
            }
            else -> {
                Timber.e("No Operation")
            }
        }
    }

    override fun onResetEKey(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onSearchICCard(p0: ExtendedBluetoothDevice?, p1: Int, p2: String?, p3: Error?) {
    }

    override fun onSetMaxNumberOfKeyboardPassword(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onModifyKeyboardPassword(p0: ExtendedBluetoothDevice?, p1: Int, p2: String?, p3: String?, p4: Error?) {
    }

    override fun onAddICCard(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Long, p4: Error?) {
    }

    override fun onGetLockTime(p0: ExtendedBluetoothDevice?, lockTime: Long, p2: Error?) {
        Timber.e("GetLockTime-$lockTime")
    }

    override fun onSearchDeviceFeature(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Error?) {
    }

    override fun onSearchAutoLockTime(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Int, p4: Int, p5: Error?) {
    }

    override fun onOperateRemoteUnlockSwitch(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Int, p4: Int, p5: Error?) {
    }

    override fun onSearchPasscode(p0: ExtendedBluetoothDevice?, p1: String?, p2: Error?) {
    }

    override fun onModifyAutoLockTime(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Error?) {
    }

    override fun onDeleteAllKeyboardPassword(p0: ExtendedBluetoothDevice?, p1: Error?) {
    }

    override fun onResetKeyboardPassword(p0: ExtendedBluetoothDevice?, p1: String?, p2: Long, p3: Error?) {
    }

    override fun onModifyFingerPrintPeriod(p0: ExtendedBluetoothDevice?, p1: Int, p2: Long, p3: Long, p4: Long, p5: Error?) {
    }

    override fun onSearchPasscodeParam(p0: ExtendedBluetoothDevice?, p1: Int, p2: String?, p3: Long, p4: Error?) {
    }

    override fun onAddFingerPrint(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Long, p4: Error?) {
    }

    override fun onAddFingerPrint(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Long, p4: Int, p5: Error?) {
    }

    override fun onGetLockSwitchState(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Error?) {
    }

    override fun onDeleteOneKeyboardPassword(p0: ExtendedBluetoothDevice?, p1: Int, p2: String?, p3: Error?) {
    }

    override fun onDeleteICCard(p0: ExtendedBluetoothDevice?, p1: Int, p2: Long, p3: Error?) {
    }

    override fun onSetWristbandKeyToLock(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onSetLockName(p0: ExtendedBluetoothDevice?, p1: String?, p2: Error?) {
    }

    override fun onSearchFingerPrint(p0: ExtendedBluetoothDevice?, p1: Int, p2: String?, p3: Error?) {
    }

    override fun onDeviceDisconnected(p0: ExtendedBluetoothDevice?) {
    }

    override fun onResetLock(p0: ExtendedBluetoothDevice?, p1: Error?) {
    }

    override fun onEnterDFUMode(p0: ExtendedBluetoothDevice?, p1: Error?) {
    }

    override fun onSetLockTime(p0: ExtendedBluetoothDevice?, p1: Error?) {
    }

    override fun onFoundDevice(device: ExtendedBluetoothDevice?) {
        mOnFoundDeviceListener?.onFoundDevice(device)
    }

    override fun onModifyICCardPeriod(p0: ExtendedBluetoothDevice?, p1: Int, p2: Long, p3: Long, p4: Long, p5: Error?) {
    }

    override fun onSetWristbandKeyToDev(p0: Error?) {
    }

    override fun onSetAdminKeyboardPassword(p0: ExtendedBluetoothDevice?, adminKeyboardPwd: String?, error: Error?) {
        if (Error.SUCCESS == error) {
            mOnTTLockDeviceListener?.onDoorResponse(R.string.password_updated)
            mOnTTLockDeviceListener?.onSetAdminKeyboardPassword(DoorPwdUpdate(mKey.lockMac, adminKeyboardPwd))
        } else
            mOnTTLockDeviceListener?.onDoorResponse(R.string.password_error)
    }

    override fun onClearFingerPrint(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onResetKeyboardPasswordProgress(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onUnlock(p0: ExtendedBluetoothDevice?, uid: Int, uniqueid: Int, lockTime: Long, error: Error?) {
        mOnTTLockDeviceListener?.onUnlock(lockTime, error)
    }

    override fun onSetWristbandKeyRssi(p0: Error?) {
    }

    override fun onClearICCard(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onFingerPrintCollection(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onFingerPrintCollection(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Int, p4: Error?) {
    }

    override fun onGetLockVersion(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Int, p4: Int, p5: Int, p6: Error?) {
    }

    override fun onAddKeyboardPassword(p0: ExtendedBluetoothDevice?, p1: Int, p2: String?, p3: Long, p4: Long, p5: Error?) {
        Timber.e("password-$p1-$p2-$p3-$p4-${p5?.errorMsg}")
    }

    override fun onRecoveryData(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onSetDeletePassword(p0: ExtendedBluetoothDevice?, p1: String?, p2: Error?) {
    }

    override fun onLock(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Int, p4: Long, p5: Error?) {
    }

    override fun onScreenPasscodeOperate(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Error?) {
    }

    override fun onGetOperateLog(device: ExtendedBluetoothDevice?, records: String?, error: Error?) {
        mOnTTLockDeviceListener?.onGetOperateLog(records, error)
    }

    override fun onReadDeviceInfo(p0: ExtendedBluetoothDevice?, p1: DeviceInfo?, p2: Error?) {
    }

    override fun onGetAdminKeyboardPassword(p0: ExtendedBluetoothDevice?, p1: Int, p2: String?, p3: Error?) {
    }

    override fun onOperateAudioSwitch(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Int, p4: Error?) {
    }

    override fun onOperateRemoteControl(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Int, p4: Error?) {
    }

    override fun onGetDoorSensorState(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Error?) {
    }

    override fun onSetNBServer(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onGetElectricQuantity(p0: ExtendedBluetoothDevice?, p1: Int, p2: Error?) {
    }

    override fun onLockInitialize(device: ExtendedBluetoothDevice?, lockData: LockData?, error: Error?) {
        if (error == Error.SUCCESS && lockData != null) {
            mOnFoundDeviceListener?.onGetKey(lockData)
        }
    }

    override fun onOperateDoorSensorLocking(p0: ExtendedBluetoothDevice?, p1: Int, p2: Int, p3: Int, p4: Error?) {
    }
}