package co.openapp.app.utils.ble

import co.openapp.app.data.model.request.DoorPwdUpdate
import com.ttlock.bl.sdk.entity.Error

interface OnTTLockDeviceListener {
    fun onUnlock(lockTime: Long, error: Error?)

    fun onGetOperateLog(records: String?, error: Error?)

    fun onDoorResponse(message: Int)

    fun onSetAdminKeyboardPassword(doorPwdUpdate: DoorPwdUpdate)
}
