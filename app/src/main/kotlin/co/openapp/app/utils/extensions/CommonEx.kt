package co.openapp.app.utils.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import android.widget.ProgressBar
import androidx.lifecycle.LiveData
import co.openapp.app.utils.livedata.LiveEvent
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by deepanshu on 20/11/17.
 */

fun showLoadingDialog(context: Context): ProgressBar {
    val progressBar = ProgressBar(context, null, android.R.attr.progressBarStyleSmall)
    progressBar.show()
    /*if (progressBar.window != null) {
        progressBar.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
    progressBar.setContentView(R.layout.progress_dialog)*/
    progressBar.isIndeterminate = true
    /*progressBar.setCancelable(false)
    progressBar.setCanceledOnTouchOutside(false)*/
    return progressBar
}

@SuppressLint("all")
fun getDeviceId(context: Context): String {
    return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
}

fun isEmailValid(email: String): Boolean {
    val pattern: Pattern
    val matcher: Matcher
    val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    pattern = Pattern.compile(EMAIL_PATTERN)
    matcher = pattern.matcher(email)
    return matcher.matches()
}

fun isPasswordValid(password: String): Boolean {
    return password.length > 5
}

fun isPhoneNumberValid(number: String): Boolean {
    return number.length == 10
}

fun <T> unSafeLazy(initializer: () -> T): Lazy<T> {
    return lazy(LazyThreadSafetyMode.NONE) {
        initializer()
    }
}

fun <T> LiveData<T>.toSingleEvent(): LiveData<T> {
    val result = LiveEvent<T>()
    result.addSource(this) {
        result.value = it
    }
    return result
}
