package co.openapp.app.utils.ble

import com.ttlock.bl.sdk.entity.LockData
import com.ttlock.bl.sdk.scanner.ExtendedBluetoothDevice

interface OnFoundDeviceListener {
    fun onFoundDevice(extendedBluetoothDevice: ExtendedBluetoothDevice?)

    fun onGetKey(lockData: LockData)
}