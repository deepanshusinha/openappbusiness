package co.openapp.app.utils.schedulers

import io.reactivex.Scheduler

/**
 *  Interface to mock different threads during testing.
 * */

interface Scheduler {
    fun io(): Scheduler

    fun mainThread(): Scheduler
}