package co.openapp.app.utils.schedulers

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Implementation of [Scheduler] with actual threads.
 * */

object AppScheduler : Scheduler {
    override fun io(): io.reactivex.Scheduler {
        return Schedulers.io()
    }

    override fun mainThread(): io.reactivex.Scheduler {
        return AndroidSchedulers.mainThread()
    }
}