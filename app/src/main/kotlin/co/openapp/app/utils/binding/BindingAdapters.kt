package co.openapp.app.utils.binding

import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.BindingAdapter

/**
 * LockData Binding adapters specific to the app.
 */
object BindingAdapters {
    @JvmStatic
    @BindingAdapter("error")
    fun setError(editText: EditText, strOrResId: Any?) {
        if (strOrResId is Int) {
            editText.error = editText.context.getString(strOrResId)
        } else if (strOrResId != null) {
            editText.error = strOrResId as String
        }
    }

    @JvmStatic
    @BindingAdapter("onFocus")
    fun bindFocusChange(editText: EditText, onFocusChangeListener: View.OnFocusChangeListener?) {
        if (editText.onFocusChangeListener == null) {
            editText.onFocusChangeListener = onFocusChangeListener
        }
    }

    @JvmStatic
    @BindingAdapter("message")
    fun setMessage(textView: TextView, strOrResId: Any?) {
        when {
            strOrResId is Int -> textView.text = textView.context.getString(strOrResId)
            strOrResId != null -> textView.text = strOrResId as String
            else -> textView.text = ""
        }
    }
}