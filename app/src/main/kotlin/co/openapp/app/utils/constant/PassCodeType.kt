package co.openapp.app.utils.constant

enum class PassCodeType {

    /**
     * 0
     */
    NONE,

    /**
     * 1
     */
    ONE_TIME,

    /**
     * 2
     */
    PERMANENT,

    /**
     * 3
     */
    PERIOD,

    /**
     * 4
     */
    DELETE,

    /**
     * 5
     */
    WEEK_END_CYCLIC,
    /**
     * 6
     */
    DAILY_CYCLIC,

    /**
     * 7
     */
    WORD_DAY_CYCLIC,

    /**
     * 8
     */
    MONDAY_CYCLIC,

    /**
     * 9
     */
    TUESDAY_CYCLIC,

    /**
     * 10
     */
    WEDNESDAY_CYCLIC,

    /**
     * 11
     */
    THURSDAY_CYCLIC,

    /**
     * 12
     *
     */
    FRIDAY_CYCLIC,

    /**
     * 13
     *
     */
    SATURDAY_CYCLIC,

    /**
     * 14
     *
     */
    SUNDAY_CYCLIC
}