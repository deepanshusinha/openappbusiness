package co.openapp.app.utils.ble

import com.polidea.rxandroidble2.RxBleDevice
import io.reactivex.subjects.PublishSubject

/**
 * Created by deepanshusinha on 02/02/18.
 */

object RxBle {

//    private var rxBleClient: RxBleClient = RxBleClient.create(OpenApp.instance.applicationContext)

//    private lateinit var connectionObservable: Observable<RxBleConnection>

    private lateinit var bleDevice: RxBleDevice

    private val disconnectTriggerSubject = PublishSubject.create<Boolean>()

    /*private fun prepareConnectionObservable(): Observable<RxBleConnection> {
        return bleDevice
                .establishConnection(false)
                .takeUntil(disconnectTriggerSubject)
                .compose(ConnectionSharingAdapter())
    }

    fun connect(macAddress: String) {
        bleDevice = rxBleClient.getBleDevice(macAddress)

        connectionObservable = prepareConnectionObservable()

        connectionObservable
                .flatMap(RxBleConnection::discoverServices)
                .flatMap { t: RxBleDeviceServices? -> t?.getCharacteristic(uuid) }
                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe { wait.visibility = View.GONE }
                .subscribe({ t: BluetoothGattCharacteristic? ->
                    Timber.e(t.toString())
                }) { throwable -> throwable.printStackTrace() }
    }

    fun disconnect() {
        disconnectTriggerSubject.onNext(true)
    }

    fun isConnected(): Boolean {
        return bleDevice.connectionState == RxBleConnection.RxBleConnectionState.CONNECTED
    }

    // Read
    fun readBle(characteristicUUID: UUID) {
        connectionObservable
                .flatMap { t: RxBleConnection? ->
                    t?.readCharacteristic(characteristicUUID)
                }
                .observeOn(AppScheduler.mainThread())
                .subscribe({ _: ByteArray? ->
                    //                                        handleSuccess(t)
                }) { throwable -> throwable.printStackTrace() }
    }

    // Write
    fun writeBle(characteristicUUID: UUID) {
        connectionObservable
                .flatMap { t: RxBleConnection? ->
                    t?.writeCharacteristic(characteristicUUID, Constants.AppKeys.UNLOCK_KEY.toByteArray())
                }
                .observeOn(AppScheduler.mainThread())
                .subscribe({ _: ByteArray? ->
                    //                    handleSuccess(t)
                }) { throwable -> throwable.printStackTrace() }
    }*/
}