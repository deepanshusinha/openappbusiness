package co.openapp.app.utils.livedata.pref

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import co.openapp.app.utils.schedulers.AppScheduler
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver

@Suppress("UNCHECKED_CAST")
class MultiPreference<T> constructor(updates: Observable<String>,
                                     private val preferences: SharedPreferences,
                                     private val keys: List<String>,
                                     private val defaultValue: T) : MutableLiveData<Pair<String, T>>() {

    private val disposable = CompositeDisposable()

    init {
        disposable.add(updates.filter { t -> keys.contains(t) }.subscribeOn(AppScheduler.io())
                .observeOn(AppScheduler.mainThread()).subscribeWith(object : DisposableObserver<String>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: String) {
                        postValue(Pair(t, (preferences.all[t] as T) ?: defaultValue))
                    }

                    override fun onError(e: Throwable) {

                    }
                }))
    }

    override fun onActive() {
        super.onActive()
        value = Pair(keys[0], (preferences.all[keys[0]] as T) ?: defaultValue)
    }
}