package co.openapp.app.utils.livedata.pref

import androidx.lifecycle.MutableLiveData
import co.openapp.app.utils.schedulers.AppScheduler
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver

class MultiPreferenceAny constructor(updates: Observable<String>,
                                     private val keys: List<String>) : MutableLiveData<String>() {

    private val disposable = CompositeDisposable()

    init {
        disposable.add(updates.filter { t -> keys.contains(t) }.subscribeOn(AppScheduler.io())
                .observeOn(AppScheduler.mainThread()).subscribeWith(object : DisposableObserver<String>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: String) {
                        postValue(t)
                    }

                    override fun onError(e: Throwable) {

                    }
                }))
    }

    override fun onActive() {
        super.onActive()
        value = keys[0]
    }
}