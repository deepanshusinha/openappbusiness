package co.openapp.app.utils.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony

/**
 * A broadcast receiver who listens for incoming SMS
 */
class SmsBroadcastReceiver(private val serviceProviderNumber: String, private val serviceProviderSmsCondition: String) : BroadcastReceiver() {

    private var listener: Listener? = null

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Telephony.Sms.Intents.SMS_RECEIVED_ACTION) {
            var smsSender = ""
            var smsBody = ""
            for (smsMessage in Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                smsSender = smsMessage.displayOriginatingAddress
                smsBody += smsMessage.messageBody
            }

            /*if (smsSender == serviceProviderNumber && smsBody.startsWith(serviceProviderSmsCondition)) {
                listener?.onTextReceived(smsBody)
            }*/
            listener?.onTextReceived(smsBody)
        }
    }

    internal fun setListener(listener: Listener) {
        this.listener = listener
    }

    internal interface Listener {
        fun onTextReceived(text: String)
    }
}
