package co.openapp.app.utils


import com.google.android.gms.common.util.Base64Utils
import timber.log.Timber
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object AES {
    private var AES_ALGORITHM = "AES"
    private var AES_TRANSFORMATION = "AES/CBC/PKCS5Padding"
    private var INIT_VECTOR = IvParameterSpec("1234567890123456".toByteArray())


    private var secretKey: SecretKeySpec? = null
    private var key: ByteArray? = null

    /*private fun setKey(myKey: String) {
        val sha: MessageDigest?
        try {
            key = myKey.toByteArray()
            val random = SecureRandom()
            val salt = ByteArray(16)
            random.nextBytes(salt)

            val spec = PBEKeySpec(myKey.toCharArray(), salt, 65536, 128) // AES-256
            val f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
            val key = f.generateSecret(spec).encoded
//            sha = MessageDigest.getInstance("SHA-1")
//            key = sha.digest(key)
//            key = Arrays.copyOf(key, 16)
            Timber.e(String(key as ByteArray))
            secretKey = SecretKeySpec(key, AES_ALGORITHM)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            Timber.e(e.toString())
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
            Timber.e(e.toString())
        }

    }*/

    fun encrypt(strToEncrypt: String, secret: String): String? {
        try {
//            setKey(secret)
            val secretKeySpec = SecretKeySpec(secret.toByteArray(), AES_ALGORITHM)
            val cipher = Cipher.getInstance(AES_TRANSFORMATION)
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, INIT_VECTOR)
            val inputBytes = cipher.doFinal(strToEncrypt.toByteArray())
            return Base64Utils.encode(inputBytes)
        } catch (e: Exception) {
            Timber.e("Error while encrypting: %s", e.toString())
        }

        return null
    }

    fun decrypt(strToDecrypt: String, secret: String): String? {
        try {
            val secretKeySpec = SecretKeySpec(secret.toByteArray(), AES_ALGORITHM)
//             setKey(secret) // this used to get encrypted key if doing data decryption without encrypted key knowledge
            // or if don't know encryption key used to encrypted data previously

            val cipher = Cipher.getInstance(AES_TRANSFORMATION)
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, INIT_VECTOR)

            val inputBytes = cipher.doFinal(Base64Utils.decode(strToDecrypt))
            return String(inputBytes)

        } catch (e: Exception) {
            Timber.e("Error while decrypting: %s", e.toString())
        }

        return null
    }
}
