package co.openapp.app.utils.workmanager

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import co.openapp.app.OpenApp
import co.openapp.app.data.repository.OpenAppRepository
import co.openapp.app.di.component.DaggerAppComponent
import timber.log.Timber
import javax.inject.Inject

class ListWorkManager(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    @Inject
    lateinit var repository: OpenAppRepository

    override fun doWork(): Result {
        Timber.e("WM: before Dagger")
        if (applicationContext is OpenApp)
            DaggerAppComponent.builder()
                    .application(applicationContext as OpenApp)
                    .build()
                    .inject(this)

        sync()
        Timber.e("WM: after Dagger")

        return Result.success()
    }

    private fun sync() {
        repository.updateList()
    }
}