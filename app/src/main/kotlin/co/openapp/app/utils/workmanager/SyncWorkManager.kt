package co.openapp.app.utils.workmanager

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import co.openapp.app.OpenApp
import co.openapp.app.di.component.DaggerAppComponent

class SyncWorkManager(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    override fun doWork(): Result {
        if (applicationContext is OpenApp)
            DaggerAppComponent.builder()
                    .application(applicationContext as OpenApp)
                    .build()
                    .inject(this)

        sync()

        return Result.success()
    }

    private fun sync() {

    }
}