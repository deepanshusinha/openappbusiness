package co.openapp.app.utils.constant

/**
 * Created by 107si on 07-12-2017.
 */
interface Constants {

    interface IntentKeys {
        companion object {
            const val MAC_ADDRESS = "macAddress"
            const val DEVICE_NAME = "deviceName"
            const val OTP_NAME = "otpName"
            const val ALLOW_EVENT = "allowEvent"
            const val ALLOW = "allow"
            const val DOOR_KEY = "doorKey"
            const val PADLOCK_TOKEN_KEY = "padlockTokenKey"
            const val PADLOCK_PWD_KEY = "padlockPwdKey"
            const val SCANNED_LOCK_LIST = "scannedLockList"
            const val PHONE_NUMBER = "phoneNumber"
        }
    }

    interface RequestKeys {
        companion object {
            const val REQUEST_CODE = 80

            const val REQUEST_CODE_SCAN = 50

            const val REQUEST_ENABLE_BT = 20

            /**
             * Constant used in the location settings dialog.
             */
            const val REQUEST_CHECK_SETTINGS = 34

            const val SMS_PERMISSION_CODE = 60

            const val MAKE_CALL_PERMISSION_REQUEST_CODE = 200
        }
    }

    interface DateKeys {
        companion object {
            const val YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss"
            const val YYYYMMDD_HHMM = "yyyy-MM-dd HH:mm"
        }
    }

    interface SharedKeys {
        companion object {
            const val USER_DETAILS = "userDetails"
            const val FILE_NAME = "open-app"
            const val PUSH_DATA = "pushDataV1"
            const val USER_LOCKS = "userLocks"
            const val USER_LOCATION = "userLocation"
            const val CABINET_OTP = "cabinetOtp"
            const val INIT_TT_LOCK = "initTTLock"
            const val VERSION_CODE = "versionCode"
            const val VERSION_NAME = "versionName"
            const val NOKE_PWD_UPDATE = "nokePwdUpdate"
            const val ACCESS_TOKEN = "accessToken"
            const val REFRESH_TOKEN = "refreshToken"
            const val COMPANY_ID = "companyId"
            const val SERVER_TIME_UTC = "X-OA-SERVER-TIME"
            const val NEW_API = "newApi"
            const val TOKEN = "token"
            const val DATA_SAVED_TIME = "dataSavedTime"
            //            const val FRESH_TIMEOUT_IN_HOURS = 86400L
            const val FRESH_TIMEOUT_IN_HOURS = "timeoutHours"
            const val FIRST_TIME = "FIRST_TIME"
            const val SYNC = "sync"
            const val GENERAL = "general"
            const val RULE_STARTS_WITH = "rule_"
            const val LOCK_STATUS = "lockStatus"
        }
    }

    interface LockType {
        companion object {
            const val CABINET_LOCK = "CabinetLock"
            const val DOOR_LOCK = "doorlock"
            const val PAD_LOCK = "padlock"
        }
    }

    interface CabinetKeys {
        companion object {
            const val UNLOCK = "unlock"
            const val MASTER_CODE = "masterCode"
            const val APP_CODE = "appCode"
            const val UID = "uid"
            const val GET_INFO = "getInfo"
            const val RESTART = "restart"
            const val SLEEP = "sleep"
            const val REG_ADMIN = "registerAdmin"
            const val SOS = "sos"
        }
    }

    interface WorkManagerKeys {
        companion object {
            const val List_PERIOD_WORK = "listPeriodWork"
        }
    }

    interface GeofenceKeys {
        companion object {

            const val GEOFENCE_RADIUS_IN_METERS = 100f // 100 meters
            const val GEOFENCE_EXPIRATION_IN_MILLISECONDS = (10 * 60 * 1000).toLong() // 10 minutes
        }
    }

    interface DatabaseKeys {
        companion object {
            const val TABLE_LOCK = "lock"
            const val TABLE_USER = "user"
            const val DB_NAME = "oab2b.db"
        }
    }

    interface AnalyticsKeys {
        companion object {
            const val OPERATIONS = "Operation"
            const val CONNECTED = "Connected"
            const val DIS_CONNECTED = "Disconnected"
            const val UNLOCKED = "Unlocked"
        }
    }
}