package co.openapp.app.utils

import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import java.util.*

/**
 * Created by 107si on 07-12-2017.
 */

object BleSnippet {

    /**
     * Gatt Attributes
     */
    var HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb"
    var CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb"
    var BATTERY_SERVICE_UUID = "0000180f-0000-1000-8000-00805f9b34fb"
    var BATTERY_LEVEL_UUID = "00002a19-0000-1000-8000-00805f9b34fb"
    var UART_SERVICE_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e"
    var RX_CHARACTERISTIC_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
    var TX_CHARACTERISTIC_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca9e"
    var SENSOR_SERVICE_UUID= "6e400300-b5a3-f393-e0a9-e50e24dcca9e"
    var SENSOR_WRITE_UUID = "6e400302-b5a3-f393-e0a9-e50e24dcca9e"
    var SENSOR_READ_UUID = "6e400301-b5a3-f393-e0a9-e50e24dcca9e"
    var MODEL_NUMBER_STRING = "00002a24-0000-1000-8000-00805f9b34fb"


    /**
     * All service and characteristics UUID
     */
    val UUID_HEART_RATE_MEASUREMENT = UUID.fromString(HEART_RATE_MEASUREMENT)
    val UUID_BATTERY_SERVICE = UUID.fromString(BATTERY_SERVICE_UUID)
    val UUID_BATTERY_LEVEL = UUID.fromString(BATTERY_LEVEL_UUID)
    val UUID_UART_SERVICE = UUID.fromString(UART_SERVICE_UUID)
    val UUID_RX_CHARACTERISTIC = UUID.fromString(RX_CHARACTERISTIC_UUID)
    val UUID_TX_CHARACTERISTIC = UUID.fromString(TX_CHARACTERISTIC_UUID)
    val UUID_SENSOR_WRITE_SERVICE = UUID.fromString(SENSOR_WRITE_UUID)
    val UUID_SENSOR_READ_SERVICE = UUID.fromString(SENSOR_READ_UUID)
    val UUID_DEVICE_READ_SERVICE = UUID.fromString(MODEL_NUMBER_STRING)


    fun isServicePrimary(service: BluetoothGattService): Boolean {
        return service.type == BluetoothGattService.SERVICE_TYPE_PRIMARY
    }

    fun isServiceSecondry(service: BluetoothGattService): Boolean {
        return service.type == BluetoothGattService.SERVICE_TYPE_SECONDARY
    }

    fun isCharacteristicNotifiable(characteristic: BluetoothGattCharacteristic): Boolean {
        return characteristic.properties and BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0
    }

    fun isCharacteristicReadable(characteristic: BluetoothGattCharacteristic): Boolean {
        return characteristic.properties and BluetoothGattCharacteristic.PROPERTY_READ != 0
    }

    fun isCharacteristicWriteable(characteristic: BluetoothGattCharacteristic): Boolean {
        return characteristic.properties and (BluetoothGattCharacteristic.PROPERTY_WRITE or BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) != 0
    }
}