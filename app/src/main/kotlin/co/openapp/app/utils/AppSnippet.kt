package co.openapp.app.utils

import co.openapp.app.utils.constant.Constants
import io.tempo.Tempo
import org.threeten.bp.Instant
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*


object AppSnippet {

    private const val HEX_CHARS = "0123456789ABCDEF"
    private val HEX_ARRAY = HEX_CHARS.toCharArray()

    val currDate: String
        get() {
            val date = Date()
            val tempoDate = Tempo.now()

            return if (tempoDate != null)
                getDateStringFromMilliseconds(tempoDate)
            else
                getDateStringFromDate(date, Constants.DateKeys.YYYYMMDD_HHMMSS)
        }

    val currDateEpochSecond: Long
        get() {
            return Tempo.now() ?: Instant.now().epochSecond
        }

    fun getDateStringFromMilliseconds(milliseconds: Long): String {
        return getDateStringFromDate(Date(milliseconds), Constants.DateKeys.YYYYMMDD_HHMMSS)
    }

    fun getDatefromDateString(date: String): Date? {
        val format = SimpleDateFormat(Constants.DateKeys.YYYYMMDD_HHMMSS, Locale.US)
        return format.parse(date)
    }

    private fun getDateStringFromDate(date: Date, neededFormat: String = Constants.DateKeys.YYYYMMDD_HHMMSS): String {
        val format = SimpleDateFormat(neededFormat, Locale.US)
        format.timeZone = TimeZone.getTimeZone("UTC")
        return format.format(date)
    }

    fun getDateStringFromTimestamp(dateString: String): String {
        val date = Date(dateString.toLong() * 1000)
        return getDateStringFromDate(date)
    }

    fun getEpochFromDateString(date: String, neededFormat: String = Constants.DateKeys.YYYYMMDD_HHMMSS): Long {
        val format = SimpleDateFormat(neededFormat, Locale.US)
        return format.parse(date).time
    }

    fun hash256(data: String): String {
        val bytes = MessageDigest
                .getInstance("SHA-256")
                .digest(data.toByteArray())
        val result = StringBuilder(bytes.size * 2)
        var byte: Int
        bytes.forEach {
            byte = it.toInt()
            result.append(HEX_CHARS[byte shr 4 and 0x0f])
            result.append(HEX_CHARS[byte and 0x0f])
        }

        return result.toString()
    }

    fun bytesToHex(bytes: ByteArray): String {
        val hexChars = CharArray(bytes.size * 2)

        for (j in 0 until bytes.size) {
            val v = bytes[j].toInt() and 0xFF
            hexChars[j * 2] = HEX_ARRAY[v.ushr(4)]
            hexChars[j * 2 + 1] = HEX_ARRAY[v and 0x0F]
        }

        /*val result = StringBuilder(bytes.size * 2)
        var byte: Int
        bytes.forEach {
            byte = it.toInt()
            result.append(HEX_ARRAY[byte ushr 4 and 0xff])
            result.append(HEX_ARRAY[byte and 0x0f])
        }*/

        return String(hexChars)
    }

    fun hexToBytes(hexRepresentation: String): ByteArray {
        if (hexRepresentation.length % 2 == 1) {
            throw IllegalArgumentException("hexToBytes requires an even-length String parameter")
        }

        val len = hexRepresentation.length
        val data = ByteArray(len / 2)

        var i = 0
        while (i < len) {
            data[i / 2] = ((Character.digit(hexRepresentation[i], 16) shl 4) + Character.digit(hexRepresentation[i + 1], 16)).toByte()
            i += 2
        }

        return data
    }
}