package co.openapp.app.utils.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri
import co.openapp.app.R
import co.openapp.app.ui.home.HomeActivity
import co.openapp.app.ui.operate.cabinet.CabinetActivity
import co.openapp.app.ui.operate.door.DoorActivity
import co.openapp.app.ui.operate.nokelock.NokelockActivity
import co.openapp.app.ui.operate.padlock.PadlockActivity

/**
 * Created by deepanshusinha on 21/02/18.
 */

fun Context.intentHomeActivity(): Intent {
    return Intent(this, HomeActivity::class.java).apply {
        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }
}

fun Context.intentCabinetActivity(): Intent {
    return Intent(this, CabinetActivity::class.java)
}

fun Context.intentDoorActivity(): Intent {
    return Intent(this, DoorActivity::class.java)
}

fun Context.intentPadlockActivity(): Intent {
    return Intent(this, PadlockActivity::class.java)
}

fun Context.intentNokeLockActivity(): Intent {
    return Intent(this, NokelockActivity::class.java)
}

fun Context.intentPlayStoreActivity(): Intent {
    return Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.play_store_url)))
}

