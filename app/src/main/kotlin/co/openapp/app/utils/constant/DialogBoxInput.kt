package co.openapp.app.utils.constant

enum class DialogBoxInput {

    TEXT_VIEW,

    EDIT_TEXT,

    OTP_VIEW
}