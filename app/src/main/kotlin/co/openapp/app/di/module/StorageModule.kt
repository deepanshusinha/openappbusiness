package co.openapp.app.di.module

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import co.openapp.app.data.db.LockDao
import co.openapp.app.data.db.OaB2bDb
import co.openapp.app.data.db.UserDao
import co.openapp.app.data.prefs.OpenAppPreferences
import co.openapp.app.data.prefs.OpenAppPreferencesLiveData
import co.openapp.app.data.repository.AppRepository
import co.openapp.app.utils.Utils
import co.openapp.app.utils.constant.Constants
import com.commonsware.cwac.saferoom.SafeHelperFactory
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by deepanshusinha on 17/02/18.
 */

@Module
class StorageModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(application: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application)
    }

    @Provides
    @Singleton
    fun provideOpenAppPreferences(sharedPreferences: SharedPreferences, gson: Gson): OpenAppPreferences {
        return OpenAppPreferences(sharedPreferences, gson)
    }

    @Provides
    @Singleton
    fun provideOpenAppPreferencesLiveData(sharedPreferences: SharedPreferences): OpenAppPreferencesLiveData {
        return OpenAppPreferencesLiveData(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideAppRepository(utils: Utils, preferences: OpenAppPreferences, livePreference: OpenAppPreferencesLiveData): AppRepository {
        return AppRepository(utils, preferences, livePreference)
    }

    @Provides
    @Singleton
    fun provideSafeHelperFactory(): SafeHelperFactory {
        return SafeHelperFactory("12355".toCharArray())
    }

    @Provides
    @Singleton
    fun provideOaB2bDb(application: Application, safeHelperFactory: SafeHelperFactory): OaB2bDb {
        return Room.databaseBuilder(application.applicationContext, OaB2bDb::class.java, Constants.DatabaseKeys.DB_NAME)
                .fallbackToDestructiveMigration()
                .openHelperFactory(safeHelperFactory)
                .build()
    }

    @Provides
    @Singleton
    fun provideLockDao(db: OaB2bDb): LockDao = db.lockDao()

    @Provides
    @Singleton
    fun provideUserDao(db: OaB2bDb): UserDao = db.userDao()
}