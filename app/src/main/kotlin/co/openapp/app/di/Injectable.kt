package co.openapp.app.di

/**
 * Marks an fragment injectable.
 */
interface Injectable