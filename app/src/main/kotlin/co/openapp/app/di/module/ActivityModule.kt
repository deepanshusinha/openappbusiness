package co.openapp.app.di.module

import co.openapp.app.di.scope.ActivityScope
import co.openapp.app.ui.operate.door.DoorActivity
import co.openapp.app.ui.operate.nokelock.NokelockActivity
import co.openapp.app.ui.view.main.GeneralActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by deepanshu on 16/11/17.
 */

@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [GeneralFragmentModule::class])
    abstract fun contributeGeneralActivity(): GeneralActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeNokelockActivity(): NokelockActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeDoorActivity(): DoorActivity

    /*@ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangePasswordActivity(): ChangePasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMobileActivity(): MobileActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeHomeActivity(): HomeActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeScanActivity(): ScanActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributePadlockActivity(): PadlockActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeCabinetActivity(): CabinetActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeDoorActivity(): DoorActivity*/
}