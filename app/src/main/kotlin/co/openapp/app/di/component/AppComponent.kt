package co.openapp.app.di.component

import android.app.Application
import co.openapp.app.OpenApp
import co.openapp.app.di.module.*
import co.openapp.app.utils.workmanager.ListWorkManager
import co.openapp.app.utils.workmanager.SyncWorkManager
import co.openapp.app.worker.OpenAppWorkerFactory
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by deepanshu on 16/11/17.
 */

@Singleton
@Component(modules = [
    ActivityModule::class,
    AndroidSupportInjectionModule::class,
    AppModule::class,
    NetworkModule::class,
    StorageModule::class,
    ViewModelModule::class
])
interface AppComponent {

    fun openAppWorkerFactory(): OpenAppWorkerFactory

    // Establish WorkerSubComponent as subComponent
    fun workerSubComponent(): WorkerSubComponent.Builder

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: OpenApp)

    fun inject(listWorkManager: ListWorkManager)

    fun inject(syncItemWorker: SyncWorkManager)
}