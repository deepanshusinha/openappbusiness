package co.openapp.app.di.interceptors

import android.content.Context
import android.provider.Settings
import android.util.Log
import co.openapp.app.data.api.OpenAppNewService
import co.openapp.app.data.model.request.refreshtoken.RefreshAccessTokenReq
import co.openapp.app.data.model.response.error.ErrorResponse
import co.openapp.app.data.model.response.refreshtoken.RefreshAccessTokenResponse
import co.openapp.app.data.repository.local.OpenAppLocalRepository
import com.google.gson.Gson
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import javax.inject.Inject


/**
 * Created by abhijeethallur on 25,July,2018
 */
class TokenAuthenticator : Authenticator {

    @Inject
    lateinit var tokenService: OpenAppNewService
    @Inject
    lateinit var localRepository: OpenAppLocalRepository
    @Inject
    lateinit var context: Context

    override fun authenticate(route: Route?, response: Response?): Request? {

        Log.e("authenticate", " == Entered")
        val errorBody = response?.body()?.string()
        val errorObj = Gson().fromJson(errorBody, ErrorResponse::class.java)
        Log.e("errorBody", " == " + Gson().toJson(errorBody))

        if (errorObj != null) {
            when (errorObj.code) {
                1001, 1013, 1027, in 1007..1010, in 1022..1025 -> {
                    throw Exception(errorObj.message, Throwable(errorObj.code.toString()))
                }
                1111 -> {
                    val refreshAccessTokenReq = RefreshAccessTokenReq(localRepository.getAccessToken(),
                            localRepository.getRefreshToken(), getDeviceType(), getDeviceId())

                    if (localRepository.getRefreshToken()?.isNotEmpty() == true) {
                        // Refresh your access_token using a synchronous api request
                        val refreshAccessTokenResponse: retrofit2.Response<RefreshAccessTokenResponse>? = tokenService.callRefreshAccessToken(refreshAccessTokenReq).execute()

                        return if (refreshAccessTokenResponse?.isSuccessful == true) {
                            val authToken = refreshAccessTokenResponse.body()?.data?.accessToken
                            authToken?.let { localRepository.saveAccessToken(it) }
                            response?.request()?.newBuilder()!!
                                    .header("Authorization", "Bearer $authToken")
                                    .build()
                        } else {
                            null
                        }
                    } else {
                        return null
                    }
                }
                else -> {
//                    localRepository.clearUserData()
//                    context.startActivity(context.intentStartUpActivity(false))
                    return null
                }
            }
        } else {
//            localRepository.clearUserData()
//            context.startActivity(context.intentStartUpActivity(false))
            return null
        }
    }

    private fun getDeviceId(): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    private fun getDeviceType(): String {
        return "android"
    }
}