package co.openapp.app.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.openapp.app.di.scope.ViewModelKey
import co.openapp.app.viewmodel.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel::class)
    abstract fun bindAuthViewModel(authViewModel: AuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SliderViewModel::class)
    abstract fun bindSliderViewModel(sliderViewModel: SliderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GeneralViewModel::class)
    abstract fun bindGeneralViewModel(generalViewModel: GeneralViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePasswordViewModel::class)
    abstract fun bindChangePasswordViewModel(changePasswordViewModel: ChangePasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MobileViewModel::class)
    abstract fun bindMobileViewModel(mobileViewModel: MobileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OtpViewModel::class)
    abstract fun bindOtpViewModel(otpViewModel: OtpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScanLocksViewModel::class)
    abstract fun bindScanLocksViewModel(scanLocksViewModel: ScanLocksViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PadlockViewModel::class)
    abstract fun bindPadlockViewModel(padlockViewModel: PadlockViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NokelockViewModel::class)
    abstract fun bindNokelockViewModel(nokelockViewModel: NokelockViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CabinetViewModel::class)
    abstract fun bindCabinetViewModel(cabinetViewModel: CabinetViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DoorViewModel::class)
    abstract fun bindDoorViewModel(doorViewModel: DoorViewModel): ViewModel
/*
    @Binds
    @IntoMap
    @ViewModelKey(LockListViewModel::class)
    abstract fun bindLockListViewModel(lockListViewModel: LockListViewModel): ViewModel*/

    @Binds
    abstract fun bindViewModelFactory(factory: OpenAppViewModelFactory): ViewModelProvider.Factory
}