package co.openapp.app.di.module

import android.app.Application
import android.content.Context
import co.openapp.app.utils.Utils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by deepanshusinha on 17/02/18.
 */

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideUtils(context: Context): Utils {
        return Utils(context)
    }
}