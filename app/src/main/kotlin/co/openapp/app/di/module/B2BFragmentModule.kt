package co.openapp.app.di.module

import co.openapp.app.ui.view.bb.MyLocksFragment
import co.openapp.app.ui.view.bb.ProfileFragment
import co.openapp.app.ui.view.bb.ScanLocksFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by deepanshu on 10-01-2018.
 */

@Module
abstract class B2BFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeScanLocksFragment(): ScanLocksFragment

    @ContributesAndroidInjector
    abstract fun contributeMyLocksFragment(): MyLocksFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment
}