package co.openapp.app.di.interceptors

import co.openapp.app.data.repository.local.OpenAppLocalRepository
import co.openapp.app.utils.constant.Constants
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * Created by abhijeethallur on 24,July,2018
 */
class ServerTimeInterceptor @Inject constructor(private val repository: OpenAppLocalRepository) : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val request = chain?.request()
        val response = chain?.proceed(request!!)
        repository.saveLastOnlineTime(response?.header(Constants.SharedKeys.SERVER_TIME_UTC)?.toLong()!!)
        return response
    }
}