package co.openapp.app.di.module

import co.openapp.app.ui.view.bb.*
import co.openapp.app.ui.view.general.OtpFragment
import co.openapp.app.ui.view.general.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by deepanshu on 10-01-2018.
 */

@Module
abstract class GeneralFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeIntroFragment(): IntroFragment

    @ContributesAndroidInjector
    abstract fun contributeSliderFragment(): SliderFragment

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeRegisterFragment(): RegisterFragment

    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordFragment(): ForgotPasswordFragment

    @ContributesAndroidInjector
    abstract fun contributeOtpFragment(): OtpFragment

    @ContributesAndroidInjector
    abstract fun contributeScanLocksFragment(): ScanLocksFragment

    @ContributesAndroidInjector
    abstract fun contributeMyLocksFragment(): MyLocksFragment

    @ContributesAndroidInjector
    abstract fun contributeMyScanLocksFragment(): MyScanLocksFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeChangePasswordFragment(): ChangePasswordFragment

    @ContributesAndroidInjector
    abstract fun contributePassCodeFragment(): PassCodeFragment
}