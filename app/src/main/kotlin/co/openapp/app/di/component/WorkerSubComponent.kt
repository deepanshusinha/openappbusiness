package co.openapp.app.di.component

import androidx.work.Worker
import androidx.work.WorkerParameters
import co.openapp.app.di.module.WorkerModule
import dagger.BindsInstance
import dagger.Subcomponent
import javax.inject.Provider

@Subcomponent(modules = [WorkerModule::class])
interface WorkerSubComponent {

    fun workers(): Map<Class<out Worker>, Provider<Worker>>

    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun workerParameters(param: WorkerParameters): Builder

        fun build(): WorkerSubComponent
    }
}