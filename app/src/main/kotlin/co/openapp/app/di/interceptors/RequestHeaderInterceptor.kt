package co.openapp.app.di.interceptors

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import co.openapp.app.R
import co.openapp.app.data.repository.AppRepository
import com.crashlytics.android.Crashlytics
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject
import javax.net.ssl.SSLPeerUnverifiedException

/**
 * Created by Deepanshu on 24,July,2018
 */

class RequestHeaderInterceptor @Inject constructor(
        private val appRepository: AppRepository,
        private val context: Context
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        val requestBuilder = chain?.request()?.newBuilder()!!
                .addHeader("Accept", "application/json")
                .addHeader("Content-type", "application/json")
                .addHeader("x-device-id", getDeviceId())
                .addHeader("x-device-name", getDeviceName())
                .addHeader("x-device-type", getDeviceType())
                .addHeader("x-app-version", appRepository.getVersionCode().toString())

        val accessToken = appRepository.getAccessToken()
        if (accessToken != null) {
            requestBuilder.addHeader("Authorization", "Bearer $accessToken")
        }

        val companyId = appRepository.getCompanyId()
        if (companyId != null) {
            requestBuilder.addHeader("x-company-id", companyId)
        }

        try {
            val response = chain.proceed(requestBuilder.build())
            when (response.code()) {
                401, 500 -> {
                    // TODO: Clear Token and handle login
//                    Timber.e("Unauthorised (redirect to log in)")
//                    Timber.e("Server Not Found")
//                    appRepository.clearToken()
                    return response
//                    throw Exception(context.getString(R.string.code_401))
                }
                403 -> {
                    Timber.e("Forbidden (correct credentials but the user does not have permission)")
                    throw Exception(context.getString(R.string.code_403))
                }
                404 -> {
                    Timber.e("file not found")
                    throw Exception(context.getString(R.string.code_404))
                }
                else -> {
                    return response
                }
            }
        } catch (e: SocketTimeoutException) {
            Crashlytics.logException(e)
            throw SocketTimeoutException()
        } catch (e: UnknownHostException) {
            Crashlytics.logException(e)
            throw UnknownHostException()
        } catch (e: SSLPeerUnverifiedException) {
            Crashlytics.logException(e)
            throw SSLPeerUnverifiedException("Peer not authenticated")
        } catch (e: Exception) {
            Crashlytics.logException(e)
            throw Exception()
        }
    }

    @SuppressLint("HardwareIds")
    private fun getDeviceId(): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    private fun getDeviceType(): String {
        return "android"
    }

    /** Returns the consumer friendly device name  */
    private fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        if (model.startsWith(manufacturer)) {
            return capitalize(model)
        }
        return capitalize(manufacturer) + " " + model
    }

    private fun capitalize(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true

        val phrase = StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }

        return phrase.toString()
    }
}