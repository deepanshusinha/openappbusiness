package co.openapp.app.di.module

import android.os.Environment
import co.openapp.app.BuildConfig
import co.openapp.app.data.api.OpenAppNewService
import co.openapp.app.di.interceptors.RequestHeaderInterceptor
import co.openapp.app.utils.livedata.adapter.LiveDataCallAdapterFactory
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.Cache
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by deepanshu on 16/11/17.
 * OkHttp3, Gson, Retrofit
 */

@Module
object NetworkModule {

    @Provides
    @Reusable
    @JvmStatic
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message -> Timber.d(message) }
                .setLevel(
                        if (BuildConfig.DEBUG)
                            HttpLoggingInterceptor.Level.BODY
                        else
                            HttpLoggingInterceptor.Level.NONE
                )
    }

    @Provides
    @Reusable
    @JvmStatic
    fun provideNewOkHttpClient(
            httpLoggingInterceptor: HttpLoggingInterceptor,
            requestHeaderInterceptor: RequestHeaderInterceptor
//            serverTimeInterceptor: ServerTimeInterceptor
    )
            : OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            httpClientBuilder.addInterceptor(httpLoggingInterceptor)
            httpClientBuilder.addNetworkInterceptor(StethoInterceptor())
        }

        if (!BuildConfig.DEBUG) {
            httpClientBuilder.certificatePinner(CertificatePinner.Builder()
                    .add("test.openapp.co", "sha256/ah07bYFVXE9Uu6OamJ1TLashvx+RXIDxei2Btk2PTVc=")
                    .build())
        }
        return httpClientBuilder
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .cache(Cache(Environment.getDownloadCacheDirectory(), 10 * 1024 * 1024))
                .addInterceptor(requestHeaderInterceptor)
//                .addInterceptor(serverTimeInterceptor)
//                .authenticator(TokenAuthenticator())
                .build()
    }

    // TODO: Set rules for naming convention json
    @Provides
    @Reusable
    @JvmStatic
    fun provideGson(): Gson {
        return GsonBuilder()
                .create()
    }

    @Provides
    @Reusable
    @JvmStatic
    fun provideNewRetrofit(okHttpNewClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpNewClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
    }

    @Provides
    @Reusable
    @JvmStatic
    fun provideOpenAppNewService(retrofit: Retrofit): OpenAppNewService =
            retrofit.create(OpenAppNewService::class.java)
}
