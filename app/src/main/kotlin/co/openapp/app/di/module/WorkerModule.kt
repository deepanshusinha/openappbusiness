package co.openapp.app.di.module

import androidx.work.RxWorker
import androidx.work.Worker
import androidx.work.WorkerFactory
import co.openapp.app.di.scope.WorkerScope
import co.openapp.app.worker.LockWorker
import co.openapp.app.worker.OpenAppWorkerFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class WorkerModule {

    @Binds
    @IntoMap
    @WorkerScope(LockWorker::class)
    abstract fun bindLockWorker(lockWorker: LockWorker): Worker

    /*@Binds
    abstract fun bindWorkerFactory(factory: OpenAppWorkerFactory) : WorkerFactory*/
}