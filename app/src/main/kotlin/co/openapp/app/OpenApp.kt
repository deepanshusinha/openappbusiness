package co.openapp.app

import android.app.Activity
import android.app.Application
import android.bluetooth.BluetoothManager
import android.content.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.IBinder
import android.provider.Telephony
import android.util.Log.ERROR
import android.util.Log.WARN
import androidx.multidex.MultiDex
import androidx.work.Configuration
import androidx.work.WorkManager
import co.openapp.app.data.repository.AppRepository
import co.openapp.app.di.AppInjector
import co.openapp.app.di.component.DaggerAppComponent
import co.openapp.app.utils.service.SmsBroadcastReceiver
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.facebook.stetho.Stetho
import com.jakewharton.threetenabp.AndroidThreeTen
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.internal.RxBleLog
import com.squareup.leakcanary.LeakCanary
import com.sunshine.blelibrary.inter.IBLE
import com.sunshine.blelibrary.service.BLEService
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.fabric.sdk.android.Fabric
import io.tempo.Tempo
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by deepanshu on 21/6/17.
 */

class OpenApp : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var mAppRepository: AppRepository

    private val mDebuggable = BuildConfig.DEBUG

    private val appComponent by lazy {
        DaggerAppComponent.builder()
                .application(this)
                .build()
    }

//    lateinit var boxStore: BoxStore
//        private set


    companion object {

        operator fun get(context: Context): OpenApp = context.applicationContext as OpenApp

        var instance: OpenApp? = null

        const val MIXPANEL_TOKEN = "d85293fb8bf2284d070dd211a73a026a"

        private var mBleService: BLEService? = null

        lateinit var rxBleClient: RxBleClient

        lateinit var smsBroadcastReceiver: SmsBroadcastReceiver
    }

    override fun onCreate() {
        super.onCreate()

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }

        instance = this@OpenApp

        // Initializing Crashlytics
        val core = CrashlyticsCore.Builder()
                .disabled(mDebuggable)
                .build()

        if (mDebuggable) {
            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String? {
                    return String.format("C:%s:%s",
                            super.createStackElementTag(element),
                            element.lineNumber)
                }
            })
            Stetho.initializeWithDefaults(this)
//            LeakCanary.install(this)
        } else {
            Timber.plant(object : Timber.Tree() {
                override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {

                    if (priority == ERROR || priority == WARN)
                        core.log(priority, tag, message)
                }
            })
        }

        Fabric.with(this, Crashlytics.Builder().core(core).build())

        // Initializing RxBleClient
        rxBleClient = RxBleClient.create(this)
        // Set Ble Logging
        RxBleLog.setLogger { level, tag, msg -> Timber.tag(tag).log(level, msg) }

        initBle()

        appComponent.inject(this)

        AppInjector.init(this)

        AndroidThreeTen.init(instance)

        doInstallUpdate()

        initWorkManager()

        smsBroadcastReceiver = SmsBroadcastReceiver("", "")
        registerReceiver(smsBroadcastReceiver, IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))

        // Initialize the Tempo Library
        Tempo.initialize(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    private fun doInstallUpdate() {
        // If clear the data and login again
        val versionCode = mAppRepository.getVersionCode()
        if (versionCode != null) {
            /*if (getVersionCode() > mAppRepository.getVersionCode()) {
                mAppRepository.clearAllData()
            }*/
            if (versionCode < 37) {
                mAppRepository.clearAllData()
            }
        }

        mAppRepository.saveVersionCode(getVersionCode())
        mAppRepository.saveVersionName(getPackageInfo().versionName)
    }

    override fun onTerminate() {
        unregisterReceiver(smsBroadcastReceiver)

        super.onTerminate()
        // It is to clear all the hold memory after termination
        instance = null
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    private fun initBle() {
        val intent = Intent(this, BLEService::class.java)
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE)

        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Timber.e("不支持BLE")
            return
        }
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val mBluetoothAdapter = bluetoothManager.adapter
        if (mBluetoothAdapter == null) {
            Timber.e("不支持BLE")
            return
        }
        if (!mBluetoothAdapter.isEnabled) {
            mBluetoothAdapter.enable()
        }
    }

    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mBleService = (service as BLEService.LocalBinder).service
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mBleService = null
        }
    }

    fun getIBLE(): IBLE? {
        return mBleService?.ible
    }

    private fun getPackageInfo(): PackageInfo {
        return packageManager.getPackageInfo(packageName, 0)
    }

    private fun getVersionCode(): Long {
        return if (Build.VERSION.SDK_INT >= 28) {
            getPackageInfo().longVersionCode
        } else {
            getPackageInfo().versionCode.toLong()
        }
    }

    private fun initWorkManager() {
        WorkManager.initialize(this, Configuration.Builder().run {
            setWorkerFactory(appComponent.openAppWorkerFactory())
            build()
        })
    }
}
