package co.openapp.app.ui.view.general

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.vo.ErrorResponse
import co.openapp.app.data.source.Status
import co.openapp.app.databinding.FragmentRegisterBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.utils.AppAnalytics
import co.openapp.app.utils.constant.Constants
import co.openapp.app.viewmodel.AuthViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_register.*
import timber.log.Timber

class RegisterFragment : BaseFragment<FragmentRegisterBinding, AuthViewModel>() {
    override val layoutRes: Int
        get() = R.layout.fragment_register

    override fun getViewModel(): Class<AuthViewModel> = AuthViewModel::class.java

    override fun bindingVariable(): Int = BR.register

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dataBinding.register = viewModel

        (activity as GeneralActivity).setupUI(registerNested)

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
                context,
                R.array.country_array,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            country_code.adapter = adapter
        }

        registerLogin.setOnClickListener { findNavController().navigate(R.id.action_registerFragment_to_loginFragment) }

        viewModel.showErrorSheet.observe(this, Observer {
            showSheet(getString(R.string.error), it, getString(R.string.got_it))
        })

        viewModel.registerResult.observe(this, Observer { response ->
            Timber.e(response.toString())

            when (response.status) {
                Status.SUCCESS -> {
                    viewModel.loading.set(false)
                    val responseData = response.data

                    if (responseData != null) {
                        if (responseData.status) {
                            AppAnalytics.logLogin(viewModel.auth.fields.email, true)
                            val bundle = bundleOf(Constants.IntentKeys.PHONE_NUMBER
                                    to "${country_code.selectedItem} ${viewModel.auth.fields.phoneNumber}")
                            findNavController().navigate(R.id.action_registerFragment_to_otpFragment, bundle)
                        } else {
                            AppAnalytics.logLogin(viewModel.auth.fields.email, false)
                            when (responseData.errorCode) {
                                702 -> {
                                    showSheet(getString(R.string.error), getString(R.string.code_702), getString(R.string.got_it))
                                }
                                709 -> {
                                    showSheet(getString(R.string.error), getString(R.string.code_709), getString(R.string.got_it))
                                }
                                710 -> {
                                    showSheet(getString(R.string.error), getString(R.string.code_710), getString(R.string.got_it))
                                }
                                else -> showSheet(getString(R.string.error), responseData.message, getString(R.string.got_it))
                            }
                        }
                    }
                    // TODO: Handle Else
                }
                Status.ERROR -> {
                    viewModel.loading.set(false)
                    val authResponse = Gson().fromJson(response.message, ErrorResponse::class.java)
                    showSheet(getString(R.string.error), authResponse.message, getString(R.string.got_it))
                }
                Status.LOADING -> {
                    viewModel.loading.set(true)
                    Timber.e("loading")
                }
            }
        })
    }

    private fun showSheet(title: String, message: String, action: String) {
        (activity as GeneralActivity).showErrorSheet(title, message, action)
    }
}