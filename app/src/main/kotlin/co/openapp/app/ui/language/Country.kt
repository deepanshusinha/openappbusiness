package co.openapp.app.ui.language


/**
 * Created by deepanshusinha on 13/02/18.
 */

data class Country(
        var countryName: String? = null,

        var countryCode: String? = null
)/* {

    @Id
    var id: Long = 0

//    @Backlink
//    lateinit var languages: List<Language>
}*/