package co.openapp.app.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.openapp.app.R
import com.ttlock.bl.sdk.scanner.ExtendedBluetoothDevice
import kotlinx.android.synthetic.main.inflater_door_item.view.*
import java.util.*

class DoorAdapter(private val mOnItemClickListener: DoorAdapter.OnItemClickListener) : RecyclerView.Adapter<DoorAdapter.MyViewHolder>() {

    var itemList: ArrayList<ExtendedBluetoothDevice> = ArrayList()

    private val SORTING_COMPARATOR = { lhs: ExtendedBluetoothDevice, rhs: ExtendedBluetoothDevice -> lhs.name.compareTo(rhs.name) }

    fun setData(device: ExtendedBluetoothDevice) {
        for (i in 0 until itemList.size) {
            itemList[i] = device
            return
        }

        itemList.add(device)
        Collections.sort(itemList, SORTING_COMPARATOR)
        notifyDataSetChanged()
    }

    fun clearScanResults() {
        itemList.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.inflater_door_item, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(itemList[position])
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ExtendedBluetoothDevice) {
            itemView.doorssid.text = item.name
        }

        init {
            itemView.setOnClickListener {
                if (adapterPosition > -1) {
                    mOnItemClickListener.onItemClick(adapterPosition)
                }
            }
        }
    }
}