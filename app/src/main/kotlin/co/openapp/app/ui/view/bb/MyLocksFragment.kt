package co.openapp.app.ui.view.bb

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.model.lock.LockData
import co.openapp.app.data.source.Status
import co.openapp.app.databinding.FragmentMyLocksBinding
import co.openapp.app.ui.adapter.LockAdapter
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.constant.Constants
import co.openapp.app.viewmodel.GeneralViewModel
import kotlinx.android.synthetic.main.activity_general.*
import kotlinx.android.synthetic.main.fragment_my_locks.*
import timber.log.Timber

class MyLocksFragment : BaseFragment<FragmentMyLocksBinding, GeneralViewModel>(), LockAdapter.OnItemClickListener {

    override val layoutRes: Int
        get() = R.layout.fragment_my_locks

    override fun getViewModel(): Class<GeneralViewModel> = GeneralViewModel::class.java

    override fun bindingVariable(): Int = BR.my_locks

    private lateinit var mLockAdapter: LockAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as GeneralActivity).appBar.visibility = View.VISIBLE

        mLockAdapter = LockAdapter(this@MyLocksFragment)

        dataBinding.myLocks = viewModel
        dataBinding.lockListRv.adapter = mLockAdapter

        viewModel.showErrorSheet.observe(this, Observer {
            swipeRefresh.isRefreshing = false
            (activity as GeneralActivity).showErrorSheet("Error", it, "Got It!")
        })

        viewModel.getSavedLockList().observe(this, Observer { response ->
            response.data?.let { updateList(it) }
        })

        swipeRefresh.isEnabled = true
        swipeRefresh.setOnRefreshListener {
            viewModel.swipeRefreshLockList()
        }

        viewModel.liveLockListRepositories.observe(this, Observer { response ->
            if (response != null)
                when (response.status) {
                    Status.SUCCESS -> {
                        (activity as GeneralActivity).updateLastLogin(AppSnippet.currDate)
                        swipeRefresh.isRefreshing = false
                        response.data?.let { updateList(it) }
                    }
                    Status.ERROR -> {
                        swipeRefresh.isRefreshing = false
                    }
                    Status.LOADING -> {
                        swipeRefresh.isRefreshing = true
                    }
                }
        })
    }

    private fun updateList(result: List<LockData>) {
        dataBinding.hasLocks = (result.isNotEmpty())
        try {
            mLockAdapter.currentList.clear()
        } catch (exception: UnsupportedOperationException) {
            Timber.e(exception.localizedMessage)
        } catch (exception: Exception) {
            Timber.e(exception.localizedMessage)
        }
        mLockAdapter.submitList(result)
    }

    override fun onItemClick(lockItem: LockData) {
        Timber.e(lockItem.lockName)

        if (lockItem.lockType == Constants.LockType.DOOR_LOCK)
            findNavController().navigate(MyLocksFragmentDirections.actionMyLocksFragmentToPasscodeFragment(lockItem))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}