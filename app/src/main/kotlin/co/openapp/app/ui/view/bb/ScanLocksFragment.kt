package co.openapp.app.ui.view.bb

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.model.lock.LockData
import co.openapp.app.databinding.FragmentScanLocksBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.utils.constant.Constants
import co.openapp.app.viewmodel.ScanLocksViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_general.*
import kotlinx.android.synthetic.main.fragment_scan_locks.*

class ScanLocksFragment : BaseFragment<FragmentScanLocksBinding, ScanLocksViewModel>() {
    override val layoutRes: Int
        get() = R.layout.fragment_scan_locks

    override fun getViewModel(): Class<ScanLocksViewModel> = ScanLocksViewModel::class.java

    override fun bindingVariable(): Int = BR.scan_locks

    private var scannedList: ArrayList<LockData>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as GeneralActivity).appBar.visibility = View.VISIBLE

        dataBinding.scanLocks = viewModel

        centerImage.setOnClickListener {
            startScanning()
        }

        setupLocation()

        setupBluetooth()

        setupScanning()

        val bundle = Bundle()

        viewModel.scannedList.observe(this, Observer { scannedLockList ->
            if (scannedLockList.isNullOrEmpty()) {
                scanStatus.visibility = View.VISIBLE
                viewLocks.visibility = View.GONE
                scanStatusLock.visibility = View.GONE
            } else {
                scanStatusLock.text = "${scannedLockList.size} locks found"
                scanStatusLock.visibility = View.VISIBLE
                viewLocks.visibility = View.VISIBLE
                scannedList = scannedLockList
            }
        })

        viewLocks.setOnClickListener {
            val listJson = Gson().toJson(scannedList)
            bundle.putString(Constants.IntentKeys.SCANNED_LOCK_LIST, listJson)
            findNavController().navigate(R.id.action_scanLocksFragment_to_myScanLocksFragment, bundle)
        }

        viewModel.matchLock.observe(this, Observer { lockData ->
            if (lockData != null) {
                viewModel.setData(lockData)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        (activity as GeneralActivity).forceOnline()
    }

    override fun onPause() {
        if (scanRipple.isRippleAnimationRunning) {
            viewModel.stopScan()
        }

        super.onPause()
    }

    fun startScanning() {
        doLocationOn()
        viewModel.startScan()
        scanStatus.text = ""
    }

    private fun setupScanning() {
        viewModel.doStartScan.observe(this, Observer {
            scanRipple.startRippleAnimation()
        })
    }

    private fun doLocationOn() {
        (activity as GeneralActivity).startLocation()
    }

    private fun setupLocation() {
        viewModel.doLocOn.observe(this, Observer {
            doLocationOn()
        })


        viewModel.doLocPer.observe(this, Observer {
            scanRipple.stopRippleAnimation()
        })
    }

    private fun setupBluetooth() {
        viewModel.doBleOn.observe(this, Observer {
            startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), Constants.RequestKeys.REQUEST_ENABLE_BT)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}