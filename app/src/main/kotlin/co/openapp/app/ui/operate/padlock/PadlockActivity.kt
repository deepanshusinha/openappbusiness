package co.openapp.app.ui.operate.padlock

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.CheckResult
import androidx.core.content.ContextCompat
import co.openapp.app.BR
import co.openapp.app.BuildConfig
import co.openapp.app.OpenApp
import co.openapp.app.R
import co.openapp.app.databinding.ActivityPadlockBinding
import co.openapp.app.ui.base.BaseActivity
import co.openapp.app.utils.BleSnippet
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.extensions.intentHomeActivity
import co.openapp.app.utils.schedulers.AppScheduler
import co.openapp.app.viewmodel.PadlockViewModel
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rx.ReplayingShare
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.RxBleDevice
import com.polidea.rxandroidble2.RxBleDeviceServices
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.RxLifecycle
import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_padlock.*
import kotlinx.android.synthetic.main.layout_unlock.*
import org.jetbrains.annotations.NotNull
import timber.log.Timber


/**
 * Created by Deepanshu on 06-12-2017.
 * To handle operation of locks
 */

class PadlockActivity : BaseActivity<ActivityPadlockBinding, PadlockViewModel>(), LifecycleProvider<ActivityEvent> {

    private val lifecycleSubject = BehaviorSubject.create<ActivityEvent>()

    @NotNull
    @CheckResult
    override fun lifecycle(): Observable<ActivityEvent> {
        return lifecycleSubject.hide()
    }

    @NotNull
    @CheckResult
    override fun <T : Any?> bindUntilEvent(event: ActivityEvent): LifecycleTransformer<T> {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, event)
    }

    @NotNull
    @CheckResult
    override fun <T : Any?> bindToLifecycle(): LifecycleTransformer<T> {
        return RxLifecycleAndroid.bindActivity(lifecycleSubject)
    }

    override val layoutRes: Int
        get() = R.layout.activity_padlock

    override fun viewModel(): Class<PadlockViewModel> = PadlockViewModel::class.java

    override fun bindingVariable(): Int = BR.padlock

    private lateinit var connectionObservable: Observable<RxBleConnection>

    private lateinit var bleDevice: RxBleDevice

    private var initial = true

    private var writing: Boolean = false

    private val disconnectTriggerSubject = PublishSubject.create<Boolean>()

    private val isConnected: Boolean
        get() = bleDevice.connectionState == RxBleConnection.RxBleConnectionState.CONNECTED

    private var pull = false

    private var connect: Int = 0

    private var initialNavigation: Boolean = false

    private var operation: Boolean = false

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleSubject.onNext(ActivityEvent.CREATE)

        val macAddress = intent.getStringExtra(Constants.IntentKeys.MAC_ADDRESS)
        val deviceName = intent.getStringExtra(Constants.IntentKeys.DEVICE_NAME)
        Timber.e(macAddress)

        padlockSSID.text = deviceName
        operateSSIDPanel.text = deviceName

        padlockDragView.setOnClickListener {
            val panelState = padlockSlidingLayout.panelState
            val collapsed = SlidingUpPanelLayout.PanelState.COLLAPSED
            val expanded = SlidingUpPanelLayout.PanelState.EXPANDED

            if (panelState == collapsed) {
                padlockSlidingLayout.panelState = expanded
            } else if (panelState == expanded) {
                padlockSlidingLayout.panelState = collapsed
            }

            Timber.e(panelState.name)
        }

        padlockSlidingLayout.addPanelSlideListener(object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View?, slideOffset: Float) {
            }

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {
                Timber.e("state" + previousState + "panel" + newState)
                if (previousState == SlidingUpPanelLayout.PanelState.DRAGGING && newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    Timber.e("send unlock key")

                    if (!initial)
                        operation = true

                    writeLockStatus(BuildConfig.UNLOCK_KEY.toByteArray(), newState)
                } else if (previousState == SlidingUpPanelLayout.PanelState.DRAGGING && newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    Timber.e("send lock key")

                    if (!initial)
                        operation = true

                    writeLockStatus(BuildConfig.LOCK_KEY.toByteArray(), newState)
                }
            }
        })
        setSlidingEnabled(false)

        bleDevice = OpenApp.rxBleClient.getBleDevice(macAddress)
        connectionObservable = prepareConnectionObservable()
    }

    override fun onStart() {
        super.onStart()
        lifecycleSubject.onNext(ActivityEvent.START)
    }

    override fun onResume() {
        super.onResume()
        lifecycleSubject.onNext(ActivityEvent.RESUME)

        connectToLock()
    }

    override fun onPause() {
        lifecycleSubject.onNext(ActivityEvent.PAUSE)
        super.onPause()
    }

    override fun onStop() {
        lifecycleSubject.onNext(ActivityEvent.STOP)
        super.onStop()
    }

    override fun onDestroy() {
        Timber.e("onDestroyConnectionStatusBefore$isConnected")
        triggerDisconnect()
        Timber.e("onDestroyConnectionStatusAfter$isConnected")

        lifecycleSubject.onNext(ActivityEvent.DESTROY)
        super.onDestroy()
    }

    override fun onBackPressed() {
        Timber.e("onBackPressedConnectionStatusBefore$isConnected")
        triggerDisconnect()
        Timber.e("onBackPressedConnectionStatusAfter$isConnected")

        startActivity(intentHomeActivity())
        overridePendingTransition(0, 0)
    }

    private fun prepareConnectionObservable(): Observable<RxBleConnection> {
        return bleDevice
                .establishConnection(false)
                .takeUntil(disconnectTriggerSubject)
                .compose(bindUntilEvent(ActivityEvent.PAUSE))
                .compose(ReplayingShare.instance())
    }

    private fun connectToLock() {
        Timber.e("connectToLock")
        if (isConnected)
            triggerDisconnect()

        connectionObservable
                .flatMapSingle(RxBleConnection::discoverServices)
                .flatMapSingle { t: RxBleDeviceServices? -> t?.getCharacteristic(BleSnippet.UUID_SENSOR_READ_SERVICE) }
                .observeOn(AppScheduler.mainThread())
                .doOnSubscribe {
                    showSnack(padlockSlidingLayout, "Connecting...", Snackbar.LENGTH_INDEFINITE)
                }
                .subscribe({
                    connect = 0
                    initialNavigation = true
                    viewModel.setConnectTime()
                    notifyStatus()
                }, ({ throwable ->
                    disableScreen()
                    connect++
                    Timber.e(throwable.localizedMessage)

                    if (connect > 2)
                        onBackPressed()
                    else
                        connectToLock()
                }), ({
                    Timber.e("connectToLockFinishes")
                }))
    }

    private fun triggerDisconnect() {
        if (isConnected) {
            disconnectTriggerSubject.onNext(true)
        }

        viewModel.setDisconnectTime()
    }

    private fun writeLockStatus(value: ByteArray, newState: SlidingUpPanelLayout.PanelState) {
        Timber.e("writeLockStatus")
        if (isConnected)
            connectionObservable
                    .firstOrError()
                    .flatMap { rxBleConnection ->
                        rxBleConnection.writeCharacteristic(BleSnippet.UUID_RX_CHARACTERISTIC, value)
                    }
                    .observeOn(AppScheduler.mainThread())
                    .doOnSubscribe { writing = true }
                    .doAfterSuccess { writing = false }
                    .subscribe({
                        Timber.e("writeLockStatusConnected")

                        if (newState == SlidingUpPanelLayout.PanelState.EXPANDED)
                            pullToShackle()
                        else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED)
                            lockedShackleIn()
                    }, ({ throwable ->
                        Timber.e(throwable.localizedMessage)
//                        showSnack(padlockConstraint, "write" + throwable.localizedMessage, Snackbar.LENGTH_SHORT)
//                        writeLockStatus(value, newState)
                    }))
    }

    private fun notifyStatus() {
        if (isConnected)
            connectionObservable
                    .flatMap { rxBleConnection -> rxBleConnection.setupNotification(BleSnippet.UUID_SENSOR_READ_SERVICE) }
                    .doOnNext { setUpNotification() }
                    .doAfterNext { runOnUiThread { enableScreen() } }
                    .flatMap { notificationObservable -> notificationObservable }
                    .observeOn(AppScheduler.mainThread())
                    .subscribe({ bytes ->
                        navigateToViews(String(bytes))
                    }, ({ throwable ->
                        Timber.e(throwable.localizedMessage)
//                        showSnack(padlockConstraint, "Notification Failure!" + throwable.localizedMessage, Snackbar.LENGTH_SHORT)
                    }))
    }

    private fun setUpNotification() {
        Timber.e("Notification Has been set up!")
        showSnack(padlockConstraint, "Notification has been set up!", Snackbar.LENGTH_SHORT)
    }

    private fun enableScreen() {
        Timber.e("enableScreen")
        setSlidingEnabled(true)
        showSnack(padlockConstraint, "Connected", Snackbar.LENGTH_SHORT)
        padlockDragView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
    }

    private fun disableScreen() {
        Timber.e("disableScreen")
        setSlidingEnabled(false)
        padlockDragView.setBackgroundColor(ContextCompat.getColor(this, R.color.grey))
    }

    private fun navigateToViews(value: String) {
        Timber.e("sensor value$value")
        val split = value.split(",")

        if (!writing) {
            if (split[0] == "1") {
                lockedShackleIn()
            } else {
                val substring = split[1].split(".")
                val toInt = Integer.parseInt(substring[0])
                if (toInt > -12)
                    unlockedShackleOut()
                else {
                    if (pull) {
                        pullToShackle()
                    } else {
                        unlockedShackleIn()
                    }
                }

                Timber.e("numberqwer$toInt")
            }
        }
    }

    private fun showPanelUp() {
        Timber.e("showPanelUp")

        swipeToUnlockIv.visibility = View.GONE
        swipeToUnlockTv.visibility = View.GONE
        swipeToUnlockMsgTv.visibility = View.GONE

        lockStatus.visibility = View.VISIBLE
        operateSSIDPanel.visibility = View.VISIBLE
        unlockLottie.visibility = View.VISIBLE
    }

    private fun showPanelDown() {
        Timber.e("showPanelDown")

        lockStatus.visibility = View.GONE
        operateSSIDPanel.visibility = View.GONE
        swipeToLockIv.visibility = View.GONE
        unlockLottie.visibility = View.GONE

        swipeToUnlockIv.visibility = View.VISIBLE
        swipeToUnlockTv.visibility = View.VISIBLE
        swipeToUnlockMsgTv.visibility = View.VISIBLE
    }

    private fun lockedShackleIn() {
        Timber.e("lockedShackleIn")
        if (initialNavigation) {
            padlockSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            initialNavigation = false
        } else
            setSlidingEnabled(true)

        showPanelDown()

        setLockCode("20")
    }

    private fun unlockedShackleIn() {
        Timber.e("unlockedShackleIn")

        if (initialNavigation) {
            padlockSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
            initialNavigation = false
        } else
            setSlidingEnabled(true)

        showPanelUp()
        swipeToLockIv.visibility = View.VISIBLE

        lockStatus.text = getString(R.string.unlocked_shackle_in)
        lockStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBottomError))

        setLockCode("10")
    }

    private fun unlockedShackleOut() {
        Timber.e("unlockedShackleOut")
        if (initialNavigation) {
            padlockSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
            initialNavigation = false
        } else
            setSlidingEnabled(false)

        pull = false

        showPanelUp()
        swipeToLockIv.visibility = View.GONE

        lockStatus.text = getString(R.string.unlocked_shackle_out)
        lockStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBottomSuccess))

        setLockCode("11")
    }

    private fun pullToShackle() {
        Timber.e("pullToShackle")
        if (initialNavigation) {
            padlockSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
            initialNavigation = false
        } else
            setSlidingEnabled(false)

        pull = true
        showPanelUp()
        swipeToLockIv.visibility = View.GONE

        lockStatus.text = getString(R.string.pull_shackle_up)
        lockStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorOpenAppPull))

        setLockCode("22")
    }

    private fun setSlidingEnabled(value: Boolean) {
        Timber.e("setSlidingEnabled$value")
        padlockSlidingLayout.isEnabled = value
    }

    private fun setLockCode(code: String) {
        if (initial) {
            initial = false
//            viewModel.setInitialStatus(code)
        } else {
            if (operation) {
//                viewModel.setAndAddOperations(code)
                operation = false
            }
        }
    }
}