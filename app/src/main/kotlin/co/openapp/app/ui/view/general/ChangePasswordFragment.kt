package co.openapp.app.ui.view.general

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.source.Status
import co.openapp.app.data.vo.ErrorResponse
import co.openapp.app.databinding.FragmentChangePasswordBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.widget.bottomsheet.SingleButtonDialog
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.viewmodel.AuthViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_change_password.*
import timber.log.Timber

class ChangePasswordFragment : BaseFragment<FragmentChangePasswordBinding, AuthViewModel>() {
    override val layoutRes: Int
        get() = R.layout.fragment_change_password

    override fun getViewModel(): Class<AuthViewModel> = AuthViewModel::class.java

    override fun bindingVariable(): Int = BR.changePassword


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.changePassword = viewModel

        (activity as GeneralActivity).setupUI(changePasswordConstraint)

        val token = arguments?.getString("token") ?: ""
        viewModel.auth.fields.token = token
        val change = token.isEmpty()

        otpViewChange.setOtpCompletionListener {
            if (it.length == 6) {
                viewModel.auth.fields.otp = it
            }
        }

        changePasswordButton.setOnClickListener {
            if (change) {
                viewModel.onChangePassword()
            } else {
                viewModel.onForgetPassword()
            }
        }

        viewModel.showErrorSheet.observe(this, Observer {
            showSheet(getString(R.string.error), it, getString(R.string.got_it))
        })

        if (change) {
            changePasswordOld.visibility = View.VISIBLE

            viewModel.changeResult.observe(this, Observer { response ->
                Timber.e(response.toString())

                when (response.status) {
                    Status.SUCCESS -> {
                        viewModel.loading.set(false)
                        val responseData = response.data
                        Timber.e("response - ${responseData?.status} - message - ${responseData?.message}")

                        if (responseData != null) {
                            if (responseData.status) {
                                showSheet(getString(R.string.success), responseData.message, getString(R.string.got_it)) {
                                    this@showSheet.dismiss()
                                    findNavController().popBackStack()
                                }
                            } else {
                                showSheet(getString(R.string.error), responseData.message, getString(R.string.got_it))
                            }
                        }
                        // TODO: Handle Else
                    }
                    Status.ERROR -> {
                        viewModel.loading.set(false)
                        val authResponse = Gson().fromJson(response.message, ErrorResponse::class.java)
                        showSheet(getString(R.string.error), authResponse.message, getString(R.string.got_it))
                    }
                    Status.LOADING -> {
                        viewModel.loading.set(true)
                    }
                }
            })
        } else {
            otpViewChange.visibility = View.VISIBLE

            viewModel.forgetResult.observe(this, Observer { response ->
                Timber.e(response.toString())

                when (response.status) {
                    Status.SUCCESS -> {
                        viewModel.loading.set(false)
                        val responseData = response.data
                        Timber.e("response - ${responseData?.status} - message - ${responseData?.message}")

                        if (responseData != null) {
                            if (responseData.status) {
                                showSheet(getString(R.string.success), responseData.message, getString(R.string.got_it)) {
                                    this@showSheet.dismiss()
                                    findNavController().navigate(R.id.action_changePasswordFragment_to_loginFragment)
                                }
                            } else {
                                showSheet(getString(R.string.error), responseData.message, getString(R.string.got_it))
                            }
                        }
                        // TODO: Handle Else
                    }
                    Status.ERROR -> {
                        viewModel.loading.set(false)
                        val authResponse = Gson().fromJson(response.message, ErrorResponse::class.java)
                        showSheet(getString(R.string.error), authResponse.message, getString(R.string.got_it))
                    }
                    Status.LOADING -> {
                        viewModel.loading.set(true)
                    }
                }
            })
        }
    }

    private fun showSheet(title: String, message: String, action: String) {
        (activity as GeneralActivity).showErrorSheet(title, message, action)
    }

    private fun showSheet(title: String, message: String, actionText: String, action: SingleButtonDialog.() -> Unit) {
        (activity as GeneralActivity).showSuccessSheet(title, message, actionText, action)
    }
}