package co.openapp.app.ui.view.general

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.vo.ErrorResponse
import co.openapp.app.data.source.Status
import co.openapp.app.databinding.FragmentForgotPasswordBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.widget.bottomsheet.SingleButtonDialog
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.viewmodel.AuthViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import timber.log.Timber

class ForgotPasswordFragment : BaseFragment<FragmentForgotPasswordBinding, AuthViewModel>() {
    override val layoutRes: Int
        get() = R.layout.fragment_forgot_password

    override fun getViewModel(): Class<AuthViewModel> = AuthViewModel::class.java

    override fun bindingVariable(): Int = BR.forgot

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dataBinding.forgot = viewModel

        (activity as GeneralActivity).setupUI(forgotConstraint)

        viewModel.showErrorSheet.observe(this, Observer {
            showSheet(getString(R.string.error), it, getString(R.string.got_it))
        })

        viewModel.forgotResult.observe(this, Observer { response ->
            Timber.e(response.toString())

            when (response.status) {
                Status.SUCCESS -> {
                    viewModel.loading.set(false)
                    val responseData = response.data
                    Timber.e("response - ${responseData?.status} - message - ${responseData?.message}")

                    if (responseData != null) {
                        if (responseData.status) {
                            showSheet(getString(R.string.success), responseData.message, getString(R.string.got_it)) {
                                this@showSheet.dismiss()
                                val bundle = bundleOf("token" to responseData.data.token)
                                findNavController().navigate(R.id.action_forgotPasswordFragment_to_changePasswordFragment, bundle)
                            }
                        } else {
                            showSheet(getString(R.string.error), responseData.message, getString(R.string.got_it))
                        }
                    }
                    // TODO: Handle Else
                }
                Status.ERROR -> {
                    viewModel.loading.set(false)
                    val authResponse = Gson().fromJson(response.message, ErrorResponse::class.java)
                    showSheet(getString(R.string.error), authResponse.message, getString(R.string.got_it))
                }
                Status.LOADING -> {
                    viewModel.loading.set(true)
                }
            }
        })
    }

    private fun showSheet(title: String, message: String, action: String) {
        (activity as GeneralActivity).showErrorSheet(title, message, action)
    }

    private fun showSheet(title: String, message: String, actionText: String, action: SingleButtonDialog.() -> Unit) {
        (activity as GeneralActivity).showSuccessSheet(title, message, actionText, action)
    }
}
