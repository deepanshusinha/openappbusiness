package co.openapp.app.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import co.openapp.app.R
import co.openapp.app.data.model.lock.LockData
import co.openapp.app.databinding.InflaterLockListItemBinding
import co.openapp.app.viewmodel.LockListViewModel

class LockAdapter(private val mOnItemClickListener: OnItemClickListener) : ListAdapter<LockData, LockAdapter.ViewHolder>(LockDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.inflater_lock_list_item, parent, false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { lockList ->
            with(holder) {
                itemView.tag = lockList
                bind(createOnClickListener(lockList), lockList)
            }
        }
    }

    private fun createOnClickListener(lockItem: LockData): View.OnClickListener {
        return View.OnClickListener {
            mOnItemClickListener.onItemClick(lockItem)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(lockItem: LockData)
    }

    class ViewHolder(
            private val binding: InflaterLockListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, lockItem: LockData) {
            with(binding) {
                clickListener = listener
                lockListViewModel = LockListViewModel(lockItem)
                executePendingBindings()
            }
        }
    }
}

private class LockDiffCallback : DiffUtil.ItemCallback<LockData>() {

    override fun areItemsTheSame(
            oldItem: LockData,
            newItem: LockData
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
            oldItem: LockData,
            newItem: LockData
    ): Boolean {
        return oldItem == newItem
    }
}