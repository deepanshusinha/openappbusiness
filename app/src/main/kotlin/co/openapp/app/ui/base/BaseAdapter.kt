package co.openapp.app.ui.base

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.polidea.rxandroidble2.scan.ScanResult
import java.util.*


/**
 * Simple [RecyclerView.Adapter] implementation analog to [android.widget.ArrayAdapter]
 * for [android.support.v7.widget.RecyclerView]. Holds to a list of objects of type [T]
 *
 * @param <T>  item type (a immutable pojo works best)
 * @param <VH> [RecyclerView.ViewHolder] for item [T]
 * @author Pascal Welsch on 04.07.14.
 * Major update 09.05.17.
</VH></T> */
abstract class BaseAdapter<T, VH : RecyclerView.ViewHolder>/*(objects: List<T>)*/ : RecyclerView.Adapter<VH>() {

    abstract fun setData(dataList: ScanResult)

    /**
     * Lock used to modify the content of [.mObjects]. Any write operation
     * performed on the array should be synchronized on this lock.
     */
    private val mLock = Any()

    private val mObjects = ArrayList<T>()

    /**
     * Returns the items in the adapter as a unmodifiable list. Use the mutate functions to change
     * the items of this adapter ([.add], [.remove]) or replace the list
     * entirely ({#link [.swap]})
     *
     * @return the current items in this adapter
     */
    val items: List<T>
        get() = Collections.unmodifiableList(ArrayList(mObjects))

    /**
     * Adds the specified object at the end of the array.
     *
     * @param object The object to add at the end of the array.
     */
    fun add(`object`: T) {
        requireNotNullItem(`object`)
        synchronized(mLock) {
            val position = itemCount
            mObjects.add(`object`)
            notifyItemInserted(position)
        }
    }

    /**
     * Adds the specified list of objects at the end of the array.
     *
     * @param collection The objects to add at the end of the array.
     */
    fun addAll(collection: Collection<T>) {
        val length = collection.size
        if (length == 0) {
            return
        }
        synchronized(mLock) {
            val position = itemCount
            for (item in collection) {
                requireNotNullItem(item)
                mObjects.add(item)
            }
            notifyItemRangeInserted(position, length)
        }
    }

    /**
     * Adds the specified items at the end of the array.
     *
     * @param items The items to add at the end of the array.
     */
    @SafeVarargs
    fun addAll(vararg items: T) {
        val length = items.size
        if (length == 0) {
            return
        }
        synchronized(mLock) {
            val position = itemCount
            for (item in items) {
                requireNotNullItem(item)
                mObjects.add(item)
            }
            notifyItemRangeInserted(position, length)
        }
    }

    /**
     * Remove all elements from the list.
     */
    fun clear() {
        if (mObjects.isEmpty()) {
            return
        }
        synchronized(mLock) {
            val size = itemCount
            mObjects.clear()
            notifyItemRangeRemoved(0, size)
        }
    }

    /**
     * Returns the item at the specified position.
     *
     * @param position index of the item to return
     * @return the item at the specified position or `null` when not found
     */
    fun getItem(position: Int): T? {
        return if (position < 0 || position >= mObjects.size) {
            null
        } else mObjects[position]
    }

    override fun getItemCount(): Int {
        return mObjects.size
    }

    /**
     * Return a stable id for an item. The item doesn't have to be part of the underlying data set.
     *
     * If you don't have an id field simply return the `item` itself
     *
     * @param item for which a stable id should be generated
     * @return a identifier for the given item
     */
    abstract fun getItemId(item: T): Any?

    /**
     * Returns the position of the specified item in the array.
     *
     * @param item The item to retrieve the position of.
     * @return The position of the specified item or -1 if there is no such item.
     */
    fun getPosition(item: T): Int {
        return mObjects.indexOf(item)
    }

    /**
     * Inserts the specified object at the specified index in the array.
     *
     * @param object The object to insert into the array.
     * @param index  The index at which the object must be inserted.
     */
    fun insert(`object`: T, index: Int) {
        requireNotNullItem(`object`)
        synchronized(mLock) {
            mObjects.add(index, `object`)
            notifyItemInserted(index)
        }
    }

    /**
     * Called by the DiffUtil when it wants to check whether two items have the same data.
     * DiffUtil uses this information to detect if the contents of an item has changed.
     *
     *
     * DiffUtil uses this method to check equality instead of [Object.equals]
     * so that you can change its behavior depending on your UI.
     * For example, if you are using DiffUtil with a
     * [RecyclerView.Adapter][android.support.v7.widget.RecyclerView.Adapter], you should
     * return whether the items' visual representations are the same.
     *
     *
     * This method is called only if [.isItemTheSame] returns
     * `true` for these items.
     *
     * @param oldItem The position of the item in the old list
     * @param newItem The position of the item in the new list which replaces the
     * oldItem
     * @return True if the contents of the items are the same or false if they are different.
     */
    fun isContentTheSame(oldItem: T?, newItem: T?): Boolean {
        return oldItem === newItem || oldItem != null && oldItem == newItem
    }

    /**
     * Called by the DiffUtil to decide whether two object represent the same Item.
     *
     *
     * For example, if your items have unique ids, this method should check their id equality.
     *
     * @param oldItem The position of the item in the old list
     * @param newItem The position of the item in the new list
     * @return True if the two items represent the same object or false if they are different.
     * @see .getItemId
     */
    fun isItemTheSame(oldItem: T?, newItem: T?): Boolean {

        if (oldItem == null && newItem == null) {
            return true
        }
        if (oldItem == null || newItem == null) {
            return false
        }

        val oldId = getItemId(oldItem)
        val newId = getItemId(newItem)

        return oldId === newId || oldId != null && oldId == newId
    }

    /**
     * Removes the specified object from the array.
     *
     * @param object The object to remove.
     */
    fun remove(`object`: T) {
        synchronized(mLock) {
            val position = getPosition(`object`)
            val removed = mObjects.remove(`object`)
            if (removed) {
                notifyItemRemoved(position)
            }
        }

    }

    /**
     * replaces the old with the new item. The new item will not be added when the old one is not
     * found.
     *
     * @param oldObject will be removed
     * @param newObject is added only when hte old item is removed
     */
    fun replaceItem(oldObject: T, newObject: T) {
        requireNotNullItem(oldObject)
        requireNotNullItem(newObject)

        synchronized(mLock) {
            val position = getPosition(oldObject)
            if (position == -1) {
                // not found, don't replace
                return
            }

            mObjects.removeAt(position)
            mObjects.add(position, newObject)

            if (isItemTheSame(oldObject, newObject)) {
                if (isContentTheSame(oldObject, newObject)) {
                    // visible content hasn't changed, don't notify
                    return
                }

                // item with same stable id has changed
                notifyItemChanged(position, newObject)
            } else {
                // item replaced with another one with a different id
                notifyItemRemoved(position)
                notifyItemInserted(position)
            }
        }
    }

    /**
     * Sorts the content of this adapter using the specified comparator.
     *
     * @param comparator The comparator used to sort the objects contained in this adapter.
     */
    fun sort(comparator: Comparator<in T>) {
        val copy = ArrayList(mObjects)
        Collections.sort(copy, comparator)
        swap(copy)
    }

    /**
     * Swaps the data, removes all existing data and replaces them with a new set of data.
     * [ ] will coordinate to update notifications. Make sure [.getItemId] is
     * implemented correctly.
     *
     * @param newObjects new set of data
     * @see .isContentTheSame
     * @see .isItemTheSame
     */
    fun swap(newObjects: List<T>?) {
        if (newObjects == null) {
            clear()
        } else {
            synchronized(mLock) {
                val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                    override fun areContentsTheSame(oldItemPosition: Int,
                                                    newItemPosition: Int): Boolean {
                        val oldItem = mObjects[oldItemPosition]
                        val newItem = newObjects[newItemPosition]
                        return isContentTheSame(oldItem, newItem)
                    }

                    override fun areItemsTheSame(oldItemPosition: Int,
                                                 newItemPosition: Int): Boolean {
                        val oldItem = mObjects[oldItemPosition]
                        val newItem = newObjects[newItemPosition]
                        return isItemTheSame(oldItem, newItem)
                    }

                    override fun getNewListSize(): Int {
                        return newObjects.size
                    }

                    override fun getOldListSize(): Int {
                        return mObjects.size
                    }
                })
                mObjects.clear()
                for (item in newObjects) {
                    requireNotNullItem(item)
                    mObjects.add(item)
                }
                result.dispatchUpdatesTo(this)
            }
        }
    }

    /**
     * Move the item from old position to new position
     * @param fromPosition old position
     * @param toPosition new position
     */
    fun onItemMove(fromPosition: Int, toPosition: Int) {
        synchronized(mLock) {
            if (fromPosition < toPosition) {
                for (i in fromPosition until toPosition) {
                    Collections.swap(mObjects, i, i + 1)
                }
            } else {
                for (i in fromPosition downTo toPosition + 1) {
                    Collections.swap(mObjects, i, i - 1)
                }
            }
            notifyItemMoved(fromPosition, toPosition)
        }
    }

    private fun requireNotNullItem(o: Any?) {
        if (o == null) {
            throw IllegalStateException("null items are not allowed")
        }
    }

    /*init {
        if (objects == null) {
            throw IllegalStateException("null is not supported. Use an empty list.")
        }
        for (item in objects) {
            requireNotNullItem(item)
            mObjects.add(item)
        }
    }*/
}
