package co.openapp.app.ui.operate.door

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.db.Key
import co.openapp.app.data.model.lock.LockData
import co.openapp.app.data.model.request.DoorPwdUpdate
import co.openapp.app.databinding.ActivityDoorBinding
import co.openapp.app.ui.base.BaseActivity
import co.openapp.app.utils.ble.OnTTLockDeviceListener
import co.openapp.app.utils.constant.Constants
import co.openapp.app.viewmodel.DoorViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.ttlock.bl.sdk.entity.Error
import kotlinx.android.synthetic.main.activity_door.*
import kotlinx.android.synthetic.main.layout_unlock.*
import timber.log.Timber


/**
 * Created by Deepanshu on 06-12-2017.
 * To handle operation of locks
 */

class DoorActivity : BaseActivity<ActivityDoorBinding, DoorViewModel>(), OnTTLockDeviceListener {

    override val layoutRes: Int
        get() = R.layout.activity_door

    override fun viewModel(): Class<DoorViewModel> = DoorViewModel::class.java

    override fun bindingVariable(): Int = BR.door

    private var count = 0
    private var isAdmin: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val lockItemString = intent?.getStringExtra(Constants.LockType.DOOR_LOCK)
        val gson = Gson()
        val lockItem = gson.fromJson<LockData>(lockItemString, object : TypeToken<LockData>() {}.type)
        val deviceName = lockItem.lockName
        val parameters = lockItem.parameters
        // TODO: Change uid here
        val doorKey = Key(
                parameters.uid!!,
                lockItem.macAddress,
                parameters.lockKey!!,
                parameters.adminPwd!!,
                parameters.aesKey,
                parameters.pwdInfo!!,
                parameters.lockFlagPos!!,
                deviceName,
                Gson().toJson(parameters.lockVersion),
                parameters.timezoneRawOffset!!
        )
        doorSSID.text = deviceName
        operateSSIDPanel.text = deviceName

        doorDragView.setOnClickListener {
            val panelState = doorSlidingLayout.panelState
            val collapsed = SlidingUpPanelLayout.PanelState.COLLAPSED
            val expanded = SlidingUpPanelLayout.PanelState.EXPANDED

            if (panelState == collapsed) {
                doorSlidingLayout.panelState = expanded
            } else if (panelState == expanded) {
                doorSlidingLayout.panelState = collapsed
            }

            Timber.e(panelState.name)
        }

        doorSlidingLayout.addPanelSlideListener(object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View?, slideOffset: Float) {
            }

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {
                if (previousState == SlidingUpPanelLayout.PanelState.DRAGGING && newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    Timber.e("send unlock key")
                    unlocked()
                    viewModel.doUnlock()
                    count++
                }
            }
        })

        viewModel.initTTLock(this, doorKey)

        otpFabMenu.bindAnchorView(otpFab)

        /*otpTv.setListener {
            if (it.length > 3) {
                if (otpFabMenu.isShowing) {
                    otpFabMenu.closeMenu()

                    doorDragView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))

                    // set period keyboard password
//                    viewModel.addPeriodKeyboardPassword(it)
                    if (viewModel.hasConnection())
                        viewModel.doSetAdminKeyboardPassword(it)
                    else
                        viewModel.showErrorSheet.value = getString(R.string.no_internet_connection)

                    otpTv.simulateDeletePress()
                    otpTv.simulateDeletePress()
                    otpTv.simulateDeletePress()
                    otpTv.simulateDeletePress()
                }
            }
        }*/
    }

    override fun onPause() {
        viewModel.setAttemptCount(count)
        super.onPause()
    }

    override fun onDestroy() {
        viewModel.stopBleService()

        // Stop Sync Engaged
        viewModel.setSyncEngaged(false)

        super.onDestroy()
    }

    private fun showPanelUp() {
        Timber.e("showPanelUp")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorBottomSuccess)
        }

        swipeToUnlockIv.visibility = View.GONE
        swipeToUnlockTv.visibility = View.GONE
        swipeToUnlockMsgTv.visibility = View.GONE
        otpFab.hide()

        lockStatus.visibility = View.VISIBLE
        operateSSIDPanel.visibility = View.VISIBLE
        unlockLottie.visibility = View.VISIBLE
    }

    private fun showPanelDown() {
        Timber.e("showPanelDown")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorBottomError)
        }

        lockStatus.visibility = View.GONE
        operateSSIDPanel.visibility = View.GONE
        swipeToLockIv.visibility = View.GONE
        unlockLottie.visibility = View.GONE

        swipeToUnlockIv.visibility = View.VISIBLE
        swipeToUnlockTv.visibility = View.VISIBLE
        swipeToUnlockMsgTv.visibility = View.VISIBLE
        if (isAdmin)
            otpFab.show()
    }

    private fun locked() {
        Timber.e("locked")
        setSlidingEnabled(true)
        doorSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED

        showPanelDown()
    }

    private fun unlocked() {
        Timber.e("unlocked")
        setSlidingEnabled(false)

        showPanelUp()
        swipeToLockIv.visibility = View.GONE

        lockStatus.text = getString(R.string.unlocked_shackle_out)
        lockStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBottomSuccess))
    }

    private fun setSlidingEnabled(value: Boolean) {
        doorSlidingLayout.isEnabled = value
    }

    override fun onUnlock(lockTime: Long, error: Error?) {
        if (error == Error.SUCCESS) {
            showSnack(doorConstraint, "Unlocked Successfully", Snackbar.LENGTH_SHORT)
        } else {
            showSnack(doorConstraint, "Try Again!", Snackbar.LENGTH_SHORT)
        }

        runOnUiThread {
            locked()
            viewModel.doOperateLog()
        }
    }

    override fun onGetOperateLog(records: String?, error: Error?) {
        runOnUiThread {
            Timber.e(records)
            Timber.e(error?.errorMsg)
            if (error == Error.SUCCESS)
                viewModel.setDoorLock(records)
            else
                viewModel.setDoorLockError(error?.errorMsg)
        }
    }

    override fun onDoorResponse(message: Int) {
        runOnUiThread {
            viewModel.loading.set(false)
            viewModel.showErrorSheet.value = getString(message)
        }
    }

    override fun onSetAdminKeyboardPassword(doorPwdUpdate: DoorPwdUpdate) {
        viewModel.updateAdminKeyboardPwd(doorPwdUpdate)
    }
}
