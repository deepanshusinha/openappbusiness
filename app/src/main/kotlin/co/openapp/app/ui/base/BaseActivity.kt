package co.openapp.app.ui.base

//import com.google.firebase.analytics.FirebaseAnalytics
import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import co.openapp.app.R
import co.openapp.app.utils.constant.DialogBoxInput
import co.openapp.app.widget.bottomsheet.SingleButtonDialog
import co.openapp.app.widget.MaterialDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.dialog_item.*
import javax.inject.Inject


/**
 * Created by deepanshu on 16/11/17.
 * Abstract activity that every other BaseActivity in this application must implement. It provides the
 * following functionality:
 * - Handles creation of Dagger components and makes sure that instances of
 * ConfigPersistentComponent are kept across configuration changes.
 * - Set up and handles a GoogleApiClient instance that can be used to access the Google sign in
 * api.
 * - Handles signing out when an authentication error_network event is received.
 */

abstract class BaseActivity<DB : ViewDataBinding, VM : ViewModel> : AppCompatActivity(), HasSupportFragmentInjector {

    private lateinit var viewDataBinding: DB

    lateinit var viewModel: VM

    private var dialog: MaterialDialog? = null

    private var bottomDialog: SingleButtonDialog? = null

    private lateinit var toast: Toast

    private var close = false

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private lateinit var mFirebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performBinding()

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }

    override fun onPause() {
        hideDialog()
        super.onPause()
    }

    private fun performBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutRes)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModel())
        viewDataBinding.setVariable(bindingVariable(), viewModel)
        viewDataBinding.executePendingBindings()
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String): Boolean =
            Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED

    fun showDialog(title: Int, messageResId: Int, buttonResId: Int, inputType: DialogBoxInput = DialogBoxInput.TEXT_VIEW, action: (MaterialDialog.() -> Unit)? = null) {
        showDialog(getString(title), getString(messageResId), getString(buttonResId), inputType, action)
    }

    fun showDialog(title: String, message: String, buttonText: String, inputType: DialogBoxInput = DialogBoxInput.TEXT_VIEW, action: (MaterialDialog.() -> Unit)? = null) {
        hideDialog()
        dialog = MaterialDialog(this).apply {
            title(title)
            message(message)
            when(inputType) {
                DialogBoxInput.TEXT_VIEW -> {
                    messageEt.visibility = View.GONE
                    otpView.visibility = View.GONE
                }
                DialogBoxInput.EDIT_TEXT -> {
                    otpView.visibility = View.GONE

                    messageEt.visibility = View.VISIBLE
                }
                DialogBoxInput.OTP_VIEW -> {
                    messageEt.visibility = View.GONE

                    otpView.visibility = View.VISIBLE
                }
            }
            onClickButton(buttonText, action)
            show()
        }
    }

    fun hideDialog() {
        if (isDialog()) {
            dialog?.hide()
        }
    }

    private fun isDialog(): Boolean {
        return (dialog != null && dialog!!.isShowing)
    }

    fun getDialog(): MaterialDialog? {
        return dialog
    }

    fun showSnack(parent: ViewGroup, messageResId: Int, length: Int,
                  actionLabelResId: Int? = null, action: ((View) -> Unit)? = null,
                  callback: ((Snackbar) -> Unit)? = null) {

        showSnack(parent, getString(messageResId), length, actionLabelResId?.let { getString(it) }, action, callback)
    }

    fun showSnack(parent: ViewGroup, message: String, length: Int,
                  actionLabel: String? = null, action: ((View) -> Unit)? = null,
                  callback: ((Snackbar) -> Unit)? = null) {

        Snackbar.make(parent, message, length)
                .apply {
                    if (actionLabel != null) {
                        setAction(actionLabel, action)
                    }

                    callback?.invoke(this)
                }
                .show()
    }

    /**
     * @titleResId - title res id
     * @messageResId - message res id
     * @buttonTextResId - button text res id
     */
    fun showSuccessSheet(titleResId: Int, messageResId: Int, buttonTextResId: Int) {
        showSuccessSheet(getString(titleResId), getString(messageResId), getString(buttonTextResId))
    }

    /**
     * @titleResId - title res id
     * @messageResId - message res id
     * @buttonTextResId - button text res id
     * @action: button action listener
     */
    fun showSuccessSheet(titleResId: Int, messageResId: Int, buttonTextResId: Int, action: SingleButtonDialog.() -> Unit) {
        showSuccessSheet(getString(titleResId), getString(messageResId), getString(buttonTextResId), action)
    }

    /**
     * @title - title
     * @message - message
     * @buttonText - button text
     */
    fun showSuccessSheet(title: String, message: String, buttonText: String) {
        showSuccessSheet(title, message, buttonText) { bottomDialog?.dismiss() }
    }

    /**
     * @title - title
     * @message - message
     * @buttonText - button text
     * @action: button action listener
     */
    fun showSuccessSheet(title: String, message: String, buttonText: String, action: SingleButtonDialog.() -> Unit) {
        showSuccessSheet(title, message, buttonText, action, R.raw.success,
                R.color.colorBottomSuccess,
                R.drawable.shape_rounded_border_white_bg_green)
    }

    /**
     * @title - title
     * @message - message
     * @buttonText - button text
     * @action - button action listener
     * @rawIds - anim res id
     * @colorResId - color res id
     */
    private fun showSuccessSheet(title: String, message: String, buttonText: String,
                                 action: SingleButtonDialog.() -> Unit, rawIds: Int,
                                 colorResId: Int, drawableResId: Int) {
        showBottomDialog(title, message, buttonText, action, rawIds, colorResId, drawableResId)
    }

    /**
     * @titleResId - title res id
     * @messageResId - message res id
     * @buttonTextResId - button text res id
     */
    fun showErrorSheet(titleResId: Int, messageResId: Int, buttonTextResId: Int) {
        showErrorSheet(getString(titleResId), getString(messageResId), getString(buttonTextResId))
    }

    /**
     * @titleResId - title res id
     * @messageResId - message res id
     * @buttonTextResId - button text res id
     * @action: button action listener
     */
    fun showErrorSheet(titleResId: Int, messageResId: Int, buttonTextResId: Int, action: SingleButtonDialog.() -> Unit) {
        showErrorSheet(getString(titleResId), getString(messageResId), getString(buttonTextResId), action)
    }

    /**
     * @title - title
     * @message - message
     * @buttonText - button text
     */
    fun showErrorSheet(title: String, message: String, buttonText: String) {
        showErrorSheet(title, message, buttonText) { bottomDialog?.dismiss() }
    }

    /**
     * @title - title
     * @message - message
     * @buttonText - button text
     * @action: button action listener
     */
    fun showErrorSheet(title: String, message: String, buttonText: String, action: SingleButtonDialog.() -> Unit) {
        showErrorSheet(title, message, buttonText, action, R.raw.error,
                R.color.colorBottomError,
                R.color.colorBottomError)
    }

    /**
     * @title - title
     * @message - message
     * @buttonText - button text
     * @action - button action listener
     * @rawIds - anim res id
     * @colorResId - color res id
     */
    private fun showErrorSheet(title: String, message: String, buttonText: String,
                               action: SingleButtonDialog.() -> Unit, rawIds: Int,
                               colorResId: Int, drawableResId: Int) {
        showBottomDialog(title, message, buttonText, action, rawIds, colorResId, drawableResId)
    }

    private fun showBottomDialog(title: String, message: String, buttonText: String,
                                 action: SingleButtonDialog.() -> Unit, rawIds: Int,
                                 colorResId: Int, drawableResId: Int) {


        bottomDialog?.dismiss()
        bottomDialog = SingleButtonDialog(this).apply {
            setTitle(title)
            setMessage(message)
            setButtonTitle(buttonText)
            onClickButton(action)
            setAnimation(rawIds)
            setBackGroundColor(colorResId, drawableResId)
            show()
        }
    }

    fun toast(message: String, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, message, duration).show()
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun setupUI(view: View) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { _, _ ->
                hideKeyboard()
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            (0 until view.childCount)
                    .asSequence()
                    .map { view.getChildAt(it) }
                    .forEach { setupUI(it) }
        }
    }

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutRes: Int

    abstract fun viewModel(): Class<VM>

    abstract fun bindingVariable(): Int

    /**
     * @return menu resource id
     */
    @get:MenuRes
    open val menuRes: Int = 0
}