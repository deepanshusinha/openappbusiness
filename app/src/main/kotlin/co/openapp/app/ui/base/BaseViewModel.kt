package co.openapp.app.ui.base

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import co.openapp.app.utils.SingleLiveEvent
import co.openapp.app.utils.Utils
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by deepanshu on 11/01/18.
 */

open class BaseViewModel : ViewModel() {

    @Inject
    lateinit var utils: Utils

    val loading = ObservableField<Boolean>()

    val showSuccessSheet = SingleLiveEvent<String>()
    val showErrorSheet = SingleLiveEvent<String>()
    val showErrorToast = SingleLiveEvent<String>()

    var mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        mCompositeDisposable.dispose()
        super.onCleared()
    }

    fun getString(stringResId: Int): String {
        return utils.getMessage(stringResId)
    }

    fun hasConnection(): Boolean {
        return utils.hasConnection()
    }
}