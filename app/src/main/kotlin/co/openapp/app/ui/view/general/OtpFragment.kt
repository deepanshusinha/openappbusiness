package co.openapp.app.ui.view.general

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.model.request.otp.OtpFields
import co.openapp.app.data.source.Status
import co.openapp.app.data.vo.ErrorResponse
import co.openapp.app.databinding.FragmentOtpBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.widget.bottomsheet.SingleButtonDialog
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.utils.constant.Constants
import co.openapp.app.viewmodel.AuthViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_otp.*
import timber.log.Timber

/**
 * Created by deepanshusinha on 20/03/18.
 */

class OtpFragment : BaseFragment<FragmentOtpBinding, AuthViewModel>() {
    override val layoutRes: Int
        get() = R.layout.fragment_otp

    override fun getViewModel(): Class<AuthViewModel> = AuthViewModel::class.java

    override fun bindingVariable(): Int = BR.otp_verify

//    val args: Regis by navArgs<String>()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dataBinding.otpVerify = viewModel

        (activity as GeneralActivity).setupUI(otpConstraint)

        val phoneNumber = arguments?.getString(Constants.IntentKeys.PHONE_NUMBER)
        otpPhoneNumber.text = phoneNumber

        val split = phoneNumber?.split(" ")
        val countryCode = split?.get(0)?.replace("+", "")
        val phone = split?.get(1)

        var reSendOtp = false

        reSend.setOnClickListener {
            reSendOtp = true
            val otpFields = OtpFields()
            otpFields.phone = phone!!
            otpFields.countryCode = countryCode!!

            viewModel.doOtp(otpFields)

        }

        otpButton.setOnClickListener {
            val otpString = otpView.text.toString()

            if (otpString.length == 6) {
                val otpFields = OtpFields()
                otpFields.otp = otpString
                otpFields.countryCode = countryCode!!
                otpFields.phone = phone!!

                viewModel.doOtp(otpFields)

            } else {
                (activity as GeneralActivity).showErrorSheet(
                        getString(R.string.error),
                        "Enter 6 Digit Otp Code",
                        getString(R.string.got_it)
                )
            }
        }

        viewModel.otpResult.observe(this, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {
                    viewModel.loading.set(false)
                    val responseData = response.data

                    if (responseData != null) {
                        if (responseData.status) {
                            if (reSendOtp) {
                                showSheet(getString(R.string.success), responseData.message, getString(R.string.got_it)) {
                                    reSendOtp = false
                                    this@showSheet.dismiss()
                                }
                            } else {
                                showSheet(getString(R.string.success), responseData.message, getString(R.string.got_it)) {
                                    this@showSheet.dismiss()
                                    findNavController().navigate(R.id.action_otpFragment_to_loginFragment)
                                }
                            }
                        } else {
                            showSheet(getString(R.string.error), responseData.message, getString(R.string.got_it))
                        }
                    }
                    // TODO: Handle Else
                }
                Status.ERROR -> {
                    viewModel.loading.set(false)
                    val authResponse = Gson().fromJson(response.message, ErrorResponse::class.java)
                    showSheet(getString(R.string.error), authResponse.message, getString(R.string.got_it))
                }
                Status.LOADING -> {
                    viewModel.loading.set(true)
                    Timber.e("loading")
                }
            }
        })
    }

    private fun showSheet(title: String, message: String, action: String) {
        (activity as GeneralActivity).showErrorSheet(title, message, action)
    }

    private fun showSheet(title: String, message: String, actionText: String, action: SingleButtonDialog.() -> Unit) {
        (activity as GeneralActivity).showSuccessSheet(title, message, actionText, action)
    }
}