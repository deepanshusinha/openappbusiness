package co.openapp.app.ui.view.main

import android.content.ActivityNotFoundException
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.model.lock.LockVersion
import co.openapp.app.data.model.request.KeyRequest
import co.openapp.app.data.model.sync.DeviceLocation
import co.openapp.app.data.source.Status
import co.openapp.app.databinding.ActivityGeneralBinding
import co.openapp.app.ui.base.BaseActivity
import co.openapp.app.ui.home.DoorAdapter
import co.openapp.app.ui.view.bb.ScanLocksFragment
import co.openapp.app.utils.ble.OnFoundDeviceListener
import co.openapp.app.utils.ble.TTLockBle
import co.openapp.app.utils.constant.DialogBoxInput
import co.openapp.app.utils.extensions.intentPlayStoreActivity
import co.openapp.app.viewmodel.GeneralViewModel
import co.openapp.app.worker.LockWorker
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ttlock.bl.sdk.entity.LockData
import com.ttlock.bl.sdk.scanner.ExtendedBluetoothDevice
import kotlinx.android.synthetic.main.activity_general.*
import kotlinx.android.synthetic.main.fragment_scan_locks.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.launch
import mumayank.com.airlocationlibrary.AirLocation
import timber.log.Timber


class GeneralActivity : BaseActivity<ActivityGeneralBinding, GeneralViewModel>(),
        DoorAdapter.OnItemClickListener, OnFoundDeviceListener {

    override val layoutRes: Int
        get() = R.layout.activity_general

    override fun viewModel(): Class<GeneralViewModel> = GeneralViewModel::class.java

    override fun bindingVariable(): Int = BR.general

    private lateinit var navController: NavController
    private var airLocation: AirLocation? = null
    private val delay = 10000L //milliseconds
    private val handler = Handler()
    private lateinit var runnable: Runnable

    private var mTTLock: TTLockBle? = null
    private lateinit var mAdapter: DoorAdapter
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar.title = ""
        setSupportActionBar(toolbar)

        bottomSheetBehavior = BottomSheetBehavior.from(doorLockSheet)

        // when it is collapsed
        bottomSheetBehavior.peekHeight = 0

        // change the state of the bottom sheet
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        // set door lock onboarding adapter
        setAdapter()

        // Set up toolbar with navigation
        navController = findNavController(R.id.nav_host_fragment_general)
        setupActionBarWithNavController(navController,
                AppBarConfiguration(
                        setOf(
                                R.id.loginFragment,
                                R.id.registerFragment,
                                R.id.introFragment,
                                R.id.myLocksFragment,
                                R.id.scanLocksFragment,
                                R.id.profileFragment
                        )
                )
        )

        // Set up Bottom Navigation Menu
        setupBottomNavigationMenu(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.scanLocksFragment -> {
                    lastTime.visibility = View.GONE
                    refreshSync()
                    fab.backgroundTintList = ContextCompat.getColorStateList(this, R.color.blue_french)
                    fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_scan_lock_unfilled_darkl))
                    showNavigationView()
                }
                R.id.profileFragment -> {
                    lastTime.visibility = View.GONE
                    refreshProfile()
                    fab.backgroundTintList = ContextCompat.getColorStateList(this, R.color.purple_grey)
                    fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_scan_lock_unfilled_darkl))
                    showNavigationView()
                }
                R.id.myLocksFragment -> {
                    lastTime.visibility = View.VISIBLE
                    refreshLock()
                    fab.backgroundTintList = ContextCompat.getColorStateList(this, R.color.purple_grey)
                    fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_scan_lock_unfilled_darkl))
                    showNavigationView()
                }
                else -> {
                    lastTime.visibility = View.GONE
                    hideNavigationView()
                }
            }
        }

        fab.setOnClickListener {
            if (bottomNavigationView.selectedItemId != R.id.scanLocksFragment) {
                bottomNavigationView.selectedItemId = R.id.scanLocksFragment
            }

            val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_general)
            navHostFragment?.childFragmentManager?.fragments?.forEach { fragment ->
                if (fragment is ScanLocksFragment && fragment.isVisible) {
                    fragment.startScanning()
                }
            }
        }

        if (viewModel.isLogin) {
            LockWorker.start()

            viewModel.callGeneralApi().observe(this, Observer { response ->
                if (response != null && response.status == Status.SUCCESS) {
                    val responseData = response.data
                    if (responseData != null) {
                        val data = responseData.data
                        if (data.version > viewModel.getAppVersion()) {
                            showDialog(R.string.update_title, R.string.update_msg, R.string.update) {
                                // Navigate To Play Store
                                try {
                                    startActivity(intentPlayStoreActivity())
                                } catch (e: ActivityNotFoundException) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }
                }
            })
        }

        viewModel.liveLockListRepositories.observe(this, Observer { })
        viewModel.liveProfileDetailRepositories.observe(this, Observer { })
        viewModel.liveSyncDataListRepositories.observe(this, Observer { response ->
            if (response != null && response.status == Status.SUCCESS) {
                val responseData = response.data
                if (responseData != null && responseData.status) {
                    refreshLock()
                }
            }
        })

        viewModel.liveInitLockRepositories.observe(this, Observer { response ->
            if (response != null) {
                when (response.status) {
                    Status.SUCCESS -> {
                        viewModel.loading.set(false)
                        val data = response.data
                    }
                    Status.ERROR -> {
                        viewModel.loading.set(false)
                        response.message?.let {
                            showErrorSheet(
                                    getString(R.string.error),
                                    it,
                                    getString(R.string.got_it)
                            )
                        }
                    }
                    Status.LOADING -> viewModel.loading.set(true)
                }
            }
        })
    }

    override fun onDestroy() {
        mTTLock = null

        super.onDestroy()
    }

    private val isLogin: Boolean
        get() = viewModel.isLogin


    private fun refreshLock() {
        if (isLogin)
            viewModel.refreshLock()
    }


    private fun refreshSync() {
        if (isLogin && !viewModel.isSyncEngaged())
            viewModel.refreshSync()
    }


    private fun refreshProfile() {
        if (isLogin)
            viewModel.refreshProfile()
    }

    private fun showNavigationView() {
        appBar.visibility = View.VISIBLE
        bottomNavigationView.visibility = View.VISIBLE
        bar.visibility = View.VISIBLE
        fab.visibility = View.VISIBLE
    }

    private fun hideNavigationView() {
        appBar.visibility = View.INVISIBLE
        bottomNavigationView.visibility = View.GONE
        bar.visibility = View.GONE
        fab.visibility = View.GONE

    }

    private fun setupBottomNavigationMenu(navController: NavController) {
        // Use NavigationUI to set up Bottom Nav
        bottomNavigationView.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp() = findNavController(R.id.nav_host_fragment_general).navigateUp()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLocation?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        airLocation?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        /*when (requestCode) {
            *//*MAKE_CALL_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startCallingIntent()
                }
                return
            }*//*
            // TODO: Same as Home Activity
            *//*Constants.RequestKeys.SMS_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startMessageService()
                }
                return
            }*//*
        }*/
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle item selection
        return when (item?.itemId) {
            R.id.add_door_lock -> {
                if (mTTLock == null)
                    mTTLock = TTLockBle(this)

                val state = bottomSheetBehavior.state

                if (state == BottomSheetBehavior.STATE_EXPANDED) {
                    mTTLock?.stopBTDeviceScan()
                    mAdapter.clearScanResults()

                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                } else if (state == BottomSheetBehavior.STATE_HIDDEN || state == BottomSheetBehavior.STATE_COLLAPSED) {
                    mTTLock?.startBTDeviceScan()

                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }

                return true
            }
            R.id.log_out -> {
                GlobalScope.launch(Dispatchers.IO) { viewModel.logOut() }
                navController.navigate(R.id.introFragment)
                true
            }
            /*R.id.help_support -> {
                startCallingIntent()
                true
            }*/
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun startLocation() {
        airLocation = AirLocation(this, true, true, object : AirLocation.Callbacks {
            override fun onSuccess(location: Location) {
                // location fetched successfully, proceed with it
                Timber.e("location - ${location.latitude} - ${location.longitude}")
                viewModel.saveCurrentLocation(DeviceLocation(location.latitude, location.longitude))
            }

            override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                // couldn't fetch location due to reason available in locationFailedEnum
                // you may optionally do something to inform the user, even though the reason may be obvious
                Timber.e(locationFailedEnum.name)

                val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_general)
                navHostFragment?.childFragmentManager?.fragments?.forEach { fragment ->
                    if (fragment is ScanLocksFragment && fragment.isVisible) {
                        fragment.scanRipple.stopRippleAnimation()
                    }
                }
            }
        })
    }

    override fun onBackPressed() {
        val currentDestination = navController.currentDestination
        if (currentDestination != null) {
            when (currentDestination.id) {
                R.id.scanLocksFragment,
                R.id.introFragment -> finish()
                R.id.myLocksFragment,
                R.id.profileFragment -> navController.navigate(R.id.scanLocksFragment)
                else -> super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    /*private fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
    }*/

    /*private fun startCallingIntent() {
        if (checkPermission(Manifest.permission.CALL_PHONE))
            startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel: +918040909955")))
        else
            requestPermissionsSafely(arrayOf(Manifest.permission.CALL_PHONE),
                    Constants.RequestKeys.MAKE_CALL_PERMISSION_REQUEST_CODE)
    }*/

    override fun onResume() {
        super.onResume()

        runnable = Runnable {
            refreshSync()
            refreshLock()
            handler.postDelayed(runnable, delay)
        }

        handler.postDelayed(runnable, delay)

        forceOnline()
    }

    override fun onPause() {
        handler.removeCallbacks(runnable)

        super.onPause()
    }

    fun forceOnline() {
        if (viewModel.isLogin && viewModel.isForceOnline) {
            showDialog(R.string.online_title, R.string.online_msg, R.string.proceed) {
                // Navigate To Play Store
                this.dismiss()
                if (viewModel.hasConnection()) {
                    refreshSync()
                    refreshProfile()
                    refreshLock()
                } else {
                    forceOnline()
                }
            }

        }
    }

    fun updateLastLogin(date: String) {
        lastTime.text = date
    }

    private fun setAdapter() {
        doorLockList.layoutManager = LinearLayoutManager(this)
        doorLockList.setHasFixedSize(true)
        mAdapter = DoorAdapter(this)
        doorLockList.adapter = mAdapter
    }

    override fun onItemClick(position: Int) {
        val extendedBluetoothDevice = mAdapter.itemList[position]

        if (extendedBluetoothDevice.isSettingMode) {
            viewModel.loading.set(true)
            mTTLock?.doAddAdministrator(extendedBluetoothDevice)
        } else
            toast("Already OnBoarded")
    }

    override fun onFoundDevice(extendedBluetoothDevice: ExtendedBluetoothDevice?) {
        if (extendedBluetoothDevice != null) {
            runOnUiThread { mAdapter.setData(extendedBluetoothDevice) }
        }
    }

    override fun onGetKey(lockData: LockData) {
        runOnUiThread {
            viewModel.loading.set(false)
            showDialog(R.string.product_id, R.string.product_msg, R.string.set, DialogBoxInput.EDIT_TEXT) {
                hideDialog()
                val productId = getDialog()?.getProductId()
                if (productId != null && productId.isNotEmpty()) {
                    val lockVersionType = object : TypeToken<LockVersion>() {}.type
                    val lockVersion = Gson().fromJson<LockVersion>(lockData.getLockVersion(), lockVersionType)
                    val keyRequest = KeyRequest(
                            lockData.getLockMac(),
                            lockData.getLockKey(),
                            lockData.getAdminPwd(),
                            lockData.getAesKeyStr(),
                            lockData.getPwdInfo(),
                            lockData.getNoKeyPwd(),
                            lockData.getDeletePwd(),
                            lockData.getFirmwareRevision(),
                            lockData.getHardwareRevision(),
                            lockData.getLockFlagPos(),
                            lockData.getLockName(),
                            lockVersion,
                            lockData.getModelNum(),
                            lockData.getSpecialValue(),
                            lockData.getTimestamp(),
                            lockData.getTimezoneRawOffset()
                    )

                    keyRequest.productId = productId.toUpperCase()
                    Timber.e(Gson().toJson(keyRequest))

                    // call init api
                    viewModel.initApi(keyRequest)
                }
            }
        }
    }
}
