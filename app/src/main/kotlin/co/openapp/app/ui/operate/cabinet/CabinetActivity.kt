package co.openapp.app.ui.operate.cabinet

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.CheckResult
import androidx.core.content.ContextCompat
import co.openapp.app.BR
import co.openapp.app.BuildConfig
import co.openapp.app.OpenApp
import co.openapp.app.R
import co.openapp.app.data.model.request.Info
import co.openapp.app.data.model.request.Operations
import co.openapp.app.data.model.request.Usage
import co.openapp.app.databinding.ActivityCabinetBinding
import co.openapp.app.ui.base.BaseActivity
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.BleSnippet
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.constant.Constants.CabinetKeys
import co.openapp.app.utils.constant.Constants.IntentKeys
import co.openapp.app.utils.extensions.intentHomeActivity
import co.openapp.app.utils.schedulers.AppScheduler
import co.openapp.app.viewmodel.CabinetViewModel
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rx.ReplayingShare
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.RxBleDevice
import com.polidea.rxandroidble2.RxBleDeviceServices
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.RxLifecycle
import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_cabinet.*
import kotlinx.android.synthetic.main.inflater_otp_fab.*
import kotlinx.android.synthetic.main.layout_unlock.*
import org.jetbrains.annotations.NotNull
import timber.log.Timber
import java.util.*


/**
 * Created by Deepanshu on 06-12-2017.
 * To handle operation of locks
 */

class CabinetActivity : BaseActivity<ActivityCabinetBinding, CabinetViewModel>(), LifecycleProvider<ActivityEvent> {

    private val lifecycleSubject = BehaviorSubject.create<ActivityEvent>()

    @NotNull
    @CheckResult
    override fun lifecycle(): Observable<ActivityEvent> {
        return lifecycleSubject.hide()
    }

    @NotNull
    @CheckResult
    override fun <T : Any?> bindUntilEvent(event: ActivityEvent): LifecycleTransformer<T> {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, event)
    }

    @NotNull
    @CheckResult
    override fun <T : Any?> bindToLifecycle(): LifecycleTransformer<T> {
        return RxLifecycleAndroid.bindActivity(lifecycleSubject)
    }

    override val layoutRes: Int
        get() = R.layout.activity_cabinet

    override fun viewModel(): Class<CabinetViewModel> = CabinetViewModel::class.java

    override fun bindingVariable(): Int = BR.cabinet

    private var mOtpName: String? = null
    private var count = 0

    private lateinit var bleDevice: RxBleDevice

    private lateinit var connectionObservable: Observable<RxBleConnection>

    private val disconnectTriggerSubject = PublishSubject.create<Boolean>()

    private val isConnected: Boolean
        get() = bleDevice.connectionState == RxBleConnection.RxBleConnectionState.CONNECTED

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleSubject.onNext(ActivityEvent.CREATE)

        val macAddress = intent.getStringExtra(IntentKeys.MAC_ADDRESS)
        val deviceName = intent.getStringExtra(IntentKeys.DEVICE_NAME)
        mOtpName = intent.getStringExtra(IntentKeys.OTP_NAME)

        cabinetSSID.text = deviceName
        operateSSIDPanel.text = deviceName

        cabinetDragView.setOnClickListener {
            if (otpFab.visibility == View.INVISIBLE)
                showErrorSheet(getString(R.string.oops), getString(R.string.otp_message) + if (mOtpName.isNullOrEmpty()) " your admin" else " $mOtpName", getString(R.string.got_it))

            val panelState = cabinetSlidingLayout.panelState
            val collapsed = SlidingUpPanelLayout.PanelState.COLLAPSED
            val expanded = SlidingUpPanelLayout.PanelState.EXPANDED

            if (panelState == collapsed) {
                cabinetSlidingLayout.panelState = expanded
            } else if (panelState == expanded) {
                cabinetSlidingLayout.panelState = collapsed
            }

            Timber.e(panelState.name)
        }

        cabinetSlidingLayout.addPanelSlideListener(object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View?, slideOffset: Float) {
            }

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {
                Timber.e("state" + previousState + "panel" + newState)
                if (previousState == SlidingUpPanelLayout.PanelState.DRAGGING && newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    Timber.e("send unlock key${BuildConfig.UNLOCK_KEY}")
                    unlocked()
                    writeLockStatus(BuildConfig.UNLOCK_KEY, CabinetKeys.UNLOCK)
                    count++
                }
            }
        })
        setSlidingEnabled(false)

        bleDevice = OpenApp.rxBleClient.getBleDevice(macAddress)
        connectionObservable = prepareConnectionObservable()

        otpFabMenu.bindAnchorView(otpFab)

        /*otpTv.setListener {
            if (it.length > 3) {
                if (otpFabMenu.isShowing) {
                    otpFabMenu.closeMenu()

                    cabinetDragView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))

                    // check master and otp code
                    if (it == viewModel.getInfo()?.master) {
                        val random = Random()
                        val value = 100000 + random.nextInt(999999)
                        val masterCode = value.toString()

                        // write master code
                        writeLockStatus("$masterCode, $it", CabinetKeys.MASTER_CODE)
                    } else {
                        // write otp-uid and otp code
                        writeLockStatus(it, CabinetKeys.UID)
                    }
                }
            }
        }*/

        sosFab.setOnClickListener {
            viewModel.loading.set(true)
            writeLockStatus(BuildConfig.SOS_KEY, CabinetKeys.SOS)
        }
    }

    override fun onStart() {
        super.onStart()
        lifecycleSubject.onNext(ActivityEvent.START)
    }

    override fun onResume() {
        super.onResume()
        lifecycleSubject.onNext(ActivityEvent.RESUME)

        connectToLock()
    }

    override fun onPause() {
        lifecycleSubject.onNext(ActivityEvent.PAUSE)
        super.onPause()
    }

    override fun onStop() {
        lifecycleSubject.onNext(ActivityEvent.STOP)
        super.onStop()
    }

    override fun onDestroy() {
        Timber.e("onDestroyConnectionStatusBefore$isConnected")
        triggerDisconnect()
        Timber.e("onDestroyConnectionStatusAfter$isConnected")

        lifecycleSubject.onNext(ActivityEvent.DESTROY)
        super.onDestroy()
    }

    override fun onBackPressed() {

        if (otpFabMenu.isShowing) {
            otpFabMenu.closeMenu()

            cabinetDragView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
            setSlidingEnabled(true)
        } else {
            Timber.e("onBackPressedConnectionStatusBefore$isConnected")
            triggerDisconnect()
            Timber.e("onBackPressedConnectionStatusAfter$isConnected")

            startActivity(intentHomeActivity())
            overridePendingTransition(0, 0)
        }
    }

    private fun prepareConnectionObservable(): Observable<RxBleConnection> {
        return bleDevice
                .establishConnection(false)
                .takeUntil(disconnectTriggerSubject)
                .compose(bindUntilEvent(ActivityEvent.PAUSE))
                .compose(ReplayingShare.instance())
    }

    private fun connectToLock() {
        Timber.e("connectToLock")
        if (isConnected)
            triggerDisconnect()

        connectionObservable
                .flatMapSingle(RxBleConnection::discoverServices)
                .flatMapSingle { t: RxBleDeviceServices? -> t?.getCharacteristic(BleSnippet.UUID_TX_CHARACTERISTIC) }
                .observeOn(AppScheduler.mainThread())
                .doOnSubscribe {
                    showSnack(cabinetSlidingLayout, "Connecting...", Snackbar.LENGTH_INDEFINITE)
                }
//                .doAfterNext { setUpNotification() }
                .doAfterNext { readLockStatus() } // TODO: remove this line and uncomment above live
                .subscribe({
                    Timber.e("connected")

                    cabinetDragView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
                    showSnack(cabinetConstraint, "Connected", Snackbar.LENGTH_SHORT)
                    viewModel.setCabinetConnectTime()
                    locked()
                }, ({ throwable ->
                    cabinetDragView.setBackgroundColor(ContextCompat.getColor(this, R.color.grey))
                    showSnack(cabinetConstraint, throwable.localizedMessage, Snackbar.LENGTH_SHORT)
                    setSlidingEnabled(false)
                    Timber.e(throwable.localizedMessage)

                    onBackPressed()
                }), ({
                    Timber.e("connectToLockFinishes")
                }))
    }

    private fun triggerDisconnect() {
        if (isConnected) {
            disconnectTriggerSubject.onNext(true)
        }

        viewModel.setCabinetDisconnectTime()
        viewModel.setAttemptCount(count)
    }

    private fun readLockStatus() {
        Timber.e("readLockStatus")
        // TODO: handle else condition here
        if (isConnected)
            connectionObservable
                    .firstOrError()
                    .flatMap { rxBleConnection ->
                        rxBleConnection.readCharacteristic(BleSnippet.UUID_DEVICE_READ_SERVICE)
                        Single.zip(
                                rxBleConnection.readCharacteristic(BleSnippet.UUID_DEVICE_READ_SERVICE),
                                rxBleConnection.readCharacteristic(BleSnippet.UUID_BATTERY_LEVEL),
                                BiFunction<ByteArray, ByteArray, Operations> { reg, time ->
                                    val cabinetSyncOperations = Operations()
                                    /*val regAdmin = String(reg)
                                    if (regAdmin.contains("UNREGISTERED"))
                                        cabinetSyncOperations.regAdmin = null
                                    else
                                        cabinetSyncOperations.regAdmin = Integer.parseInt(regAdmin.split("_")[1])*/

                                    Timber.e(Integer.parseInt(AppSnippet.bytesToHex(time), 16).toString())
                                    cabinetSyncOperations.battery = Integer.parseInt(AppSnippet.bytesToHex(time), 16)

                                    return@BiFunction cabinetSyncOperations
                                }
                        )
                    }
                    /*.doAfterSuccess {
                        if (it.regAdmin == 0)
                            writeLockStatus(viewModel.getUserId().toString(), Constants.CabinetKeys.REG_ADMIN)
                    }*/
                    .observeOn(AppScheduler.mainThread())
                    .subscribe({
                        viewModel.setCabinetOperations(it)
                    }, ({ throwable ->
                        Timber.e(throwable.localizedMessage)
                    }))
    }

    private fun setUpNotification() {
        Timber.e("set up notification")
        if (isConnected) {
            var usage = false

            connectionObservable
                    .flatMap { rxBleConnection -> rxBleConnection.setupNotification(BleSnippet.UUID_TX_CHARACTERISTIC) }
                    .doOnNext { Timber.e("Notification has been set up!") }
                    .doAfterNext {
                        Timber.e("After Notification")
//                        readLockStatus()
                    }
                    .flatMap { notificationObservable -> notificationObservable }
                    .observeOn(AppScheduler.mainThread())
                    .subscribe({ bytes ->
                        val lockInfo = String(bytes)
                        val info = lockInfo.split(",")
                        Timber.e("lockinfo$lockInfo")

                        if (info.size == 5 && !usage) {
                            if (info[3] == "1") {
                                otpFab.visibility = View.INVISIBLE
                                setSlidingEnabled(false)
                                showErrorSheet(getString(R.string.oops), getString(R.string.otp_message) + if (mOtpName.isNullOrEmpty()) "your admin" else mOtpName, getString(R.string.got_it))
                            } else {
                                otpFab.visibility = View.VISIBLE
                                setSlidingEnabled(true)
                            }
                            usage = true

                            viewModel.setUsage(Usage(info[0], info[1], info[2], info[3], info[4]))

                            writeLockStatus(BuildConfig.CAB_GET_INFO, Constants.CabinetKeys.GET_INFO)
                        } else if (info.size == 4 && usage) {
                            usage = false

                            viewModel.setInfo(Info(info[0], info[1], "0", info[3]))
                        }
                    }, ({ throwable ->
                        Timber.e(throwable.localizedMessage)
                    }))
        }
    }

    private fun writeLockStatus(code: String, type: String) {
        // TODO: handle else condition here
        if (isConnected)
            connectionObservable
                    .firstOrError()
                    .flatMap { t: RxBleConnection ->
                        val value: ByteArray = when (type) {
                            CabinetKeys.APP_CODE -> (BuildConfig.CAB_APP_CODE + code).toByteArray()
                            CabinetKeys.GET_INFO -> code.toByteArray()
                            CabinetKeys.MASTER_CODE -> (BuildConfig.CAB_MAS_CODE + code.split(",")[0]).toByteArray()
                            CabinetKeys.REG_ADMIN -> (BuildConfig.CAB_REG_ADMIN_CODE + code).toByteArray()
                            CabinetKeys.RESTART -> code.toByteArray()
                            CabinetKeys.SLEEP -> code.toByteArray()
                            CabinetKeys.SOS -> BuildConfig.SOS_KEY.toByteArray()
                            CabinetKeys.UID -> (BuildConfig.CAB_OTP_UID + viewModel.getUserId()).toByteArray()
                            CabinetKeys.UNLOCK -> BuildConfig.UNLOCK_KEY.toByteArray()
                            else -> BuildConfig.UNLOCK_KEY.toByteArray()
                        }

                        t.writeCharacteristic(BleSnippet.UUID_RX_CHARACTERISTIC, value)
                    }
                    .observeOn(AppScheduler.mainThread())
                    .doAfterSuccess { Timber.e(type) }
                    .subscribe({
                        Timber.e("writeLockStatusConnected")

                        when (type) {
                            CabinetKeys.APP_CODE -> {
                                showSuccessSheet(R.string.success, R.string.otp_is_step, R.string.got_it) {
                                    dismiss()
                                    this@CabinetActivity.onBackPressed()
                                }

                                // set otp code
                                viewModel.setCabinetOtp(code)
                            }
                            CabinetKeys.MASTER_CODE -> {
                                // set master code
                                viewModel.setCabinetMaster(code.split(",")[0])

                                Handler().postDelayed({
                                    // write otp cope
//                                    writeLockStatus(code.split(",")[1], CabinetKeys.UID)
                                }, 1000)
                            }
                            CabinetKeys.SOS -> {
                                viewModel.loading.set(false)
                                viewModel.setAndAddSosTime(AppSnippet.currDate)
                                sosFab.visibility = View.GONE
                            }
                            CabinetKeys.UID -> {
                                Handler().postDelayed({
                                    // write app code
//                                    writeLockStatus(code, CabinetKeys.APP_CODE)
                                }, 1000)
                            }
                            CabinetKeys.UNLOCK -> {
                                Handler().postDelayed({
                                    locked()
                                }, 4000)

                                viewModel.setAndAddOperateTime(AppSnippet.currDate)
                            }
                            CabinetKeys.REG_ADMIN -> viewModel.setRegisteredAdmin(viewModel.getUserId())
                        }
                    }, ({ throwable ->
                        Timber.e(throwable.localizedMessage)
                        locked()
                    }))
    }

    private fun showPanelUp() {
        Timber.e("showPanelUp")
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorBottomSuccess)

        swipeToUnlockIv.visibility = View.GONE
        swipeToUnlockTv.visibility = View.GONE
        swipeToUnlockMsgTv.visibility = View.GONE

        lockStatus.visibility = View.VISIBLE
        operateSSIDPanel.visibility = View.VISIBLE
        unlockLottie.visibility = View.VISIBLE
    }

    private fun showPanelDown() {
        Timber.e("showPanelDown")
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorBottomError)

        lockStatus.visibility = View.GONE
        operateSSIDPanel.visibility = View.GONE
        swipeToLockIv.visibility = View.GONE
        unlockLottie.visibility = View.GONE

        swipeToUnlockIv.visibility = View.VISIBLE
        swipeToUnlockTv.visibility = View.VISIBLE
        swipeToUnlockMsgTv.visibility = View.VISIBLE
    }

    private fun locked() {
        Timber.e("locked")
        setSlidingEnabled(true)
        cabinetSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED

        showPanelDown()
    }

    private fun unlocked() {
        Timber.e("unlocked")
        setSlidingEnabled(false)

        showPanelUp()
        swipeToLockIv.visibility = View.GONE

        lockStatus.text = getString(R.string.unlocked_shackle_out)
        lockStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBottomSuccess))
    }

    private fun setSlidingEnabled(value: Boolean) {
        cabinetSlidingLayout.isEnabled = value
    }
}