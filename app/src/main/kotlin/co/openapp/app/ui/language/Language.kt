package co.openapp.app.ui.language


/**
 * Created by deepanshusinha on 13/02/18.
 */

data class Language(

        var languageName: String? = null,

        var languageCode: String? = null
)/* {
    @Id
    var id: Long = 0

//    lateinit var country: ToOne<Country>
}*/