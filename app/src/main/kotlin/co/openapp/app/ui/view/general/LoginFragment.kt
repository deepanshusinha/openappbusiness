package co.openapp.app.ui.view.general

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.vo.ErrorResponse
import co.openapp.app.data.source.Status
import co.openapp.app.databinding.FragmentLoginBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.utils.AppAnalytics
import co.openapp.app.viewmodel.AuthViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment<FragmentLoginBinding, AuthViewModel>() {
    override val layoutRes: Int
        get() = R.layout.fragment_login

    override fun getViewModel(): Class<AuthViewModel> = AuthViewModel::class.java

    override fun bindingVariable(): Int = BR.login

    private var firstTime = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dataBinding.login = viewModel

        (activity as GeneralActivity).setupUI(loginConstraint)

        loginForgot.setOnClickListener { findNavController().navigate(R.id.action_loginFragment_to_forgotPasswordFragment) }

        loginRegister.setOnClickListener { findNavController().navigate(R.id.action_loginFragment_to_registerFragment) }

        viewModel.showErrorSheet.observe(this, Observer {
            (activity as GeneralActivity).showErrorSheet("Error", it, "Got It!")
        })

        viewModel.loginResult.observe(this, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {
                    viewModel.loading.set(false)
                    val responseData = response.data

                    if (responseData != null) {
                        if (responseData.status) {
                            if (firstTime) {
                                AppAnalytics.logLogin(viewModel.auth.fields.userName, true)
                                firstTime = false
                                val bundle = bundleOf("login" to true)
                                findNavController().navigate(R.id.action_loginFragment_to_scanLocksFragment, bundle)
                            }
                        } else {
                            AppAnalytics.logLogin(viewModel.auth.fields.userName, false)
                            when (responseData.errorCode) {
                                700 -> {
                                    showSheet(getString(R.string.error), getString(R.string.code_700), getString(R.string.got_it))
                                }
                                703 -> {
                                    showSheet(getString(R.string.error), getString(R.string.code_703), getString(R.string.got_it))
                                }
                                // TODO: Handle for email not verified and phone not verified
                                else -> showSheet(getString(R.string.error), responseData.message, getString(R.string.got_it))
                            }
                        }
                    }
                    // TODO: Handle Else
                }
                Status.ERROR -> {
                    viewModel.loading.set(false)
                    val authResponse = Gson().fromJson(response.message, ErrorResponse::class.java)
                    showSheet(getString(R.string.error), authResponse.message, getString(R.string.got_it))
                }
                Status.LOADING -> {
                    viewModel.loading.set(true)
                }
            }
        })
    }

    private fun showSheet(title: String, message: String, action: String) {
        (activity as GeneralActivity).showErrorSheet(title, message, action)
    }
}