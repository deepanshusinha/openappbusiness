package co.openapp.app.ui.home

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.*
import co.openapp.app.BR
import co.openapp.app.OpenApp
import co.openapp.app.R
import co.openapp.app.data.model.request.KeyRequest
import co.openapp.app.databinding.ActivityHomeBinding
import co.openapp.app.ui.base.BaseActivity
import co.openapp.app.utils.ble.OnFoundDeviceListener
import co.openapp.app.utils.ble.TTLockBle
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.constant.Constants.RequestKeys.Companion.SMS_PERMISSION_CODE
import co.openapp.app.utils.constant.DialogBoxInput
import co.openapp.app.utils.extensions.intentPlayStoreActivity
import co.openapp.app.utils.service.SmsBroadcastReceiver
import co.openapp.app.utils.workmanager.ListWorkManager
import co.openapp.app.viewmodel.HomeViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.ttlock.bl.sdk.entity.LockData
import com.ttlock.bl.sdk.scanner.ExtendedBluetoothDevice
import kotlinx.android.synthetic.main.activity_home.*
import timber.log.Timber
import java.util.concurrent.TimeUnit


class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(),
        DoorAdapter.OnItemClickListener, OnFoundDeviceListener, SmsBroadcastReceiver.Listener {

    override val layoutRes: Int
        get() = R.layout.activity_home

    override fun viewModel(): Class<HomeViewModel> = HomeViewModel::class.java

    override fun bindingVariable(): Int = BR.home

    private var mTTLock: TTLockBle? = null

    private lateinit var mAdapter: DoorAdapter

    private val MAKE_CALL_PERMISSION_REQUEST_CODE = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bottomSheetBehavior = BottomSheetBehavior.from(doorLockSheet)

        // when it is collapsed
        bottomSheetBehavior.peekHeight = 0

        // change the state of the bottom sheet
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        setAdapter()

        viewModel.navigateToScanActivity.observe(this, Observer {
            //            startActivity(intentScanActivity())
            overridePendingTransition(R.anim.enter, R.anim.exit)
        })

        viewModel.showForceDialog.observe(this, Observer {
            showDialog(R.string.update_title, R.string.update_msg, R.string.update) {
                // Navigate To Play Store
                try {
                    startActivity(intentPlayStoreActivity())
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
        })
        // TODO: Register a service which do fetch and sync api

        if (viewModel.isAdmin()) {
            fabDoor.visibility = View.VISIBLE
        }

        homeFab.setClosedOnTouchOutside(true)

        homeFab.setOnMenuButtonClickListener {
            homeFab.toggle(true)
        }

        fabDoor.setOnClickListener {
            homeFab.close(false)

            if (mTTLock == null)
                mTTLock = TTLockBle(this)

            val state = bottomSheetBehavior.state

            if (state == BottomSheetBehavior.STATE_EXPANDED) {
                mTTLock?.stopBTDeviceScan()
                mAdapter.clearScanResults()

                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            } else if (state == BottomSheetBehavior.STATE_HIDDEN || state == BottomSheetBehavior.STATE_COLLAPSED) {
                mTTLock?.startBTDeviceScan()

                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }

        fabHelp.setOnClickListener { startCallingIntent() }

        if (isSmsPermissionGranted())
            startMessageService()
        else
            showRequestPermissionsInfoAlertDialog()

        startWorkManager()
    }

    private fun startWorkManager() {

        val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresCharging(true)
                .setRequiresBatteryNotLow(true)
                .build()

        val workRequest = PeriodicWorkRequestBuilder<ListWorkManager>(15, TimeUnit.MINUTES)
                .addTag(Constants.WorkManagerKeys.List_PERIOD_WORK)
                .setConstraints(constraints)
                .build()

        WorkManager.getInstance().enqueueUniquePeriodicWork(
                Constants.WorkManagerKeys.List_PERIOD_WORK,
                ExistingPeriodicWorkPolicy.KEEP,
                workRequest
        )
    }

    override fun onResume() {
        super.onResume()
        viewModel.callGeneralApi()
        viewModel.fetchLockList()
//        viewModel.pushDataToServer()
        viewModel.pushNokeLockPwdUpdateToServer()
    }

    override fun onDestroy() {
        mTTLock = null

        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            MAKE_CALL_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startCallingIntent()
                }
                return
            }
            SMS_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startMessageService()
                }
                return
            }
        }
    }

    private fun startMessageService() {
        OpenApp.smsBroadcastReceiver.setListener(this)
    }

    private fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
    }

    private fun startCallingIntent() {
        if (checkPermission(Manifest.permission.CALL_PHONE))
            startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel: +918040909955")))
        else
            requestPermissionsSafely(arrayOf(Manifest.permission.CALL_PHONE), MAKE_CALL_PERMISSION_REQUEST_CODE)
    }

    private fun setAdapter() {
        doorLockList.layoutManager = LinearLayoutManager(this)
        doorLockList.setHasFixedSize(true)
        mAdapter = DoorAdapter(this)
        doorLockList.adapter = mAdapter
    }

    override fun onItemClick(position: Int) {
        val extendedBluetoothDevice = mAdapter.itemList[position]

        if (extendedBluetoothDevice.isSettingMode) {
            viewModel.loading.set(true)
            mTTLock?.doAddAdministrator(extendedBluetoothDevice)
        } else
            toast("Already OnBoarded")
    }

    override fun onFoundDevice(extendedBluetoothDevice: ExtendedBluetoothDevice?) {
        if (extendedBluetoothDevice != null) {
            runOnUiThread { mAdapter.setData(extendedBluetoothDevice) }
        }
    }

    override fun onGetKey(lockData: LockData) {
        runOnUiThread {
            viewModel.loading.set(false)
            showDialog(R.string.product_id, R.string.product_msg, R.string.set, DialogBoxInput.EDIT_TEXT) {
                hideDialog()
                val productId = getDialog()?.getProductId()
                if (productId != null && productId.isNotEmpty()) {
//                    key.productId = productId.toUpperCase()

                    // call init api
//                    viewModel.initApi(key)
                }
            }
        }

    }

    /**
     * Check if we have SMS permission
     */
    private fun isSmsPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
    }

    /**
     * Request runtime SMS permission
     */
    private fun requestReadAndSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
            // You may display a non-blocking explanation here, read more in the documentation:
            // https://developer.android.com/training/permissions/requesting.html
        }
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS), SMS_PERMISSION_CODE)
    }

    /**
     * Displays an AlertDialog explaining the user why the SMS permission is going to be requests
     */
    private fun showRequestPermissionsInfoAlertDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Sms Permission")
        builder.setMessage("Sms permission is required to read messages")

        builder.setPositiveButton("OK") { dialogInterface, _ ->
            dialogInterface.dismiss()
            // Display system runtime permission request?
            requestReadAndSendSmsPermission()
        }

        builder.setCancelable(false)
        builder.show()
    }

    override fun onTextReceived(text: String) {
        Timber.e(text)
    }
}
