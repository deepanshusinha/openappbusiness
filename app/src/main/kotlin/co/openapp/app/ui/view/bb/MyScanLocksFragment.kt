package co.openapp.app.ui.view.bb

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.model.lock.LockData
import co.openapp.app.databinding.FragmentMyLocksBinding
import co.openapp.app.ui.adapter.LockAdapter
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.utils.constant.Constants
import co.openapp.app.viewmodel.GeneralViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_general.*
import kotlinx.android.synthetic.main.fragment_my_locks.*
import timber.log.Timber

class MyScanLocksFragment : BaseFragment<FragmentMyLocksBinding, GeneralViewModel>(), LockAdapter.OnItemClickListener {

    override val layoutRes: Int
        get() = R.layout.fragment_my_locks

    override fun getViewModel(): Class<GeneralViewModel> = GeneralViewModel::class.java

    override fun bindingVariable(): Int = BR.my_locks

    private lateinit var mLockAdapter: LockAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as GeneralActivity).appBar.visibility = View.VISIBLE

        mLockAdapter = LockAdapter(this@MyScanLocksFragment)

        dataBinding.myLocks = viewModel
        dataBinding.lockListRv.adapter = mLockAdapter

        viewModel.showErrorSheet.observe(this, Observer {
            swipeRefresh.isRefreshing = false
            (activity as GeneralActivity).showErrorSheet("Error", it, "Got It!")
        })

        // get arguments form bundle
        val string = arguments?.getString(Constants.IntentKeys.SCANNED_LOCK_LIST)

        if (string != null) {
            val gson = Gson()
            val result = gson.fromJson<ArrayList<LockData>>(string, object : TypeToken<ArrayList<LockData>>() {}.type)
            updateList(result)
        }

        viewModel.clickedLock.observe(this, Observer { lockItem ->
            val lockDetailsString = Gson().toJson(lockItem)
            val bundle = Bundle()
            when (lockItem.lockType) {
                Constants.LockType.PAD_LOCK -> {
                    bundle.putString(Constants.LockType.PAD_LOCK, lockDetailsString)
                    findNavController().navigate(R.id.action_myScanLocksFragment_to_nokelockActivity, bundle)
                }
                Constants.LockType.DOOR_LOCK -> {
                    bundle.putString(Constants.LockType.DOOR_LOCK, lockDetailsString)
                    findNavController().navigate(R.id.action_myScanLocksFragment_to_doorActivity, bundle)
                }
            }
        })
    }

    private fun updateList(result: List<LockData>) {
        dataBinding.hasLocks = (result.isNotEmpty())
        try {
            mLockAdapter.currentList.clear()
        } catch (exception: UnsupportedOperationException) {
            Timber.e(exception.localizedMessage)
        } catch (exception: Exception) {
            Timber.e(exception.localizedMessage)
        }
        Timber.e("Result - $result")
        mLockAdapter.submitList(result)
    }

    override fun onItemClick(lockItem: LockData) {
        viewModel.isLockRule(lockItem.macAddress, lockItem.id)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}