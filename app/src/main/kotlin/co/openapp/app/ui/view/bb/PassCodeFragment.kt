package co.openapp.app.ui.view.bb

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.model.lock.PassCode
import co.openapp.app.data.source.Status
import co.openapp.app.databinding.FragmentPasscodeBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.utils.AppSnippet
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.constant.PassCodeType
import co.openapp.app.viewmodel.DoorViewModel
import kotlinx.android.synthetic.main.activity_general.*
import kotlinx.android.synthetic.main.fragment_passcode.*
import timber.log.Timber
import java.util.*


class PassCodeFragment : BaseFragment<FragmentPasscodeBinding, DoorViewModel>() {

    enum class TYPE {
        START_TIME,
        END_TIME
    }

    override val layoutRes: Int
        get() = R.layout.fragment_passcode

    override fun getViewModel(): Class<DoorViewModel> = DoorViewModel::class.java

    override fun bindingVariable(): Int = BR.passcode

    private lateinit var date: String

    private lateinit var time: String
    private val calendar = Calendar.getInstance()

    private var timeType: TYPE = TYPE.START_TIME
    private var passcodeType: PassCodeType = PassCodeType.ONE_TIME

    private val dateListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
        timePickerDialog.show()

        date = "$year-${month + 1}-$dayOfMonth"

        when (timeType) {
            TYPE.START_TIME -> start.text = date
            TYPE.END_TIME -> end.text = date
        }
    }

    private val timeListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
        time = "$date $hourOfDay:$minute"

        when (timeType) {
            TYPE.START_TIME -> start.text = time
            TYPE.END_TIME -> end.text = time
        }
    }

    private lateinit var datePickerDialog: DatePickerDialog
    private lateinit var timePickerDialog: TimePickerDialog

    private val args: PassCodeFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as GeneralActivity).appBar.visibility = View.VISIBLE

        datePickerDialog = DatePickerDialog(
                requireContext(),
                dateListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        )

        timePickerDialog = TimePickerDialog(
                requireContext(),
                timeListener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        )

        viewModel.showErrorSheet.observe(this, Observer {
            viewModel.loading.set(false)
            (activity as GeneralActivity).showErrorSheet(getString(R.string.error), it, getString(R.string.got_it))
        })

        viewModel.showSuccessSheet.observe(this, Observer {
            viewModel.loading.set(false)
            (activity as GeneralActivity).showSuccessSheet(getString(R.string.success), it, getString(R.string.got_it))
        })

        viewModel.generatePassCodeRepositories.observe(this, Observer { response ->
            if (response != null)
                when (response.status) {
                    Status.SUCCESS -> {
                        viewModel.loading.set(false)
                        val data = response.data
                        if (data != null) {
                            if (data.status)
                                viewModel.showSuccessSheet.value = data.message + "\n" + "Your PassCode is: ${data.data?.passCode}"
                            else
                                viewModel.showErrorSheet.value = data.message
                        } else
                            viewModel.showErrorSheet.value = response.message
                    }
                    Status.ERROR -> {
                        viewModel.loading.set(false)
                        val data = response.data
                        if (data != null && data.status) {
                            viewModel.showErrorSheet.value = data.message
                        } else
                            viewModel.showErrorSheet.value = response.message
                    }
                    Status.LOADING -> {
                        viewModel.loading.set(true)
                    }
                }
        })

        startCardView.setOnClickListener {
            timeType = TYPE.START_TIME
            datePickerDialog.show()
        }

        endCardView.setOnClickListener {
            timeType = TYPE.END_TIME
            datePickerDialog.show()
        }

        passcodeButton.setOnClickListener {
            when (passcodeType) {
                PassCodeType.NONE -> viewModel.showErrorSheet.value = "Please Select PassCode Type"
                PassCodeType.ONE_TIME -> generatePassCode(oneTimeTV.text.toString(), 1)
                PassCodeType.PERMANENT -> generatePassCode(timedTV.text.toString(), 2)
                PassCodeType.PERIOD -> generatePassCode(
                        timedTV.text.toString(),
                        3,
                        AppSnippet.getEpochFromDateString(start.text.toString(), Constants.DateKeys.YYYYMMDD_HHMM),
                        AppSnippet.getEpochFromDateString(end.text.toString(), Constants.DateKeys.YYYYMMDD_HHMM)
                )
                PassCodeType.DELETE -> generatePassCode(resetTV.text.toString(), 4)
                else -> generatePassCode("", 0)
            }
        }

        oneTimeLayout.addListener { _, expanded ->
            passcodeType = if (expanded)
                PassCodeType.ONE_TIME
            else
                PassCodeType.NONE
        }

        timedLayout.addListener { _, expanded ->
            passcodeType = if (expanded)
                PassCodeType.PERIOD
            else
                PassCodeType.NONE
        }

        eraseLayout.addListener { _, expanded ->
            passcodeType = if (expanded)
                PassCodeType.DELETE
            else
                PassCodeType.NONE
        }

        permanentView.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                startCardView.visibility = View.GONE
                endCardView.visibility = View.GONE

                passcodeType = PassCodeType.PERMANENT
                message.text = getString(R.string.permanent_message)
            } else {
                startCardView.visibility = View.VISIBLE
                endCardView.visibility = View.VISIBLE

                passcodeType = PassCodeType.PERIOD
                message.text = getString(R.string.timed_message)
            }
        }

        start.text = AppSnippet.currDate
        end.text = AppSnippet.currDate

        Timber.e(AppSnippet.currDate)
    }

    private fun generatePassCode(email: String, type: Int, startDate: Long = 0, endDate: Long = 0) {
        if (email.isEmpty())
            viewModel.showErrorSheet.value = getString(R.string.user_name_field_required)
        else
            viewModel.generatePassCode(PassCode(email, type, startDate, endDate), args.lockInfo.id)
    }
}