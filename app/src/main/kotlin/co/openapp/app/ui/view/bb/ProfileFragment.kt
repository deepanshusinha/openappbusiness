package co.openapp.app.ui.view.bb

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.source.Status
import co.openapp.app.databinding.FragmentProfileBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.ui.view.main.GeneralActivity
import co.openapp.app.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_general.*
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment<FragmentProfileBinding, AuthViewModel>() {
    override val layoutRes: Int
        get() = R.layout.fragment_profile

    override fun getViewModel(): Class<AuthViewModel> = AuthViewModel::class.java

    override fun bindingVariable(): Int = BR.profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as GeneralActivity).appBar.visibility = View.GONE

        dataBinding.profile = viewModel

        viewModel.profileDetailList()

        viewModel.profileResult.observe(this, Observer {
            if (it.status == Status.SUCCESS) {
                val userList = it.data

                if (!userList.isNullOrEmpty()) {
                    val userData = userList[0]

                    email.text = userData.email
                    val phoneNumber = userData.phone
                    if (phoneNumber != null && phoneNumber.isNotEmpty()) {
                        phone.text = phoneNumber
                        phoneCard.visibility = View.VISIBLE
                    }

                    var fullName = userData.firstName
                    val lastNameValue = userData.lastName
                    if (lastNameValue != null && lastNameValue.isNotEmpty()) {
                        fullName = "$fullName $lastNameValue"
                    }

                    name.text = fullName

                    val businessList = userData.business
                    if (businessList.isNotEmpty()) {
                        organisation.text = businessList[0].subCompanyName
                    }
                }
            }
        })

        changePasswordCard.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_changePasswordFragment)
        }

        version.text = viewModel.getVersionName()
    }
}