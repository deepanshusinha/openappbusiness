package co.openapp.app.ui.view.general

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.databinding.FragmentIntroBinding
import co.openapp.app.ui.adapter.SliderAdapter
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.fragment_intro.*

class IntroFragment : BaseFragment<FragmentIntroBinding, AuthViewModel>() {
    override val layoutRes: Int
        get() = R.layout.fragment_intro

    override fun getViewModel(): Class<AuthViewModel> = AuthViewModel::class.java

    override fun bindingVariable(): Int = BR.intro

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (viewModel.isLogin) {
            findNavController().navigate(R.id.action_introFragment_to_scanLocksFragment)
        }

        val adapter = SliderAdapter(childFragmentManager)

        introPager.adapter = adapter
        introPager.setOnTouchListener { _, _ -> true }

        viewModel.changeAnimation.observe(this, Observer {
            val currentItem = introPager.currentItem
            if (currentItem < adapter.count - 1) {
                introPager.currentItem = currentItem + 1
            } else {
                introPager.currentItem = 0
            }
        })

        viewModel.startAnimation()

        introLogin.setOnClickListener {
            findNavController().navigate(R.id.action_introFragment_to_loginFragment)
        }

        introRegister.setOnClickListener {
            findNavController().navigate(R.id.action_introFragment_to_registerFragment)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}