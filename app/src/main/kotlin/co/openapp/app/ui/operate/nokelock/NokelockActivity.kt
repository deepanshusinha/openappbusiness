package co.openapp.app.ui.operate.nokelock

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import co.openapp.app.BR
import co.openapp.app.OpenApp
import co.openapp.app.R
import co.openapp.app.data.model.lock.LockData
import co.openapp.app.data.model.lock.VerifyTripId
import co.openapp.app.data.model.lock.VerifyTripOtp
import co.openapp.app.data.source.Status.*
import co.openapp.app.databinding.ActivityNokelockBinding
import co.openapp.app.ui.base.BaseActivity
import co.openapp.app.utils.constant.Constants
import co.openapp.app.utils.constant.DialogBoxInput
import co.openapp.app.viewmodel.PadlockViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.sunshine.blelibrary.config.Config
import com.sunshine.blelibrary.config.LockType
import com.sunshine.blelibrary.inter.OnConnectionListener
import com.sunshine.blelibrary.utils.GlobalParameterUtils
import kotlinx.android.synthetic.main.activity_nokelock.padlockConstraint
import kotlinx.android.synthetic.main.activity_nokelock.padlockSSID
import kotlinx.android.synthetic.main.activity_nokelock.padlockSlidingLayout
import kotlinx.android.synthetic.main.activity_nokelock.swipeToUnlockIv
import kotlinx.android.synthetic.main.activity_nokelock.swipeToUnlockMsgTv
import kotlinx.android.synthetic.main.activity_nokelock.swipeToUnlockTv
import kotlinx.android.synthetic.main.activity_padlock.*
import kotlinx.android.synthetic.main.layout_unlock.*
import timber.log.Timber


/**
 * Created by Deepanshu on 06-12-2017.
 * To handle operation of locks
 */

class NokelockActivity : BaseActivity<ActivityNokelockBinding, PadlockViewModel>(), OnConnectionListener {

    override val layoutRes: Int
        get() = R.layout.activity_nokelock

    override fun viewModel(): Class<PadlockViewModel> = PadlockViewModel::class.java

    override fun bindingVariable(): Int = BR.nokelock

    private var connect = true

    private lateinit var mMacAddress: String
    private var lockId = ""
    private var tripSheetId = ""
    private var lockUnlocked = false
    private var oldState = SlidingUpPanelLayout.PanelState.COLLAPSED
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.showErrorSheet.observe(this, Observer {
            it?.let { it1 -> showErrorSheet(getString(R.string.error), it1, getString(R.string.got_it)) }
        })

        viewModel.showSuccessSheet.observe(this, Observer {
            it?.let { it1 -> showSuccessSheet(getString(R.string.success), it1, getString(R.string.got_it)) }
        })

        viewModel.unLockedState.observe(this, Observer { unLockedState ->
            if (unLockedState) {
                unlocked(false)
            } else {
                locked(true)
            }
        })

        val lockItemString = intent?.getStringExtra(Constants.LockType.PAD_LOCK)
        val gson = Gson()
        val lockItem = gson.fromJson<LockData>(lockItemString, object : TypeToken<LockData>() {}.type)
        val timeRules = lockItem.timeRules
        if (timeRules.isNotEmpty()) {
            viewModel.mTimeRules = timeRules
        }

        mMacAddress = lockItem.macAddress.toUpperCase()
        lockId = lockItem.id
        if (lockItem.tripSheet.isNotEmpty()) {
            tripSheetId = lockItem.tripSheet[0].tripSheetId
        }
        val deviceName = lockItem.lockName
        val pwdKey = lockItem.parameters.blePassword!!
        val tokenKey = lockItem.parameters.aesKey
        val tokenList = tokenKey.split(",")
        val tokenArray = ByteArray(tokenList.size)
        tokenList.forEachIndexed { index, s ->
            tokenArray[index] = s.trim().toByte()
        }

        Config.yx_key = tokenArray

        if (pwdKey.contains(",")) {
            val pwdList = pwdKey.split(",")
            val pwdArray = ByteArray(pwdList.size)
            pwdList.forEachIndexed { index, s ->
                pwdArray[index] = s.trim().toByte()
            }
            Config.password = pwdArray
        } else
            Config.password = pwdKey.toByteArray()

        GlobalParameterUtils.getInstance().lockType = LockType.YXS

        padlockSSID.text = deviceName
        operateSSIDPanel.text = deviceName

        padlockDragView.setOnClickListener {
            val panelState = padlockSlidingLayout.panelState
            val collapsed = SlidingUpPanelLayout.PanelState.COLLAPSED
            val expanded = SlidingUpPanelLayout.PanelState.EXPANDED

            if (panelState == collapsed) {
                padlockSlidingLayout.panelState = expanded
            } else if (panelState == expanded) {
                padlockSlidingLayout.panelState = collapsed
            }

        }

        padlockSlidingLayout.addPanelSlideListener(object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View?, slideOffset: Float) {
            }

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {
                Timber.e("state-$previousState panel-$newState oldState-$oldState")
                if (previousState == SlidingUpPanelLayout.PanelState.DRAGGING &&
                        newState == SlidingUpPanelLayout.PanelState.EXPANDED &&
                        oldState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    oldState = newState
                    // Swipe up to unlock a lock
                    if (viewModel.isLockClosingData() && lockUnlocked) {
                        Timber.e("Lock is already opened")
                        Handler().postDelayed({
                            lockStatus.text = "Swipe Down to Lock"

                            if (tripSheetId.isNotEmpty()) {
                                // check for trip sheet id
                                showDialog(
                                        R.string.trip_id,
                                        R.string.trip_id_message,
                                        R.string.verify,
                                        DialogBoxInput.EDIT_TEXT) {
                                    val tripId = getProductId()
                                    if (tripId.isEmpty()) {
                                        viewModel.showErrorSheet.value = getString(R.string.trip_id_valid)
                                    } else {
                                        viewModel.verifyTripSheetId(VerifyTripId(tripSheetId, tripId))
                                    }
                                }
                            }
                        }, 1000)
                        lockStatus.text = "Lock is already opened"
                    } else {
                        viewModel.unLockedState.postValue(true)

                        OpenApp.instance?.getIBLE()?.openLock()
                    }
                } else if (previousState == SlidingUpPanelLayout.PanelState.DRAGGING &&
                        newState == SlidingUpPanelLayout.PanelState.COLLAPSED &&
                        oldState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    oldState = newState
                    // Swipe down to lock a lock
                    setSlidingEnabled(false)
                    viewModel.unLockedState.postValue(false)
                    viewModel.setAndAddOperateLockedTime()
                    Handler().postDelayed({
                        finish()
                    }, 500)
                }
            }
        })

        setSlidingEnabled(false)

        registerReceiver(viewModel.broadcastReceiver, Config.initFilter())

        connectToLock()

        // Temporary
        viewModel.temp.observe(this, Observer { value ->
            if (value) {
                setSlidingEnabled(true)
                lockStatus.text = "Swipe Down to Lock"
                padlockSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED

                if (tripSheetId.isNotEmpty()) {
                    // check for trip sheet id
                    showDialog(
                            R.string.trip_id,
                            R.string.trip_id_message,
                            R.string.verify,
                            DialogBoxInput.EDIT_TEXT) {
                        val tripId = getProductId()
                        if (tripId.isEmpty()) {
                            viewModel.showErrorSheet.value = getString(R.string.trip_id_valid)
                        } else {
                            viewModel.verifyTripSheetId(VerifyTripId(tripSheetId, tripId))
                        }
                    }
                }
            }
        })

        viewModel.onBackPressed.observe(this, Observer {
            finish()
        })

        viewModel.verifyTripSheetOtpRepositories.observe(this, Observer { response ->
            if (response != null) {
                when (response.status) {
                    SUCCESS -> {
                        val data = response.data
                        if (data != null && data.status) {
                            viewModel.showSuccessSheet.value = "Validation Successful"
                            hideDialog()
                        } else
                            viewModel.showErrorSheet.value = data?.message
                    }
                    ERROR -> {
                        val data = response.data
                        if (data != null && data.status) {
                            viewModel.showErrorSheet.value = data.message
                        } else
                            viewModel.showErrorSheet.value = response.message
                    }
                    LOADING -> {
                    }
                }
            }
        })

        viewModel.verifyTripSheetIdRepositories.observe(this, Observer { response ->
            if (response != null) {
                when (response.status) {
                    SUCCESS -> {
                        val data = response.data
                        if (data != null && data.status) {
                            viewModel.showSuccessSheet.value = "Validation Successful"
                            hideDialog()
                        } else
                            viewModel.showErrorSheet.value = data?.message
                    }
                    ERROR -> {
                        val data = response.data
                        if (data != null && data.status) {
                            viewModel.showErrorSheet.value = data.message
                        } else
                            viewModel.showErrorSheet.value = response.message
                    }
                    LOADING -> {
                    }
                }
            }
        })
    }

    override fun onPause() {
        connect = false
        OpenApp.instance?.getIBLE()?.disconnect()
        super.onPause()
    }

    override fun onDestroy() {
        OpenApp.instance?.getIBLE()?.close()

        unregisterReceiver(viewModel.broadcastReceiver)

        // Stop Sync Engaged
        viewModel.setSyncEngaged(false)

        super.onDestroy()
    }

    private fun showPanelUp() {
        Timber.d("showPanelUp")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorBottomSuccess)
        }

        swipeToUnlockIv.visibility = View.GONE
        swipeToUnlockTv.visibility = View.GONE
        swipeToUnlockMsgTv.visibility = View.GONE

        lockStatus.visibility = View.VISIBLE
        operateSSIDPanel.visibility = View.VISIBLE
        unlockLottie.visibility = View.VISIBLE
    }

    private fun showPanelDown() {
        Timber.d("showPanelDown")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorBottomError)
        }

        lockStatus.visibility = View.GONE
        operateSSIDPanel.visibility = View.GONE
        swipeToLockIv.visibility = View.GONE
        unlockLottie.visibility = View.GONE

        swipeToUnlockIv.visibility = View.VISIBLE
        swipeToUnlockTv.visibility = View.VISIBLE
        swipeToUnlockMsgTv.visibility = View.VISIBLE
    }

    private fun locked(enableSliding: Boolean) {
        Timber.d("locked")
        setSlidingEnabled(enableSliding)
        padlockSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED

        showPanelDown()
    }

    private fun unlocked(enableSliding: Boolean) {
        Timber.d("unlocked")
        setSlidingEnabled(enableSliding)

        showPanelUp()
        swipeToLockIv.visibility = View.GONE

        lockStatus.text = getString(R.string.unlocked_shackle_out)
        lockStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBottomSuccess))
    }

    private fun setSlidingEnabled(value: Boolean) {
        padlockSlidingLayout.isEnabled = value
    }

    private fun connectToLock() {
        if (!TextUtils.isEmpty(mMacAddress)) {
            OpenApp.instance?.getIBLE()?.connect(mMacAddress, this)
        }
    }

    override fun onConnect() {
        showSnack(padlockConstraint, "Connected", Snackbar.LENGTH_SHORT)
        viewModel.setConnectTime()
    }

    override fun onDisconnect(p0: Int) {
        Timber.d("disconnect")
        padlockDragView.setBackgroundColor(ContextCompat.getColor(this, R.color.grey))
        showSnack(padlockConstraint, "Disconnected", Snackbar.LENGTH_SHORT)
        setSlidingEnabled(false)
        viewModel.setDisconnectTime()
        if (connect)
            connectToLock()
        else
            finish()
    }

    override fun onServicesDiscovered(p0: String?, p1: String?) {
        Timber.d("onServicesDiscovered: $p0 - $p1")
        Handler().postDelayed({
            OpenApp.instance?.getIBLE()?.token
            padlockDragView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
            if (viewModel.isLockClosingData()) {
                lockUnlocked = viewModel.isLockUnlocked(lockId)
                Timber.e("$lockUnlocked")
                if (lockUnlocked) {
                    setSlidingEnabled(true)
                    padlockSlidingLayout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
                    unlocked(true)

                    if (tripSheetId.isNotEmpty()) {
                        // check for trip sheet id
                        showDialog(
                                R.string.trip_id,
                                R.string.trip_id_message,
                                R.string.verify,
                                DialogBoxInput.EDIT_TEXT) {
                            val tripId = getProductId()
                            if (tripId.isEmpty()) {
                                viewModel.showErrorSheet.value = getString(R.string.trip_id_valid)
                            } else {
                                viewModel.verifyTripSheetId(VerifyTripId(tripSheetId, tripId))
                            }
                        }
                    }
                } else {
                    viewModel.unLockedState.postValue(false)

                    if (tripSheetId.isNotEmpty()) {
                        // check for trip sheet otp
                        showDialog(
                                R.string.trip_otp,
                                R.string.trip_otp_message,
                                R.string.verify,
                                DialogBoxInput.OTP_VIEW) {
                            val otp = getOtp()
                            if (otp.length == 6) {
                                viewModel.verifyTripSheetOtp(VerifyTripOtp(lockId, otp.toInt()))
                            } else {
                                viewModel.showErrorSheet.value = getString(R.string.otp_valid)
                            }
                        }
                    }
                }
            } else {
                viewModel.unLockedState.postValue(false)
            }
            /*if (!Arrays.equals(Config.password, Config.default_password) *//*|| changePassword*//*) {
                padlockDragView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white))
                locked()
            }*/
        }, 500)
    }
}