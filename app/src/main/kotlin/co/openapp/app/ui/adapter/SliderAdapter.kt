package co.openapp.app.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import co.openapp.app.R
import co.openapp.app.data.db.Slider
import co.openapp.app.ui.view.general.SliderFragment

/**
 * Created by deepanshusinha on 13/03/18.
 */

class SliderAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private var jsonArray = listOf(
            Slider(R.string.slider_head_one, R.raw.slider_one),
            Slider(R.string.slider_head_two, R.raw.slider_two_new),
            Slider(R.string.slider_head_three, R.raw.slider_three_new),
            Slider(R.string.slider_head_four, R.raw.slider_four_new),
            Slider(R.string.slider_head_five, R.raw.slider_five_new),
            Slider(R.string.slider_head_six, R.raw.slider_six_new),
            Slider(R.string.slider_head_seven, R.raw.slider_seven),
            Slider(R.string.slider_head_eight, R.raw.slider_eight)
    )

    override fun getItem(position: Int): Fragment = SliderFragment.newInstance(jsonArray[position])

    override fun getCount(): Int = jsonArray.size
}