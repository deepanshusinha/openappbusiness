package co.openapp.app.ui.view.general

import android.os.Bundle
import android.view.View
import co.openapp.app.BR
import co.openapp.app.R
import co.openapp.app.data.db.Slider
import co.openapp.app.databinding.FragmentSliderBinding
import co.openapp.app.ui.base.BaseFragment
import co.openapp.app.viewmodel.SliderViewModel
import kotlinx.android.synthetic.main.fragment_slider.*

/**
 * Created by deepanshusinha on 13/03/18.
 */

class SliderFragment : BaseFragment<FragmentSliderBinding, SliderViewModel>() {

    override val layoutRes: Int
        get() = R.layout.fragment_slider

    override fun getViewModel(): Class<SliderViewModel> = SliderViewModel::class.java

    override fun bindingVariable(): Int = BR.slider

    companion object {
        const val TEXT_KEY = "text_key"
        const val IMAGE_KEY = "image_key"
        fun newInstance(slider: Slider): SliderFragment {
            val sliderFragment = SliderFragment()
            val args = Bundle()
            args.putInt(TEXT_KEY, slider.title)
            args.putInt(IMAGE_KEY, slider.json)
            sliderFragment.arguments = args
            return sliderFragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sliderHead.text = arguments?.getInt(TEXT_KEY)?.let { getString(it) }
        arguments?.getInt(IMAGE_KEY)?.let { sliderImage.setAnimation(it) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sliderImage.enableMergePathsForKitKatAndAbove(true)
        startAnimation()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser)
            startAnimation()
    }

    private fun startAnimation() {
        if (sliderImage != null) {

            if (sliderImage.isAnimating)
                sliderImage.cancelAnimation()

            sliderImage.playAnimation()
        }
    }
}