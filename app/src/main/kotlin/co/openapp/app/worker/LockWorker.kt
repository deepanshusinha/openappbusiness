package co.openapp.app.worker

import android.app.Application
import android.os.Build
import androidx.work.*
import co.openapp.app.utils.constant.Constants
import co.openapp.app.viewmodel.GeneralViewModel
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LockWorker
@Inject constructor(
        application: Application,
        workerParameters: WorkerParameters,
        private val viewModel: GeneralViewModel
) : Worker(application, workerParameters) {
    override fun doWork(): Result {
        Timber.e("Before WorkManager")
        viewModel.refreshSync()

        if (viewModel.isLogin) {
            viewModel.refreshLock()
        }
        Timber.e("After WorkManager")
        return Result.success()
    }

    /*override fun createWork(): Single<Result> {
        return lockRepository.lockListDataWorker()
                .doOnSuccess { message -> Timber.e("lockWorker-$message") }
                .map { Result.success() }
                .onErrorReturnItem(Result.failure())
    }*/

    companion object {
        fun start() {
            Timber.e("lockWorkerStarted")

            val constraintsBuilder = Constraints.Builder()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                constraintsBuilder.setRequiresDeviceIdle(true)
            }

            val constraints = constraintsBuilder
                    .setRequiresBatteryNotLow(true)
                    .setRequiresCharging(true)
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()

            val workRequest = PeriodicWorkRequestBuilder<LockWorker>(16, TimeUnit.MINUTES)
                    .addTag(Constants.WorkManagerKeys.List_PERIOD_WORK)
                    .setConstraints(constraints)
                    .build()

            WorkManager
                    .getInstance()
                    .enqueueUniquePeriodicWork(
                            Constants.WorkManagerKeys.List_PERIOD_WORK,
                            ExistingPeriodicWorkPolicy.KEEP,
                            workRequest
                    )
        }
    }
}