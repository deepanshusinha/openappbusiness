package co.openapp.app.worker

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import co.openapp.app.di.component.WorkerSubComponent
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class OpenAppWorkerFactory
@Inject
constructor(private val workerSubComponent: WorkerSubComponent.Builder) : WorkerFactory() {

    override fun createWorker(
            appContext: Context,
            workerClassName: String,
            workerParameters: WorkerParameters
    ) = workerSubComponent
            .workerParameters(workerParameters)
            .build().run {
                createWorker(workerClassName, workers())
            }

    private fun createWorker(
            workerClassName: String,
            workers: Map<Class<out Worker>, Provider<Worker>>
    ): ListenableWorker? = try {
        val workerClass = Class.forName(workerClassName).asSubclass(Worker::class.java)

        var provider = workers[workerClass]
        if (provider == null) {
            for ((key, value) in workers) {
                if (workerClass.isAssignableFrom(key)) {
                    provider = value
                    break
                }
            }
        }
        if (provider == null) {
            throw IllegalArgumentException("Missing binding for $workerClassName")
        }

        provider.get()
    } catch (e: Exception) {
        throw RuntimeException(e)
    }
}