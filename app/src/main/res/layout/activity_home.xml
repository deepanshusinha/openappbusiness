<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="home"
            type="co.openapp.app.viewmodel.HomeViewModel" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context="co.openapp.app.ui.home.HomeActivity">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/homeConstraint"
            android:layout_width="match_parent"
            android:layout_height="match_parent">

            <androidx.appcompat.widget.Toolbar
                android:id="@+id/homeToolbar"
                android:layout_width="0dp"
                android:layout_height="?attr/actionBarSize"
                android:background="@android:color/white"
                android:theme="?attr/actionBarTheme"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                app:popupTheme="@style/AppTheme.PopupOverlay">

                <RelativeLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:id="@+id/homeToolbarTitle"
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_alignParentStart="true"
                        android:layout_centerVertical="true"
                        android:layout_marginBottom="8dp"
                        android:layout_marginTop="12dp"
                        android:layout_toStartOf="@+id/homeLogout"
                        android:fontFamily="@font/circular_std_medium"
                        android:text="@string/home"
                        android:textColor="@android:color/black"
                        android:textSize="20sp" />

                    <ImageView
                        android:id="@+id/homeLogout"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentEnd="true"
                        android:layout_centerVertical="true"
                        android:layout_marginEnd="@dimen/dp_16"
                        android:layout_marginStart="@dimen/dp_16"
                        android:background="?attr/selectableItemBackgroundBorderless"
                        android:contentDescription="@string/app_name"
                        android:onClick="@{() -> home.attemptLogout()}"
                        android:padding="@dimen/dp_16"
                        android:scaleType="fitCenter"
                        app:srcCompat="@drawable/ic_power_settings_new_black_24dp" />
                </RelativeLayout>

            </androidx.appcompat.widget.Toolbar>

            <com.airbnb.lottie.LottieAnimationView
                android:layout_width="@dimen/dp_0"
                android:layout_height="@dimen/dp_0"
                android:layout_margin="@dimen/dp_16"
                app:layout_constraintBottom_toTopOf="@+id/homeTitle"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/homeToolbar"
                app:lottie_autoPlay="true"
                app:lottie_loop="true"
                app:lottie_rawRes="@raw/welcome_page" />

            <TextView
                android:id="@+id/homeHead"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="32dp"
                android:layout_marginStart="32dp"
                android:fontFamily="@font/lineto_brown"
                android:text="@string/scanning_for_locks"
                android:textColor="@android:color/black"
                android:textSize="20sp"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/homeToolbar" />

            <TextView
                android:id="@+id/homeTitle"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginBottom="16dp"
                android:layout_marginEnd="32dp"
                android:layout_marginStart="32dp"
                android:fontFamily="@font/circular_std_medium"
                android:text="@string/get_started"
                android:textAllCaps="true"
                android:textColor="@color/colorBottomSuccess"
                app:layout_constraintBottom_toTopOf="@+id/homeHead"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent" />

            <TextView
                android:id="@+id/homeSubHead"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginEnd="48dp"
                android:layout_marginStart="48dp"
                android:layout_marginTop="24dp"
                android:fontFamily="@font/lineto_brown_light"
                android:text="@string/home_scan_message"
                android:textAlignment="center"
                android:textColor="@android:color/black"
                android:textSize="18sp"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/homeHead" />

            <Button
                android:id="@+id/homeScanButton"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginEnd="50dp"
                android:layout_marginStart="50dp"
                android:layout_marginTop="36dp"
                android:background="@drawable/shape_rounded_blue"
                android:fontFamily="@font/circular_std_medium"
                android:foreground="?attr/selectableItemBackgroundBorderless"
                android:onClick="@{() -> home.startScanning()}"
                android:paddingBottom="16dp"
                android:paddingTop="16dp"
                android:text="@string/scan_locks_near_me"
                android:textAlignment="center"
                android:textColor="@android:color/white"
                android:textSize="18sp"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/homeSubHead" />

            <TextView
                android:id="@+id/homeVersion"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginEnd="48dp"
                android:layout_marginStart="48dp"
                android:layout_marginTop="16dp"
                android:fontFamily="@font/lineto_brown_light"
                android:text="@{home.versionCode}"
                android:textAlignment="center"
                android:textSize="12sp"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/homeScanButton" />

            <co.openapp.app.widget.fab.FloatingActionMenu
                android:id="@+id/homeFab"
                android:layout_width="0dp"
                android:layout_height="0dp"
                android:padding="@dimen/dp_16"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/homeToolbar"
                app:menu_backgroundColor="#ccffffff"
                app:menu_labels_ellipsize="end"
                app:menu_labels_singleLine="true">

                <co.openapp.app.widget.fab.FloatingActionButton
                    android:id="@+id/fabDoor"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:src="@drawable/ic_lock"
                    android:visibility="gone"
                    app:fab_label="@string/door_lock"
                    app:fab_size="mini" />

                <co.openapp.app.widget.fab.FloatingActionButton
                    android:id="@+id/fabSms"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:src="@drawable/ic_textsms"
                    app:fab_label="@string/sms_access"
                    app:fab_size="mini" />

                <co.openapp.app.widget.fab.FloatingActionButton
                    android:id="@+id/fabHelp"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:src="@drawable/ic_call"
                    app:fab_label="@string/help"
                    app:fab_size="mini" />
            </co.openapp.app.widget.fab.FloatingActionMenu>
        </androidx.constraintlayout.widget.ConstraintLayout>

        <!-- include bottom sheet -->
        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/doorLockSheet"
            android:layout_width="match_parent"
            android:layout_height="340dp"
            android:background="@color/colorOverlayLight"
            app:behavior_hideable="true"
            app:layout_behavior="@string/bottom_sheet_behavior"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent">

            <androidx.recyclerview.widget.RecyclerView
                android:id="@+id/doorLockList"
                android:layout_width="0dp"
                android:layout_height="0dp"
                android:layout_margin="16dp"
                app:layout_behavior="@string/appbar_scrolling_view_behavior"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />
        </androidx.constraintlayout.widget.ConstraintLayout>
    </androidx.coordinatorlayout.widget.CoordinatorLayout>
</layout>